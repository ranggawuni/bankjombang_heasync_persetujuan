﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmConfig
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.GroupBox1 = New System.Windows.Forms.GroupBox()
    Me.cKodeKantorInduk = New System.Windows.Forms.TextBox()
    Me.Label13 = New System.Windows.Forms.Label()
    Me.cInterval = New System.Windows.Forms.TextBox()
    Me.Label11 = New System.Windows.Forms.Label()
    Me.cPath = New System.Windows.Forms.TextBox()
    Me.Label10 = New System.Windows.Forms.Label()
    Me.cWaktuPosting = New System.Windows.Forms.TextBox()
    Me.Label7 = New System.Windows.Forms.Label()
    Me.cTglPosting = New System.Windows.Forms.TextBox()
    Me.Label6 = New System.Windows.Forms.Label()
    Me.cTglBunga = New System.Windows.Forms.TextBox()
    Me.Label5 = New System.Windows.Forms.Label()
    Me.cURL = New System.Windows.Forms.TextBox()
    Me.Label4 = New System.Windows.Forms.Label()
    Me.cPort = New System.Windows.Forms.TextBox()
    Me.Label3 = New System.Windows.Forms.Label()
    Me.cIP = New System.Windows.Forms.TextBox()
    Me.Label2 = New System.Windows.Forms.Label()
    Me.cDatabase = New System.Windows.Forms.TextBox()
    Me.Label1 = New System.Windows.Forms.Label()
    Me.GroupBox2 = New System.Windows.Forms.GroupBox()
    Me.optServicePengajuan2 = New System.Windows.Forms.RadioButton()
    Me.optServicePengajuan1 = New System.Windows.Forms.RadioButton()
    Me.Label9 = New System.Windows.Forms.Label()
    Me.GroupBox3 = New System.Windows.Forms.GroupBox()
    Me.Label12 = New System.Windows.Forms.Label()
    Me.optServicePostingBungaTabungan2 = New System.Windows.Forms.RadioButton()
    Me.optServicePostingBungaTabungan1 = New System.Windows.Forms.RadioButton()
    Me.Label8 = New System.Windows.Forms.Label()
    Me.GroupBox4 = New System.Windows.Forms.GroupBox()
    Me.cmdKeluar = New System.Windows.Forms.Button()
    Me.cmdSimpan = New System.Windows.Forms.Button()
    Me.cTglBunga1 = New System.Windows.Forms.TextBox()
    Me.Label14 = New System.Windows.Forms.Label()
    Me.GroupBox1.SuspendLayout()
    Me.GroupBox2.SuspendLayout()
    Me.GroupBox3.SuspendLayout()
    Me.GroupBox4.SuspendLayout()
    Me.SuspendLayout()
    '
    'GroupBox1
    '
    Me.GroupBox1.Controls.Add(Me.Label14)
    Me.GroupBox1.Controls.Add(Me.cTglBunga1)
    Me.GroupBox1.Controls.Add(Me.cKodeKantorInduk)
    Me.GroupBox1.Controls.Add(Me.Label13)
    Me.GroupBox1.Controls.Add(Me.cInterval)
    Me.GroupBox1.Controls.Add(Me.Label11)
    Me.GroupBox1.Controls.Add(Me.cPath)
    Me.GroupBox1.Controls.Add(Me.Label10)
    Me.GroupBox1.Controls.Add(Me.cWaktuPosting)
    Me.GroupBox1.Controls.Add(Me.Label7)
    Me.GroupBox1.Controls.Add(Me.cTglPosting)
    Me.GroupBox1.Controls.Add(Me.Label6)
    Me.GroupBox1.Controls.Add(Me.cTglBunga)
    Me.GroupBox1.Controls.Add(Me.Label5)
    Me.GroupBox1.Controls.Add(Me.cURL)
    Me.GroupBox1.Controls.Add(Me.Label4)
    Me.GroupBox1.Controls.Add(Me.cPort)
    Me.GroupBox1.Controls.Add(Me.Label3)
    Me.GroupBox1.Controls.Add(Me.cIP)
    Me.GroupBox1.Controls.Add(Me.Label2)
    Me.GroupBox1.Controls.Add(Me.cDatabase)
    Me.GroupBox1.Controls.Add(Me.Label1)
    Me.GroupBox1.Location = New System.Drawing.Point(5, 0)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(703, 327)
    Me.GroupBox1.TabIndex = 0
    Me.GroupBox1.TabStop = False
    '
    'cKodeKantorInduk
    '
    Me.cKodeKantorInduk.Location = New System.Drawing.Point(207, 256)
    Me.cKodeKantorInduk.Name = "cKodeKantorInduk"
    Me.cKodeKantorInduk.Size = New System.Drawing.Size(27, 20)
    Me.cKodeKantorInduk.TabIndex = 10
    '
    'Label13
    '
    Me.Label13.AutoSize = True
    Me.Label13.Location = New System.Drawing.Point(7, 259)
    Me.Label13.Name = "Label13"
    Me.Label13.Size = New System.Drawing.Size(96, 13)
    Me.Label13.TabIndex = 18
    Me.Label13.Text = "Kode Kantor Induk"
    '
    'cInterval
    '
    Me.cInterval.Location = New System.Drawing.Point(207, 230)
    Me.cInterval.Name = "cInterval"
    Me.cInterval.Size = New System.Drawing.Size(27, 20)
    Me.cInterval.TabIndex = 9
    '
    'Label11
    '
    Me.Label11.AutoSize = True
    Me.Label11.Location = New System.Drawing.Point(7, 233)
    Me.Label11.Name = "Label11"
    Me.Label11.Size = New System.Drawing.Size(185, 13)
    Me.Label11.TabIndex = 16
    Me.Label11.Text = "Interval Persetujuan Pengajuan Kredit"
    '
    'cPath
    '
    Me.cPath.Location = New System.Drawing.Point(207, 204)
    Me.cPath.Name = "cPath"
    Me.cPath.Size = New System.Drawing.Size(490, 20)
    Me.cPath.TabIndex = 8
    '
    'Label10
    '
    Me.Label10.AutoSize = True
    Me.Label10.Location = New System.Drawing.Point(7, 207)
    Me.Label10.Name = "Label10"
    Me.Label10.Size = New System.Drawing.Size(61, 13)
    Me.Label10.TabIndex = 14
    Me.Label10.Text = "Config path"
    '
    'cWaktuPosting
    '
    Me.cWaktuPosting.Location = New System.Drawing.Point(207, 178)
    Me.cWaktuPosting.Name = "cWaktuPosting"
    Me.cWaktuPosting.Size = New System.Drawing.Size(55, 20)
    Me.cWaktuPosting.TabIndex = 7
    Me.cWaktuPosting.Text = "00:00:00"
    '
    'Label7
    '
    Me.Label7.AutoSize = True
    Me.Label7.Location = New System.Drawing.Point(7, 181)
    Me.Label7.Name = "Label7"
    Me.Label7.Size = New System.Drawing.Size(163, 13)
    Me.Label7.TabIndex = 12
    Me.Label7.Text = "Waktu Posting Bunga Tabungan"
    '
    'cTglPosting
    '
    Me.cTglPosting.Location = New System.Drawing.Point(207, 152)
    Me.cTglPosting.Name = "cTglPosting"
    Me.cTglPosting.Size = New System.Drawing.Size(27, 20)
    Me.cTglPosting.TabIndex = 6
    '
    'Label6
    '
    Me.Label6.AutoSize = True
    Me.Label6.Location = New System.Drawing.Point(7, 155)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(170, 13)
    Me.Label6.TabIndex = 10
    Me.Label6.Text = "Tanggal Posting Bunga Tabungan"
    '
    'cTglBunga
    '
    Me.cTglBunga.Location = New System.Drawing.Point(207, 126)
    Me.cTglBunga.Name = "cTglBunga"
    Me.cTglBunga.Size = New System.Drawing.Size(27, 20)
    Me.cTglBunga.TabIndex = 5
    '
    'Label5
    '
    Me.Label5.AutoSize = True
    Me.Label5.Location = New System.Drawing.Point(7, 129)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(192, 13)
    Me.Label5.TabIndex = 8
    Me.Label5.Text = "Tanggal Perhitungan Bunga Tabungan"
    '
    'cURL
    '
    Me.cURL.Location = New System.Drawing.Point(207, 100)
    Me.cURL.Name = "cURL"
    Me.cURL.Size = New System.Drawing.Size(173, 20)
    Me.cURL.TabIndex = 4
    '
    'Label4
    '
    Me.Label4.AutoSize = True
    Me.Label4.Location = New System.Drawing.Point(7, 103)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(53, 13)
    Me.Label4.TabIndex = 6
    Me.Label4.Text = "URL VPS"
    '
    'cPort
    '
    Me.cPort.Location = New System.Drawing.Point(207, 74)
    Me.cPort.Name = "cPort"
    Me.cPort.Size = New System.Drawing.Size(55, 20)
    Me.cPort.TabIndex = 3
    '
    'Label3
    '
    Me.Label3.AutoSize = True
    Me.Label3.Location = New System.Drawing.Point(7, 77)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(26, 13)
    Me.Label3.TabIndex = 4
    Me.Label3.Text = "Port"
    '
    'cIP
    '
    Me.cIP.Location = New System.Drawing.Point(207, 48)
    Me.cIP.Name = "cIP"
    Me.cIP.Size = New System.Drawing.Size(173, 20)
    Me.cIP.TabIndex = 2
    '
    'Label2
    '
    Me.Label2.AutoSize = True
    Me.Label2.Location = New System.Drawing.Point(7, 51)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(51, 13)
    Me.Label2.TabIndex = 2
    Me.Label2.Text = "IP Server"
    '
    'cDatabase
    '
    Me.cDatabase.Location = New System.Drawing.Point(207, 22)
    Me.cDatabase.Name = "cDatabase"
    Me.cDatabase.Size = New System.Drawing.Size(173, 20)
    Me.cDatabase.TabIndex = 1
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Location = New System.Drawing.Point(7, 25)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(53, 13)
    Me.Label1.TabIndex = 0
    Me.Label1.Text = "Database"
    '
    'GroupBox2
    '
    Me.GroupBox2.Controls.Add(Me.optServicePengajuan2)
    Me.GroupBox2.Controls.Add(Me.optServicePengajuan1)
    Me.GroupBox2.Controls.Add(Me.Label9)
    Me.GroupBox2.Location = New System.Drawing.Point(5, 333)
    Me.GroupBox2.Name = "GroupBox2"
    Me.GroupBox2.Size = New System.Drawing.Size(703, 40)
    Me.GroupBox2.TabIndex = 1
    Me.GroupBox2.TabStop = False
    '
    'optServicePengajuan2
    '
    Me.optServicePengajuan2.AutoSize = True
    Me.optServicePengajuan2.Location = New System.Drawing.Point(309, 14)
    Me.optServicePengajuan2.Name = "optServicePengajuan2"
    Me.optServicePengajuan2.Size = New System.Drawing.Size(60, 17)
    Me.optServicePengajuan2.TabIndex = 12
    Me.optServicePengajuan2.Text = "&Disable"
    Me.optServicePengajuan2.UseVisualStyleBackColor = True
    '
    'optServicePengajuan1
    '
    Me.optServicePengajuan1.AutoSize = True
    Me.optServicePengajuan1.Checked = True
    Me.optServicePengajuan1.Location = New System.Drawing.Point(240, 14)
    Me.optServicePengajuan1.Name = "optServicePengajuan1"
    Me.optServicePengajuan1.Size = New System.Drawing.Size(58, 17)
    Me.optServicePengajuan1.TabIndex = 11
    Me.optServicePengajuan1.TabStop = True
    Me.optServicePengajuan1.Text = "&Enable"
    Me.optServicePengajuan1.UseVisualStyleBackColor = True
    '
    'Label9
    '
    Me.Label9.AutoSize = True
    Me.Label9.Location = New System.Drawing.Point(7, 16)
    Me.Label9.Name = "Label9"
    Me.Label9.Size = New System.Drawing.Size(227, 13)
    Me.Label9.TabIndex = 3
    Me.Label9.Text = "Autorun service persetujuan pengajuan kredit :"
    '
    'GroupBox3
    '
    Me.GroupBox3.Controls.Add(Me.Label12)
    Me.GroupBox3.Controls.Add(Me.optServicePostingBungaTabungan2)
    Me.GroupBox3.Controls.Add(Me.optServicePostingBungaTabungan1)
    Me.GroupBox3.Controls.Add(Me.Label8)
    Me.GroupBox3.Location = New System.Drawing.Point(5, 379)
    Me.GroupBox3.Name = "GroupBox3"
    Me.GroupBox3.Size = New System.Drawing.Size(703, 40)
    Me.GroupBox3.TabIndex = 2
    Me.GroupBox3.TabStop = False
    '
    'Label12
    '
    Me.Label12.AutoSize = True
    Me.Label12.Location = New System.Drawing.Point(375, 16)
    Me.Label12.Name = "Label12"
    Me.Label12.Size = New System.Drawing.Size(200, 13)
    Me.Label12.TabIndex = 12
    Me.Label12.Text = "(Exit service to refresh the service status)"
    '
    'optServicePostingBungaTabungan2
    '
    Me.optServicePostingBungaTabungan2.AutoSize = True
    Me.optServicePostingBungaTabungan2.Location = New System.Drawing.Point(309, 14)
    Me.optServicePostingBungaTabungan2.Name = "optServicePostingBungaTabungan2"
    Me.optServicePostingBungaTabungan2.Size = New System.Drawing.Size(60, 17)
    Me.optServicePostingBungaTabungan2.TabIndex = 14
    Me.optServicePostingBungaTabungan2.TabStop = True
    Me.optServicePostingBungaTabungan2.Text = "&Disable"
    Me.optServicePostingBungaTabungan2.UseVisualStyleBackColor = True
    '
    'optServicePostingBungaTabungan1
    '
    Me.optServicePostingBungaTabungan1.AutoSize = True
    Me.optServicePostingBungaTabungan1.Location = New System.Drawing.Point(240, 14)
    Me.optServicePostingBungaTabungan1.Name = "optServicePostingBungaTabungan1"
    Me.optServicePostingBungaTabungan1.Size = New System.Drawing.Size(58, 17)
    Me.optServicePostingBungaTabungan1.TabIndex = 13
    Me.optServicePostingBungaTabungan1.TabStop = True
    Me.optServicePostingBungaTabungan1.Text = "&Enable"
    Me.optServicePostingBungaTabungan1.UseVisualStyleBackColor = True
    '
    'Label8
    '
    Me.Label8.AutoSize = True
    Me.Label8.Location = New System.Drawing.Point(7, 16)
    Me.Label8.Name = "Label8"
    Me.Label8.Size = New System.Drawing.Size(205, 13)
    Me.Label8.TabIndex = 3
    Me.Label8.Text = "Autorun service posting bunga tabungan :"
    '
    'GroupBox4
    '
    Me.GroupBox4.Controls.Add(Me.cmdKeluar)
    Me.GroupBox4.Controls.Add(Me.cmdSimpan)
    Me.GroupBox4.Location = New System.Drawing.Point(5, 425)
    Me.GroupBox4.Name = "GroupBox4"
    Me.GroupBox4.Size = New System.Drawing.Size(704, 48)
    Me.GroupBox4.TabIndex = 3
    Me.GroupBox4.TabStop = False
    '
    'cmdKeluar
    '
    Me.cmdKeluar.Image = Global.HeaSync_Persetujuan_Pengajuan.My.Resources.Resources.app_window_cross_icon_16
    Me.cmdKeluar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.cmdKeluar.Location = New System.Drawing.Point(623, 15)
    Me.cmdKeluar.Name = "cmdKeluar"
    Me.cmdKeluar.Size = New System.Drawing.Size(75, 23)
    Me.cmdKeluar.TabIndex = 16
    Me.cmdKeluar.Text = "Close"
    Me.cmdKeluar.UseVisualStyleBackColor = True
    '
    'cmdSimpan
    '
    Me.cmdSimpan.Image = Global.HeaSync_Persetujuan_Pengajuan.My.Resources.Resources.save_icon_16
    Me.cmdSimpan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.cmdSimpan.Location = New System.Drawing.Point(546, 15)
    Me.cmdSimpan.Name = "cmdSimpan"
    Me.cmdSimpan.Size = New System.Drawing.Size(75, 23)
    Me.cmdSimpan.TabIndex = 15
    Me.cmdSimpan.Text = "Save"
    Me.cmdSimpan.UseVisualStyleBackColor = True
    '
    'cTglBunga1
    '
    Me.cTglBunga1.Location = New System.Drawing.Point(269, 126)
    Me.cTglBunga1.Name = "cTglBunga1"
    Me.cTglBunga1.Size = New System.Drawing.Size(27, 20)
    Me.cTglBunga1.TabIndex = 19
    '
    'Label14
    '
    Me.Label14.AutoSize = True
    Me.Label14.Location = New System.Drawing.Point(240, 129)
    Me.Label14.Name = "Label14"
    Me.Label14.Size = New System.Drawing.Size(23, 13)
    Me.Label14.TabIndex = 20
    Me.Label14.Text = "s/d"
    '
    'FrmConfig
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(714, 480)
    Me.Controls.Add(Me.GroupBox4)
    Me.Controls.Add(Me.GroupBox3)
    Me.Controls.Add(Me.GroupBox2)
    Me.Controls.Add(Me.GroupBox1)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.KeyPreview = True
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "FrmConfig"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
    Me.Text = "Setting"
    Me.GroupBox1.ResumeLayout(False)
    Me.GroupBox1.PerformLayout()
    Me.GroupBox2.ResumeLayout(False)
    Me.GroupBox2.PerformLayout()
    Me.GroupBox3.ResumeLayout(False)
    Me.GroupBox3.PerformLayout()
    Me.GroupBox4.ResumeLayout(False)
    Me.ResumeLayout(False)

  End Sub
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
  Friend WithEvents cURL As System.Windows.Forms.TextBox
  Friend WithEvents Label4 As System.Windows.Forms.Label
  Friend WithEvents cPort As System.Windows.Forms.TextBox
  Friend WithEvents Label3 As System.Windows.Forms.Label
  Friend WithEvents cIP As System.Windows.Forms.TextBox
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents cDatabase As System.Windows.Forms.TextBox
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents cWaktuPosting As System.Windows.Forms.TextBox
  Friend WithEvents Label7 As System.Windows.Forms.Label
  Friend WithEvents cTglPosting As System.Windows.Forms.TextBox
  Friend WithEvents Label6 As System.Windows.Forms.Label
  Friend WithEvents cTglBunga As System.Windows.Forms.TextBox
  Friend WithEvents Label5 As System.Windows.Forms.Label
  Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
  Friend WithEvents optServicePengajuan2 As System.Windows.Forms.RadioButton
  Friend WithEvents optServicePengajuan1 As System.Windows.Forms.RadioButton
  Friend WithEvents Label9 As System.Windows.Forms.Label
  Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
  Friend WithEvents optServicePostingBungaTabungan2 As System.Windows.Forms.RadioButton
  Friend WithEvents optServicePostingBungaTabungan1 As System.Windows.Forms.RadioButton
  Friend WithEvents Label8 As System.Windows.Forms.Label
  Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
  Friend WithEvents cmdKeluar As System.Windows.Forms.Button
  Friend WithEvents cmdSimpan As System.Windows.Forms.Button
  Friend WithEvents cPath As System.Windows.Forms.TextBox
  Friend WithEvents Label10 As System.Windows.Forms.Label
  Friend WithEvents cInterval As System.Windows.Forms.TextBox
  Friend WithEvents Label11 As System.Windows.Forms.Label
  Friend WithEvents Label12 As System.Windows.Forms.Label
  Friend WithEvents cKodeKantorInduk As System.Windows.Forms.TextBox
  Friend WithEvents Label13 As System.Windows.Forms.Label
  Friend WithEvents Label14 As System.Windows.Forms.Label
  Friend WithEvents cTglBunga1 As System.Windows.Forms.TextBox
End Class
