﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
    Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
    Me.SettingToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
    Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
    Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
    Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
    Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
    Me.SettingToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
    Me.ManualOverrideToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
    Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripSeparator()
    Me.ExitToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
    Me.timerPersetujuan = New System.Windows.Forms.Timer(Me.components)
    Me.timerPostingBunga = New System.Windows.Forms.Timer(Me.components)
    Me.bwKirimPengajuan = New System.ComponentModel.BackgroundWorker()
    Me.bwAmbilPengajuan = New System.ComponentModel.BackgroundWorker()
    Me.bwPostingAdm = New System.ComponentModel.BackgroundWorker()
    Me.bwPostingBunga = New System.ComponentModel.BackgroundWorker()
    Me.GroupBox2 = New System.Windows.Forms.GroupBox()
    Me.dTglPosting = New System.Windows.Forms.DateTimePicker()
    Me.dTgl1 = New System.Windows.Forms.DateTimePicker()
    Me.dTgl = New System.Windows.Forms.DateTimePicker()
    Me.lblNasabahBunga = New System.Windows.Forms.Label()
    Me.lblNasabahAdm = New System.Windows.Forms.Label()
    Me.lblStatusBunga = New System.Windows.Forms.Label()
    Me.lblBunga = New System.Windows.Forms.Label()
    Me.pbPostingBunga = New System.Windows.Forms.ProgressBar()
    Me.lblStatusAdm = New System.Windows.Forms.Label()
    Me.lblAdm = New System.Windows.Forms.Label()
    Me.pbPostingAdm = New System.Windows.Forms.ProgressBar()
    Me.Label3 = New System.Windows.Forms.Label()
    Me.Label2 = New System.Windows.Forms.Label()
    Me.Label1 = New System.Windows.Forms.Label()
    Me.GroupBox1 = New System.Windows.Forms.GroupBox()
    Me.lblStatusAmbil = New System.Windows.Forms.Label()
    Me.lblStatusKirim = New System.Windows.Forms.Label()
    Me.lblAmbil = New System.Windows.Forms.Label()
    Me.pbAmbilPengajuan = New System.Windows.Forms.ProgressBar()
    Me.lblKirim = New System.Windows.Forms.Label()
    Me.pbKirimPengajuan = New System.Windows.Forms.ProgressBar()
    Me.Label4 = New System.Windows.Forms.Label()
    Me.cTime = New System.Windows.Forms.TextBox()
    Me.cmdClose = New System.Windows.Forms.Button()
    Me.timerLabel = New System.Windows.Forms.Timer(Me.components)
    Me.Label5 = New System.Windows.Forms.Label()
    Me.cURL = New System.Windows.Forms.TextBox()
    Me.lblOnline = New System.Windows.Forms.Label()
    Me.ShowToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
    Me.ContextMenuStrip1.SuspendLayout()
    Me.MenuStrip1.SuspendLayout()
    Me.GroupBox2.SuspendLayout()
    Me.GroupBox1.SuspendLayout()
    Me.SuspendLayout()
    '
    'ContextMenuStrip1
    '
    Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ShowToolStripMenuItem, Me.ToolStripMenuItem2, Me.SettingToolStripMenuItem, Me.ToolStripMenuItem1, Me.ExitToolStripMenuItem})
    Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
    Me.ContextMenuStrip1.Size = New System.Drawing.Size(163, 120)
    '
    'ToolStripMenuItem2
    '
    Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
    Me.ToolStripMenuItem2.Size = New System.Drawing.Size(162, 22)
    Me.ToolStripMenuItem2.Text = "Setting"
    '
    'SettingToolStripMenuItem
    '
    Me.SettingToolStripMenuItem.Name = "SettingToolStripMenuItem"
    Me.SettingToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
    Me.SettingToolStripMenuItem.Text = "Manual Override"
    '
    'ToolStripMenuItem1
    '
    Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
    Me.ToolStripMenuItem1.Size = New System.Drawing.Size(159, 6)
    '
    'ExitToolStripMenuItem
    '
    Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
    Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
    Me.ExitToolStripMenuItem.Text = "Exit"
    '
    'Timer1
    '
    '
    'MenuStrip1
    '
    Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem})
    Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
    Me.MenuStrip1.Name = "MenuStrip1"
    Me.MenuStrip1.Size = New System.Drawing.Size(1123, 24)
    Me.MenuStrip1.TabIndex = 1
    Me.MenuStrip1.Text = "MenuStrip1"
    '
    'FileToolStripMenuItem
    '
    Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SettingToolStripMenuItem1, Me.ManualOverrideToolStripMenuItem, Me.ToolStripMenuItem3, Me.ExitToolStripMenuItem1})
    Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
    Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
    Me.FileToolStripMenuItem.Text = "File"
    '
    'SettingToolStripMenuItem1
    '
    Me.SettingToolStripMenuItem1.Name = "SettingToolStripMenuItem1"
    Me.SettingToolStripMenuItem1.Size = New System.Drawing.Size(162, 22)
    Me.SettingToolStripMenuItem1.Text = "Setting"
    '
    'ManualOverrideToolStripMenuItem
    '
    Me.ManualOverrideToolStripMenuItem.Name = "ManualOverrideToolStripMenuItem"
    Me.ManualOverrideToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
    Me.ManualOverrideToolStripMenuItem.Text = "Manual Override"
    '
    'ToolStripMenuItem3
    '
    Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
    Me.ToolStripMenuItem3.Size = New System.Drawing.Size(159, 6)
    '
    'ExitToolStripMenuItem1
    '
    Me.ExitToolStripMenuItem1.Name = "ExitToolStripMenuItem1"
    Me.ExitToolStripMenuItem1.Size = New System.Drawing.Size(162, 22)
    Me.ExitToolStripMenuItem1.Text = "Exit"
    '
    'timerPersetujuan
    '
    Me.timerPersetujuan.Interval = 1000
    '
    'timerPostingBunga
    '
    Me.timerPostingBunga.Interval = 1000
    '
    'bwKirimPengajuan
    '
    '
    'bwAmbilPengajuan
    '
    '
    'bwPostingAdm
    '
    '
    'bwPostingBunga
    '
    '
    'GroupBox2
    '
    Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
    Me.GroupBox2.Controls.Add(Me.dTglPosting)
    Me.GroupBox2.Controls.Add(Me.dTgl1)
    Me.GroupBox2.Controls.Add(Me.dTgl)
    Me.GroupBox2.Controls.Add(Me.lblNasabahBunga)
    Me.GroupBox2.Controls.Add(Me.lblNasabahAdm)
    Me.GroupBox2.Controls.Add(Me.lblStatusBunga)
    Me.GroupBox2.Controls.Add(Me.lblBunga)
    Me.GroupBox2.Controls.Add(Me.pbPostingBunga)
    Me.GroupBox2.Controls.Add(Me.lblStatusAdm)
    Me.GroupBox2.Controls.Add(Me.lblAdm)
    Me.GroupBox2.Controls.Add(Me.pbPostingAdm)
    Me.GroupBox2.Controls.Add(Me.Label3)
    Me.GroupBox2.Controls.Add(Me.Label2)
    Me.GroupBox2.Controls.Add(Me.Label1)
    Me.GroupBox2.Location = New System.Drawing.Point(8, 169)
    Me.GroupBox2.Name = "GroupBox2"
    Me.GroupBox2.Size = New System.Drawing.Size(1110, 165)
    Me.GroupBox2.TabIndex = 16
    Me.GroupBox2.TabStop = False
    Me.GroupBox2.Text = "Administrasi && Bunga Tabungan"
    '
    'dTglPosting
    '
    Me.dTglPosting.CustomFormat = "dd-MM-yyyy HH:mm:ss"
    Me.dTglPosting.Enabled = False
    Me.dTglPosting.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.dTglPosting.Location = New System.Drawing.Point(103, 55)
    Me.dTglPosting.Name = "dTglPosting"
    Me.dTglPosting.Size = New System.Drawing.Size(136, 20)
    Me.dTglPosting.TabIndex = 24
    '
    'dTgl1
    '
    Me.dTgl1.CustomFormat = "dd"
    Me.dTgl1.Enabled = False
    Me.dTgl1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
    Me.dTgl1.Location = New System.Drawing.Point(218, 29)
    Me.dTgl1.Name = "dTgl1"
    Me.dTgl1.Size = New System.Drawing.Size(77, 20)
    Me.dTgl1.TabIndex = 23
    '
    'dTgl
    '
    Me.dTgl.CustomFormat = "dd"
    Me.dTgl.Enabled = False
    Me.dTgl.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
    Me.dTgl.Location = New System.Drawing.Point(103, 29)
    Me.dTgl.Margin = New System.Windows.Forms.Padding(4)
    Me.dTgl.Name = "dTgl"
    Me.dTgl.Size = New System.Drawing.Size(79, 20)
    Me.dTgl.TabIndex = 22
    '
    'lblNasabahBunga
    '
    Me.lblNasabahBunga.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblNasabahBunga.Location = New System.Drawing.Point(765, 125)
    Me.lblNasabahBunga.Name = "lblNasabahBunga"
    Me.lblNasabahBunga.Size = New System.Drawing.Size(324, 23)
    Me.lblNasabahBunga.TabIndex = 21
    Me.lblNasabahBunga.Text = "Nasabah Kantor :  00 - 9999999 of 999999"
    Me.lblNasabahBunga.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.lblNasabahBunga.Visible = False
    '
    'lblNasabahAdm
    '
    Me.lblNasabahAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblNasabahAdm.Location = New System.Drawing.Point(765, 89)
    Me.lblNasabahAdm.Name = "lblNasabahAdm"
    Me.lblNasabahAdm.Size = New System.Drawing.Size(324, 23)
    Me.lblNasabahAdm.TabIndex = 20
    Me.lblNasabahAdm.Text = "Nasabah Kantor :  00 - 9999999 of 999999"
    Me.lblNasabahAdm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.lblNasabahAdm.Visible = False
    '
    'lblStatusBunga
    '
    Me.lblStatusBunga.BackColor = System.Drawing.Color.Transparent
    Me.lblStatusBunga.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblStatusBunga.Location = New System.Drawing.Point(11, 125)
    Me.lblStatusBunga.Name = "lblStatusBunga"
    Me.lblStatusBunga.Size = New System.Drawing.Size(178, 23)
    Me.lblStatusBunga.TabIndex = 19
    Me.lblStatusBunga.Text = "Service is active"
    Me.lblStatusBunga.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lblBunga
    '
    Me.lblBunga.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblBunga.Location = New System.Drawing.Point(626, 125)
    Me.lblBunga.Name = "lblBunga"
    Me.lblBunga.Size = New System.Drawing.Size(133, 23)
    Me.lblBunga.TabIndex = 18
    Me.lblBunga.Text = "100 % Complete"
    Me.lblBunga.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.lblBunga.Visible = False
    '
    'pbPostingBunga
    '
    Me.pbPostingBunga.Location = New System.Drawing.Point(195, 125)
    Me.pbPostingBunga.Name = "pbPostingBunga"
    Me.pbPostingBunga.Size = New System.Drawing.Size(425, 23)
    Me.pbPostingBunga.TabIndex = 17
    Me.pbPostingBunga.Visible = False
    '
    'lblStatusAdm
    '
    Me.lblStatusAdm.BackColor = System.Drawing.Color.Transparent
    Me.lblStatusAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblStatusAdm.Location = New System.Drawing.Point(11, 89)
    Me.lblStatusAdm.Name = "lblStatusAdm"
    Me.lblStatusAdm.Size = New System.Drawing.Size(178, 23)
    Me.lblStatusAdm.TabIndex = 16
    Me.lblStatusAdm.Text = "Service is active"
    Me.lblStatusAdm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lblAdm
    '
    Me.lblAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblAdm.Location = New System.Drawing.Point(626, 89)
    Me.lblAdm.Name = "lblAdm"
    Me.lblAdm.Size = New System.Drawing.Size(133, 23)
    Me.lblAdm.TabIndex = 13
    Me.lblAdm.Text = "100 % Complete"
    Me.lblAdm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.lblAdm.Visible = False
    '
    'pbPostingAdm
    '
    Me.pbPostingAdm.Location = New System.Drawing.Point(195, 89)
    Me.pbPostingAdm.Name = "pbPostingAdm"
    Me.pbPostingAdm.Size = New System.Drawing.Size(425, 23)
    Me.pbPostingAdm.TabIndex = 12
    Me.pbPostingAdm.Visible = False
    '
    'Label3
    '
    Me.Label3.AutoSize = True
    Me.Label3.Location = New System.Drawing.Point(12, 59)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(90, 13)
    Me.Label3.TabIndex = 8
    Me.Label3.Text = "Tanggal Posting :"
    '
    'Label2
    '
    Me.Label2.AutoSize = True
    Me.Label2.Location = New System.Drawing.Point(189, 33)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(23, 13)
    Me.Label2.TabIndex = 7
    Me.Label2.Text = "s/d"
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Location = New System.Drawing.Point(12, 33)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(86, 13)
    Me.Label1.TabIndex = 6
    Me.Label1.Text = "Tanggal Bunga :"
    '
    'GroupBox1
    '
    Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
    Me.GroupBox1.Controls.Add(Me.lblStatusAmbil)
    Me.GroupBox1.Controls.Add(Me.lblStatusKirim)
    Me.GroupBox1.Controls.Add(Me.lblAmbil)
    Me.GroupBox1.Controls.Add(Me.pbAmbilPengajuan)
    Me.GroupBox1.Controls.Add(Me.lblKirim)
    Me.GroupBox1.Controls.Add(Me.pbKirimPengajuan)
    Me.GroupBox1.Location = New System.Drawing.Point(8, 57)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(1110, 106)
    Me.GroupBox1.TabIndex = 15
    Me.GroupBox1.TabStop = False
    Me.GroupBox1.Text = "Persetujuan Pengajuan Kredit"
    '
    'lblStatusAmbil
    '
    Me.lblStatusAmbil.BackColor = System.Drawing.Color.Transparent
    Me.lblStatusAmbil.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblStatusAmbil.Location = New System.Drawing.Point(11, 68)
    Me.lblStatusAmbil.Name = "lblStatusAmbil"
    Me.lblStatusAmbil.Size = New System.Drawing.Size(178, 23)
    Me.lblStatusAmbil.TabIndex = 13
    Me.lblStatusAmbil.Text = "Service is active"
    Me.lblStatusAmbil.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lblStatusKirim
    '
    Me.lblStatusKirim.BackColor = System.Drawing.Color.Transparent
    Me.lblStatusKirim.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblStatusKirim.Location = New System.Drawing.Point(11, 30)
    Me.lblStatusKirim.Name = "lblStatusKirim"
    Me.lblStatusKirim.Size = New System.Drawing.Size(178, 23)
    Me.lblStatusKirim.TabIndex = 12
    Me.lblStatusKirim.Text = "Service is active"
    Me.lblStatusKirim.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lblAmbil
    '
    Me.lblAmbil.BackColor = System.Drawing.Color.Transparent
    Me.lblAmbil.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblAmbil.Location = New System.Drawing.Point(758, 68)
    Me.lblAmbil.Name = "lblAmbil"
    Me.lblAmbil.Size = New System.Drawing.Size(177, 23)
    Me.lblAmbil.TabIndex = 11
    Me.lblAmbil.Text = "0 % Complete"
    Me.lblAmbil.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.lblAmbil.Visible = False
    '
    'pbAmbilPengajuan
    '
    Me.pbAmbilPengajuan.Location = New System.Drawing.Point(195, 68)
    Me.pbAmbilPengajuan.Name = "pbAmbilPengajuan"
    Me.pbAmbilPengajuan.Size = New System.Drawing.Size(557, 23)
    Me.pbAmbilPengajuan.TabIndex = 10
    Me.pbAmbilPengajuan.Visible = False
    '
    'lblKirim
    '
    Me.lblKirim.BackColor = System.Drawing.Color.Transparent
    Me.lblKirim.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblKirim.Location = New System.Drawing.Point(758, 30)
    Me.lblKirim.Name = "lblKirim"
    Me.lblKirim.Size = New System.Drawing.Size(177, 23)
    Me.lblKirim.TabIndex = 9
    Me.lblKirim.Text = "0 % Complete"
    Me.lblKirim.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.lblKirim.Visible = False
    '
    'pbKirimPengajuan
    '
    Me.pbKirimPengajuan.Location = New System.Drawing.Point(195, 30)
    Me.pbKirimPengajuan.Name = "pbKirimPengajuan"
    Me.pbKirimPengajuan.Size = New System.Drawing.Size(557, 23)
    Me.pbKirimPengajuan.TabIndex = 8
    Me.pbKirimPengajuan.Visible = False
    '
    'Label4
    '
    Me.Label4.AutoSize = True
    Me.Label4.BackColor = System.Drawing.Color.Transparent
    Me.Label4.Location = New System.Drawing.Point(5, 34)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(73, 13)
    Me.Label4.TabIndex = 14
    Me.Label4.Text = "System Time :"
    '
    'cTime
    '
    Me.cTime.Enabled = False
    Me.cTime.Location = New System.Drawing.Point(78, 30)
    Me.cTime.Name = "cTime"
    Me.cTime.Size = New System.Drawing.Size(119, 20)
    Me.cTime.TabIndex = 17
    '
    'cmdClose
    '
    Me.cmdClose.Image = Global.HeaSync_Persetujuan_Pengajuan.My.Resources.Resources.app_window_cross_icon_24
    Me.cmdClose.Location = New System.Drawing.Point(1083, 27)
    Me.cmdClose.Name = "cmdClose"
    Me.cmdClose.Size = New System.Drawing.Size(35, 32)
    Me.cmdClose.TabIndex = 19
    Me.cmdClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
    Me.cmdClose.UseVisualStyleBackColor = True
    '
    'timerLabel
    '
    Me.timerLabel.Interval = 1000
    '
    'Label5
    '
    Me.Label5.AutoSize = True
    Me.Label5.BackColor = System.Drawing.Color.Transparent
    Me.Label5.Location = New System.Drawing.Point(230, 34)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(178, 13)
    Me.Label5.TabIndex = 21
    Me.Label5.Text = "URL Persetujuan Pengajuan Kredit :"
    '
    'cURL
    '
    Me.cURL.Enabled = False
    Me.cURL.Location = New System.Drawing.Point(414, 30)
    Me.cURL.Name = "cURL"
    Me.cURL.Size = New System.Drawing.Size(346, 20)
    Me.cURL.TabIndex = 22
    '
    'lblOnline
    '
    Me.lblOnline.AutoSize = True
    Me.lblOnline.BackColor = System.Drawing.Color.Transparent
    Me.lblOnline.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblOnline.Location = New System.Drawing.Point(766, 32)
    Me.lblOnline.Name = "lblOnline"
    Me.lblOnline.Size = New System.Drawing.Size(107, 17)
    Me.lblOnline.TabIndex = 23
    Me.lblOnline.Text = "O F F L I N E "
    '
    'ShowToolStripMenuItem
    '
    Me.ShowToolStripMenuItem.Name = "ShowToolStripMenuItem"
    Me.ShowToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
    Me.ShowToolStripMenuItem.Text = "Show"
    '
    'MainForm
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(1123, 340)
    Me.ControlBox = False
    Me.Controls.Add(Me.lblOnline)
    Me.Controls.Add(Me.cURL)
    Me.Controls.Add(Me.Label5)
    Me.Controls.Add(Me.cmdClose)
    Me.Controls.Add(Me.cTime)
    Me.Controls.Add(Me.GroupBox2)
    Me.Controls.Add(Me.GroupBox1)
    Me.Controls.Add(Me.Label4)
    Me.Controls.Add(Me.MenuStrip1)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.IsMdiContainer = True
    Me.MainMenuStrip = Me.MenuStrip1
    Me.MaximizeBox = False
    Me.Name = "MainForm"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
    Me.Text = "Service Persetujuan Pengajuan & Posting Bunga Tabungan"
    Me.ContextMenuStrip1.ResumeLayout(False)
    Me.MenuStrip1.ResumeLayout(False)
    Me.MenuStrip1.PerformLayout()
    Me.GroupBox2.ResumeLayout(False)
    Me.GroupBox2.PerformLayout()
    Me.GroupBox1.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
  Friend WithEvents SettingToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents Timer1 As System.Windows.Forms.Timer
  Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
  Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents SettingToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ManualOverrideToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents ToolStripMenuItem3 As System.Windows.Forms.ToolStripSeparator
  Friend WithEvents ExitToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
  Friend WithEvents timerPersetujuan As System.Windows.Forms.Timer
  Friend WithEvents timerPostingBunga As System.Windows.Forms.Timer
  Friend WithEvents bwKirimPengajuan As System.ComponentModel.BackgroundWorker
  Friend WithEvents bwAmbilPengajuan As System.ComponentModel.BackgroundWorker
  Friend WithEvents bwPostingAdm As System.ComponentModel.BackgroundWorker
  Friend WithEvents bwPostingBunga As System.ComponentModel.BackgroundWorker
  Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
  Friend WithEvents lblNasabahBunga As System.Windows.Forms.Label
  Friend WithEvents lblNasabahAdm As System.Windows.Forms.Label
  Friend WithEvents lblStatusBunga As System.Windows.Forms.Label
  Friend WithEvents lblBunga As System.Windows.Forms.Label
  Friend WithEvents pbPostingBunga As System.Windows.Forms.ProgressBar
  Friend WithEvents lblStatusAdm As System.Windows.Forms.Label
  Friend WithEvents lblAdm As System.Windows.Forms.Label
  Friend WithEvents pbPostingAdm As System.Windows.Forms.ProgressBar
  Friend WithEvents Label3 As System.Windows.Forms.Label
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
  Friend WithEvents lblStatusAmbil As System.Windows.Forms.Label
  Friend WithEvents lblStatusKirim As System.Windows.Forms.Label
  Friend WithEvents lblAmbil As System.Windows.Forms.Label
  Friend WithEvents pbAmbilPengajuan As System.Windows.Forms.ProgressBar
  Friend WithEvents lblKirim As System.Windows.Forms.Label
  Friend WithEvents pbKirimPengajuan As System.Windows.Forms.ProgressBar
  Friend WithEvents Label4 As System.Windows.Forms.Label
  Friend WithEvents cTime As System.Windows.Forms.TextBox
  Friend WithEvents dTglPosting As System.Windows.Forms.DateTimePicker
  Friend WithEvents dTgl1 As System.Windows.Forms.DateTimePicker
  Friend WithEvents dTgl As System.Windows.Forms.DateTimePicker
  Friend WithEvents cmdClose As System.Windows.Forms.Button
  Friend WithEvents timerLabel As System.Windows.Forms.Timer
  Friend WithEvents Label5 As System.Windows.Forms.Label
  Friend WithEvents cURL As System.Windows.Forms.TextBox
  Friend WithEvents lblOnline As System.Windows.Forms.Label
  Friend WithEvents ShowToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
