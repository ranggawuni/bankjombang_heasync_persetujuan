﻿
Public Class PopupForm
  Private WithEvents Tray As NotifyIcon
  Dim lServicePersetujuanPengajuanKredit As Boolean
  Dim lServicePostingTabunganBungaAdm As Boolean
  Dim cWaktuPostingAdm As String
  Dim cWaktuPostingBunga As String
  Dim cWaktuKirimPengajuan As String
  Dim cWaktuAmbilPengajuan As String
  Dim cDuration As String
  Dim lClickClose As Boolean = False
  Dim lServiceKirimPersetujuan As Boolean = True
  Dim lServiceAmbilPersetujuan As Boolean = True
  Dim lServicePostingAdm As Boolean = True
  Dim lServicePostingBunga As Boolean = True
  Dim isServiceKirimRunning As Boolean
  Dim isServiceAmbilRunning As Boolean
  Dim isServicePostingAdmRunning As Boolean
  Dim isServicePostingBungaRunning As Boolean

  Public Sub New()
    InitializeComponent()
    Icon = My.Resources.trayicon
    isServiceKirimRunning = True
    isServiceAmbilRunning = True
    isServicePostingAdmRunning = True
    isServicePostingBungaRunning = True
  End Sub

  Private Sub CancelButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CancelFormButton.Click
    Me.DialogResult = Windows.Forms.DialogResult.Cancel
    Me.Close()
  End Sub

  Private Sub cmdClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cmdClose.Click
    If Not lServiceKirimPersetujuan Or Not lServiceAmbilPersetujuan Or Not lServicePostingAdm Or Not lServicePostingBunga Then
      If MessageBox.Show("Manual override still running. Closing this form will terminate the process. Continue?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
        If Not lServiceKirimPersetujuan Or Not lServiceAmbilPersetujuan Then
          cmdPersetujuanPengajuanKredit.PerformClick()
        End If
        If Not lServicePostingAdm Or Not lServicePostingBunga Then
          cmdPostingBungaAdm.PerformClick()
        End If
        Me.DialogResult = Windows.Forms.DialogResult.Abort
        If Not lClickClose Then
          Me.Close()
        End If
      End If
    Else
      Me.DialogResult = Windows.Forms.DialogResult.Abort
      If cmdPersetujuanPengajuanKredit.Text = "Enable" Then
        cmdPersetujuanPengajuanKredit.PerformClick()
      End If
      If cmdPostingBungaAdm.Text = "Enable" Then
        cmdPostingBungaAdm.PerformClick()
      End If
      If Not lClickClose Then
        Me.Close()
      End If
    End If
  End Sub

  Private Sub cmdKirim_Click(sender As Object, e As EventArgs) Handles cmdKirim.Click
    If cmdKirim.Text = "Kirim" Then
      If isServiceAmbilRunning Then
        MessageBox.Show("Ambil Data Persetujuan is still running. Please wait.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Exit Sub
      End If
      bwKirimPengajuan.WorkerSupportsCancellation = True
      bwKirimPengajuan.WorkerReportsProgress = True
      bwKirimPengajuan.RunWorkerAsync()
      pbKirimPengajuan.Visible = True
      lblKirim.Visible = pbKirimPengajuan.Visible
      cmdKirim.Text = "Cancel"
      cWaktuKirimPengajuan = cTime.Text
      lServiceKirimPersetujuan = False
      isServiceKirimRunning = True
    Else
      pbKirimPengajuan.Visible = True
      lblKirim.Visible = pbKirimPengajuan.Visible
      bwKirimPengajuan.CancelAsync()
      cmdKirim.Text = "Kirim"
      cWaktuKirimPengajuan = ""
      lServiceKirimPersetujuan = True
      isServiceKirimRunning = False
    End If
  End Sub

  Private Sub cmdPostingBungaAdm_Click(sender As Object, e As EventArgs) Handles cmdPostingBungaAdm.Click
    Dim x As New ClassXmlFile() With {.JenisDatabase = ClassXmlFile.enJenisDatabase.local, .Field = "service_posting_bunga"}
    If cmdPostingBungaAdm.Text = "Enable" Then
      x.Value = "0"
      cmdPostingBungaAdm.Text = "Disable"
      lServicePostingTabunganBungaAdm = True
      If cmdPostingBunga.Text = "Cancel" Then
        cmdPostingBunga.PerformClick()
      End If
      If cmdPostingAdm.Text = "Cancel" Then
        cmdPostingAdm.PerformClick()
      End If
      lblStatusAdm.Text = "Service is active"
      lblStatusBunga.Text = "Service is active"
      lServicePostingAdm = True
      lServicePostingBunga = True
    Else
      x.Value = "1"
      cmdPostingBungaAdm.Text = "Enable"
      lServicePostingTabunganBungaAdm = False
      lblStatusAdm.Text = "Idle"
      lblStatusBunga.Text = "Idle"
      lServicePostingAdm = False
      lServicePostingBunga = False
    End If
    cmdPostingAdm.Enabled = Not lServicePostingTabunganBungaAdm
    cmdPostingBunga.Enabled = Not lServicePostingTabunganBungaAdm
    isServicePostingAdmRunning = lServicePostingTabunganBungaAdm
    isServicePostingBungaRunning = lServicePostingTabunganBungaAdm
    x.EditValue()
  End Sub

  Private Sub PopupForm_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
    lClickClose = True
    cmdClose.PerformClick()
  End Sub

  Private Sub PopupForm_Load(sender As Object, e As EventArgs) Handles Me.Load
    Dim x As New ClassXmlFile() With {.JenisDatabase = ClassXmlFile.enJenisDatabase.local, .Field = "tgl_bunga"}
    Dim nHariBunga As Integer = CInt(x.GetValue)
    x.Field = "tgl_bunga1"
    Dim nHariBunga1 As Integer = CInt(x.GetValue)
    x.Field = "tgl_posting"
    Dim nHariPosting As Integer = CInt(x.GetValue)
    Dim nBulan As Integer = Today.Month
    'If Debugger.IsAttached Then
    '  nBulan = 6
    'End If
    Dim dTglTemp As Date = DateSerial(Today.Year, nBulan - 1, nHariBunga)
    dTgl.Value = dTglTemp
    Dim dTglTemp1 As Date = DateSerial(Today.Year, nBulan, nHariBunga1)
    dTgl1.Value = dTglTemp1
    x.Field = "jam_posting"
    Dim cJamPosting As String = x.GetValue
    Dim dTglPostingTemp As Date = DateSerial(Today.Year, nBulan, nHariPosting)
    Dim cWaktu As String = String.Format("{0} {1}", FormatDateTime(dTglPostingTemp, DateFormat.ShortDate), cJamPosting)
    dTglTemp = DateSerial(Today.Year, nBulan, nHariPosting)
    dTglPosting.Value = CDate(cWaktu)
    Tray = CType(MainForm.cTray, NotifyIcon)
    Timer1.Interval = 1000
    Timer1.Enabled = True

    If CekServicePersetujuanPengajuan() Then
      cmdPersetujuanPengajuanKredit.Text = "Disable"
      lServicePersetujuanPengajuanKredit = True
      lblStatusAmbil.Text = "Service is active"
      lblStatusKirim.Text = "Service is active"
    Else
      cmdPersetujuanPengajuanKredit.Text = "Enable"
      lServicePersetujuanPengajuanKredit = False
      lblStatusAmbil.Text = "Idle"
      lblStatusKirim.Text = "Idle"
    End If
    cmdKirim.Enabled = Not lServicePersetujuanPengajuanKredit
    cmdAmbil.Enabled = Not lServicePersetujuanPengajuanKredit
    isServiceKirimRunning = lServicePersetujuanPengajuanKredit
    isServiceAmbilRunning = lServicePersetujuanPengajuanKredit
    Timer2.Enabled = True
    Timer3.Enabled = True
    If CekServicePostingBunga() Then
      cmdPostingBungaAdm.Text = "Disable"
      lServicePostingTabunganBungaAdm = True
      lblStatusBunga.Text = "Service is active"
      lblStatusAdm.Text = "Service is active"
    Else
      cmdPostingBungaAdm.Text = "Enable"
      lServicePostingTabunganBungaAdm = False
      lblStatusBunga.Text = "Idle"
      lblStatusAdm.Text = "Idle"
    End If
    cmdPostingAdm.Enabled = Not lServicePostingTabunganBungaAdm
    cmdPostingBunga.Enabled = Not lServicePostingTabunganBungaAdm
    isServicePostingAdmRunning = lServicePostingTabunganBungaAdm
    isServicePostingBungaRunning = lServicePostingTabunganBungaAdm
  End Sub

  Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
    cTime.Text = Now.ToString
    'If CDate(cTime.Text).Day = dTglPosting.Value.Day And FormatDateTime(CDate(cTime.Text), DateFormat.LongTime) = FormatDateTime(dTglPosting.Value, DateFormat.LongTime) Then
    '  MessageBox.Show("Posting")
    'End If
  End Sub

  Private Sub cmdAmbil_Click(sender As Object, e As EventArgs) Handles cmdAmbil.Click
    If cmdAmbil.Text = "Ambil" Then
      If isServiceKirimRunning Then
        MessageBox.Show("Kirim Data Persetujuan is still running. Please wait.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Exit Sub
      End If
      bwAmbilPengajuan.WorkerSupportsCancellation = True
      bwAmbilPengajuan.WorkerReportsProgress = True
      bwAmbilPengajuan.RunWorkerAsync()
      pbAmbilPengajuan.Visible = True
      lblAmbil.Visible = pbAmbilPengajuan.Visible
      cmdAmbil.Text = "Cancel"
      cWaktuAmbilPengajuan = cTime.Text
      lServiceAmbilPersetujuan = False
      isServiceAmbilRunning = True
    Else
      bwAmbilPengajuan.CancelAsync()
      pbAmbilPengajuan.Visible = True
      lblAmbil.Visible = pbAmbilPengajuan.Visible
      cmdAmbil.Text = "Ambil"
      cWaktuAmbilPengajuan = ""
      lServiceAmbilPersetujuan = True
      isServiceAmbilRunning = False
    End If
  End Sub

  Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
    If lServicePersetujuanPengajuanKredit Then
      Label5.Text = "Persetujuan Pengajuan Kredit service is active. Disable to manual override"
    Else
      Label5.Text = "Persetujuan Pengajuan Kredit service is inactive."
    End If
    If Label5.ForeColor = Color.Black Then
      Label5.ForeColor = Color.CornflowerBlue
    Else
      Label5.ForeColor = Color.Black
    End If
  End Sub

  Private Sub cmdPersetujuanPengajuanKredit_Click(sender As Object, e As EventArgs) Handles cmdPersetujuanPengajuanKredit.Click
    Dim x As New ClassXmlFile() With {.JenisDatabase = ClassXmlFile.enJenisDatabase.local, .Field = "service_persetujuan_pengajuan"}
    If cmdPersetujuanPengajuanKredit.Text = "Enable" Then
      x.Value = "0"
      cmdPersetujuanPengajuanKredit.Text = "Disable"
      lServicePersetujuanPengajuanKredit = True
      If cmdAmbil.Text = "Cancel" Then
        cmdAmbil.PerformClick()
      End If
      If cmdKirim.Text = "Cancel" Then
        cmdKirim.PerformClick()
      End If
      lblStatusAmbil.Text = "Service is active"
      lblStatusKirim.Text = "Service is active"
      lServiceKirimPersetujuan = True
      lServiceAmbilPersetujuan = True
    Else
      x.Value = "1"
      cmdPersetujuanPengajuanKredit.Text = "Enable"
      lServicePersetujuanPengajuanKredit = False
      lblStatusAmbil.Text = "Idle"
      lblStatusKirim.Text = "Idle"
      lServiceKirimPersetujuan = False
      lServiceAmbilPersetujuan = False
    End If
    cmdKirim.Enabled = Not lServicePersetujuanPengajuanKredit
    cmdAmbil.Enabled = Not lServicePersetujuanPengajuanKredit
    isServiceAmbilRunning = lServicePersetujuanPengajuanKredit
    isServiceKirimRunning = lServicePersetujuanPengajuanKredit
    x.EditValue()
  End Sub

  Private Sub bwKirimPengajuan_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bwKirimPengajuan.DoWork
    Threading.Thread.Sleep(1000)
    isServiceKirimRunning = True
    Dim classPersetujuanPengajuan As New ClassPersetujuanPengajuan
    classPersetujuanPengajuan.KirimDataPengajuan(bwKirimPengajuan, e)

    ' '' set the e.Cancel to True to indicate to the RunWorkerCompleted that you cancelled out
    If bwKirimPengajuan.CancellationPending Then
      lServiceKirimPersetujuan = True
      e.Cancel = True
      bwKirimPengajuan.ReportProgress(100, "Cancelled.")
    End If
  End Sub

  Private Sub bwKirimPengajuan_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bwKirimPengajuan.ProgressChanged
    ' This event is fired when you call the ReportProgress method from inside your DoWork.
    ' Any visual indicators about the progress should go here.
    pbKirimPengajuan.Value = e.ProgressPercentage
    Dim cTemp As String = CType(e.UserState, String)
    If cTemp.Substring(0, 1) = "R" Then
      Dim cResult As String() = cTemp.Split(CChar("~"))
      lblStatusKirim.Text = cResult(0)
    Else
      lblStatusKirim.Text = cTemp
    End If
    lblKirim.Text = e.ProgressPercentage.ToString & "% complete."
  End Sub

  Private Sub bwKirimPengajuan_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bwKirimPengajuan.RunWorkerCompleted
    '' This event is fired when your BackgroundWorker exits.
    '' It may have exitted Normally after completing its task, 
    '' or because of Cancellation, or due to any Error.
    lblKirim.Text = ""
    If e.Error IsNot Nothing Then
      '' if BackgroundWorker terminated due to error
      MessageBox.Show(e.Error.Message)
      lblStatusKirim.Text = "Error occurred!"
    ElseIf e.Cancelled Then
      '' otherwise if it was cancelled
      lblStatusKirim.Text = "Task Cancelled!"
    Else
      '' otherwise it completed normally
      lblStatusKirim.Text = "Task completed!"
    End If
    lServiceKirimPersetujuan = True
    pbKirimPengajuan.Value = 0
    pbKirimPengajuan.Visible = False
    lblKirim.Visible = pbKirimPengajuan.Visible
    cmdKirim.Text = "Kirim"
    If cmdAmbil.Text = "Kirim" And cmdPersetujuanPengajuanKredit.Text = "Disable" Then
      lblStatusAmbil.Text = "Service is active"
    End If
    isServiceKirimRunning = False
  End Sub

  Private Sub Timer3_Tick(sender As Object, e As EventArgs) Handles Timer3.Tick
    If lServicePostingTabunganBungaAdm Then
      Label7.Text = "Posting Bunga service is active. Disable to manual override"
    Else
      Label7.Text = "Posting Bunga service is inactive."
    End If
    If Label7.ForeColor = Color.HotPink Then
      Label7.ForeColor = Color.Black
    Else
      Label7.ForeColor = Color.HotPink
    End If
  End Sub

  Private Sub bwAmbilPengajuan_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bwAmbilPengajuan.DoWork
    Threading.Thread.Sleep(1000)
    isServiceAmbilRunning = True
    Dim classPersetujuanPengajuan As New ClassPersetujuanPengajuan
    classPersetujuanPengajuan.AmbilDataPengajuan(bwAmbilPengajuan, e)

    ' '' set the e.Cancel to True to indicate to the RunWorkerCompleted that you cancelled out
    If bwAmbilPengajuan.CancellationPending Then
      lServiceAmbilPersetujuan = False
      e.Cancel = True
      bwAmbilPengajuan.ReportProgress(100, "Cancelled.")
    End If
  End Sub

  Private Sub bwAmbilPengajuan_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bwAmbilPengajuan.ProgressChanged
    ' This event is fired when you call the ReportProgress method from inside your DoWork.
    ' Any visual indicators about the progress should go here.
    pbAmbilPengajuan.Value = e.ProgressPercentage
    Dim cTemp As String = CType(e.UserState, String)
    If cTemp.Substring(0, 1) = "R" Then
      Dim cResult As String() = cTemp.Split(CChar("~"))
      lblStatusAmbil.Text = cResult(0)
    Else
      lblStatusAmbil.Text = cTemp
    End If
    lblAmbil.Text = e.ProgressPercentage.ToString & "% complete."
  End Sub

  Private Sub bwAmbilPengajuan_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bwAmbilPengajuan.RunWorkerCompleted
    '' This event is fired when your BackgroundWorker exits.
    '' It may have exitted Normally after completing its task, 
    '' or because of Cancellation, or due to any Error.
    lblAmbil.Text = ""
    If e.Error IsNot Nothing Then
      '' if BackgroundWorker terminated due to error
      MessageBox.Show(e.Error.Message)
      lblStatusAmbil.Text = "Error occurred!"
    ElseIf e.Cancelled Then
      '' otherwise if it was cancelled
      lblStatusAmbil.Text = "Task Cancelled!"
    Else
      '' otherwise it completed normally
      lblStatusAmbil.Text = "Task completed!"
    End If
    lServiceAmbilPersetujuan = True
    pbAmbilPengajuan.Value = 0
    pbAmbilPengajuan.Visible = False
    lblAmbil.Visible = pbAmbilPengajuan.Visible
    cmdAmbil.Text = "Ambil"
    If cmdAmbil.Text = "Ambil" And cmdPersetujuanPengajuanKredit.Text = "Disable" Then
      lblStatusAmbil.Text = "Service is active"
    End If
    isServiceAmbilRunning = False
  End Sub

  Private Sub bwPostingAdm_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bwPostingAdm.DoWork
    Threading.Thread.Sleep(1000)
    isServicePostingAdmRunning = True
    Dim classPosting As New ClassPosting() With {.TanggalPosting = dTglPosting.Value,
                                                .TanggalBungaAwal = dTgl.Value,
                                                .TanggalBungaAkhir = dTgl1.Value,
                                                .TrayApp = Tray}
    classPosting.PostingAdm(bwPostingAdm, e)

    ' '' set the e.Cancel to True to indicate to the RunWorkerCompleted that you cancelled out
    If bwPostingAdm.CancellationPending Then
      lServicePostingAdm = False
      e.Cancel = True
      bwPostingAdm.ReportProgress(100, "Cancelled.")
    End If
  End Sub

  <System.Diagnostics.CodeAnalysis.SuppressMessage("CodeRush", "String.Format can be used")>
  Private Sub bwPostingAdm_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bwPostingAdm.ProgressChanged
    ' This event is fired when you call the ReportProgress method from inside your DoWork.
    ' Any visual indicators about the progress should go here.
    pbPostingAdm.Value = e.ProgressPercentage
    Dim cTemp As String = CType(e.UserState, String)
    If cTemp.Substring(0, 1) = "R" Then
      Dim cResult As String() = cTemp.Split(CChar("~"))
      lblStatusAdm.Text = cResult(0)
      lblNasabahAdm.Text = cResult(1)
    Else
      lblStatusAdm.Text = cTemp
    End If
    lblAdm.Text = e.ProgressPercentage.ToString & "% complete."
  End Sub


  Private Sub bwPostingAdm_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bwPostingAdm.RunWorkerCompleted
    '' This event is fired when your BackgroundWorker exits.
    '' It may have exitted Normally after completing its task, 
    '' or because of Cancellation, or due to any Error.
    lblAdm.Text = ""
    If e.Error IsNot Nothing Then
      '' if BackgroundWorker terminated due to error
      MessageBox.Show(e.Error.Message)
      lblStatusAdm.Text = "Error occurred!"
    ElseIf e.Cancelled Then
      '' otherwise if it was cancelled
      lblStatusAdm.Text = "Task Cancelled!"
    Else
      '' otherwise it completed normally
      lblStatusAdm.Text = "Task completed! - " & cDuration
    End If
    lServicePostingAdm = True
    pbPostingAdm.Value = 0
    pbPostingAdm.Visible = False
    lblAdm.Visible = pbPostingAdm.Visible
    lblNasabahAdm.Visible = pbPostingAdm.Visible
    lblNasabahAdm.Text = ""
    cmdPostingAdm.Text = "Administrasi"
    If cmdPostingAdm.Text = "Administrasi" And cmdPostingBungaAdm.Text = "Disable" Then
      lblStatusAdm.Text = "Service is active"
    End If
    isServicePostingAdmRunning = False
  End Sub

  Private Sub bwPostingBunga_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bwPostingBunga.DoWork
    Threading.Thread.Sleep(1000)
    isServicePostingBungaRunning = True
    Dim classPosting As New ClassPosting() With {.TanggalPosting = dTglPosting.Value,
                                                 .TanggalBungaAwal = dTgl.Value,
                                                 .TanggalBungaAkhir = dTgl1.Value,
                                                 .TrayApp = Tray}
    classPosting.PostingBunga(bwPostingBunga, e)

    ' '' set the e.Cancel to True to indicate to the RunWorkerCompleted that you cancelled out
    If bwPostingBunga.CancellationPending Then
      lServicePostingBunga = False
      e.Cancel = True
      bwPostingBunga.ReportProgress(100, "Cancelled.")
    End If
  End Sub

  Private Sub bwPostingBunga_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bwPostingBunga.ProgressChanged
    ' This event is fired when you call the ReportProgress method from inside your DoWork.
    ' Any visual indicators about the progress should go here.
    pbPostingBunga.Value = e.ProgressPercentage
    Dim cTemp As String = CType(e.UserState, String)
    If cTemp.Substring(0, 1) = "R" Then
      Dim cResult As String() = cTemp.Split(CChar("~"))
      lblStatusBunga.Text = cResult(0)
      lblNasabahBunga.Text = cResult(1)
    Else
      lblStatusBunga.Text = cTemp
    End If
    lblBunga.Text = e.ProgressPercentage.ToString & "% complete."
  End Sub

  Private Sub bwPostingBunga_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bwPostingBunga.RunWorkerCompleted
    '' This event is fired when your BackgroundWorker exits.
    '' It may have exitted Normally after completing its task, 
    '' or because of Cancellation, or due to any Error.
    lblBunga.Text = ""
    If e.Error IsNot Nothing Then
      '' if BackgroundWorker terminated due to error
      MessageBox.Show(e.Error.Message)
      lblStatusBunga.Text = "Error occurred!"
    ElseIf e.Cancelled Then
      '' otherwise if it was cancelled
      lblStatusBunga.Text = "Task Cancelled!"
    Else
      '' otherwise it completed normally
      lblStatusBunga.Text = "Task completed!"
    End If
    lServicePostingBunga = True
    pbPostingBunga.Value = 0
    pbPostingBunga.Visible = False
    lblBunga.Visible = pbPostingBunga.Visible
    lblNasabahBunga.Visible = pbPostingBunga.Visible
    cmdPostingBunga.Text = "Bunga"
    If cmdPostingBunga.Text = "Bunga" And cmdPostingBungaAdm.Text = "Disable" Then
      lblStatusBunga.Text = "Service is active"
    End If
    isServicePostingBungaRunning = False
  End Sub

  Private Sub cmdPostingAdm_Click(sender As Object, e As EventArgs) Handles cmdPostingAdm.Click
    If cmdPostingAdm.Text = "Administrasi" Then
      If isServicePostingBungaRunning Then
        MessageBox.Show("Posting Bunga is still running. Please wait.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Exit Sub
      End If
      bwPostingAdm.WorkerSupportsCancellation = True
      bwPostingAdm.WorkerReportsProgress = True
      bwPostingAdm.RunWorkerAsync()
      pbPostingAdm.Visible = True
      lblAdm.Visible = pbPostingAdm.Visible
      lblNasabahAdm.Visible = pbPostingAdm.Visible
      cmdPostingAdm.Text = "Cancel"
      cWaktuPostingAdm = cTime.Text
      lServicePostingAdm = False
      isServicePostingAdmRunning = True
    Else
      pbPostingAdm.Visible = True
      lblAdm.Visible = pbPostingAdm.Visible
      lblNasabahAdm.Visible = pbPostingAdm.Visible
      bwPostingAdm.CancelAsync()
      cmdPostingAdm.Text = "Administrasi"
      cWaktuPostingAdm = ""
      lServicePostingAdm = True
      isServicePostingAdmRunning = False
    End If
  End Sub

  Private Sub cmdPostingBunga_Click(sender As Object, e As EventArgs) Handles cmdPostingBunga.Click
    If cmdPostingBunga.Text = "Bunga" Then
      If isServicePostingAdmRunning Then
        MessageBox.Show("Posting Administrasi is still running. Please wait.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Exit Sub
      End If
      bwPostingBunga.WorkerSupportsCancellation = True
      bwPostingBunga.WorkerReportsProgress = True
      bwPostingBunga.RunWorkerAsync()
      pbPostingBunga.Visible = True
      lblBunga.Visible = pbPostingBunga.Visible
      lblNasabahBunga.Visible = pbPostingBunga.Visible
      cmdPostingBunga.Text = "Cancel"
      cWaktuPostingBunga = cTime.Text
      lServicePostingBunga = False
      isServicePostingBungaRunning = True
    Else
      pbPostingBunga.Visible = True
      lblBunga.Visible = pbPostingBunga.Visible
      lblNasabahBunga.Visible = pbPostingBunga.Visible
      bwPostingBunga.CancelAsync()
      cmdPostingBunga.Text = "Bunga"
      cWaktuPostingBunga = ""
      lServicePostingBunga = True
      isServicePostingBungaRunning = False
    End If
  End Sub
End Class