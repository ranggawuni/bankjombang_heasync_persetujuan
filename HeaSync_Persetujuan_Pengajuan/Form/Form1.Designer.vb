﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.button1 = New System.Windows.Forms.Button()
    Me.button2 = New System.Windows.Forms.Button()
    Me.Label1 = New System.Windows.Forms.Label()
    Me.Label2 = New System.Windows.Forms.Label()
    Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
    Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
    Me.SuspendLayout()
    '
    'button1
    '
    Me.button1.Location = New System.Drawing.Point(114, 116)
    Me.button1.Name = "button1"
    Me.button1.Size = New System.Drawing.Size(75, 23)
    Me.button1.TabIndex = 0
    Me.button1.Text = "start"
    Me.button1.UseVisualStyleBackColor = True
    '
    'button2
    '
    Me.button2.Location = New System.Drawing.Point(195, 116)
    Me.button2.Name = "button2"
    Me.button2.Size = New System.Drawing.Size(75, 23)
    Me.button2.TabIndex = 1
    Me.button2.Text = "cancel"
    Me.button2.UseVisualStyleBackColor = True
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Location = New System.Drawing.Point(12, 15)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(60, 13)
    Me.Label1.TabIndex = 6
    Me.Label1.Text = "Source File"
    '
    'Label2
    '
    Me.Label2.AutoSize = True
    Me.Label2.BackColor = System.Drawing.Color.Transparent
    Me.Label2.Location = New System.Drawing.Point(42, 81)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(79, 13)
    Me.Label2.TabIndex = 7
    Me.Label2.Text = "Compare String"
    '
    'BackgroundWorker1
    '
    Me.BackgroundWorker1.WorkerReportsProgress = True
    Me.BackgroundWorker1.WorkerSupportsCancellation = True
    '
    'ProgressBar1
    '
    Me.ProgressBar1.Location = New System.Drawing.Point(15, 71)
    Me.ProgressBar1.Name = "ProgressBar1"
    Me.ProgressBar1.Size = New System.Drawing.Size(572, 23)
    Me.ProgressBar1.TabIndex = 8
    '
    'Form1
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(731, 150)
    Me.Controls.Add(Me.Label2)
    Me.Controls.Add(Me.ProgressBar1)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.button2)
    Me.Controls.Add(Me.button1)
    Me.Name = "Form1"
    Me.Text = "Form1"
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents button1 As System.Windows.Forms.Button
  Friend WithEvents button2 As System.Windows.Forms.Button
  Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
  Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar

End Class
