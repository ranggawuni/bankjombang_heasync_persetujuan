﻿Public Class FrmConfig

  Private Sub LoadData()
    Dim x As New ClassXmlFile
    With x
      .JenisDatabase = ClassXmlFile.enJenisDatabase.local
      .Field = "databasename"
      cDatabase.Text = .GetValue

      .Field = "ip"
      cIP.Text = .GetValue

      .Field = "port"
      cPort.Text = .GetValue

      .Field = "tgl_bunga"
      cTglBunga.Text = .GetValue

      .Field = "tgl_bunga1"
      cTglBunga1.Text = .GetValue

      .Field = "tgl_posting"
      cTglPosting.Text = .GetValue

      .Field = "jam_posting"
      cWaktuPosting.Text = .GetValue

      .Field = "interval_persetujuan"
      cInterval.Text = .GetValue

      .Field = "kode_kantor"
      cKodeKantorInduk.Text = .GetValue

      .Field = "service_persetujuan_pengajuan"
      If .GetValue = "0" Then
        optServicePengajuan1.Checked = True
      Else
        optServicePengajuan2.Checked = True
      End If

      .Field = "service_posting_bunga"
      If .GetValue = "0" Then
        optServicePostingBungaTabungan1.Checked = True
      Else
        optServicePostingBungaTabungan2.Checked = True
      End If

      .JenisDatabase = ClassXmlFile.enJenisDatabase.remote
      .Field = "ip"
      cURL.Text = .GetValue
    End With
  End Sub

  Private Sub LoadPath()
    cPath.Text = My.Settings.appPath
  End Sub

  Private Sub cmdSimpan_Click(sender As Object, e As EventArgs) Handles cmdSimpan.Click
    Dim x As New ClassXmlFile()
    With x
      .JenisDatabase = ClassXmlFile.enJenisDatabase.local

      .Field = "databasename"
      .Value = cDatabase.Text
      .EditValue()

      .Field = "ip"
      .Value = cIP.Text
      .EditValue()

      .Field = "port"
      .Value = cPort.Text
      .EditValue()

      .Field = "tgl_bunga"
      .Value = cTglBunga.Text
      .EditValue()

      .Field = "tgl_bunga1"
      .Value = cTglBunga1.Text
      .EditValue()

      .Field = "tgl_posting"
      .Value = cTglPosting.Text
      .EditValue()

      .Field = "jam_posting"
      .Value = cWaktuPosting.Text
      .EditValue()

      .Field = "service_persetujuan_pengajuan"
      If optServicePengajuan1.Checked Then
        .Value = "0"
      ElseIf optServicePengajuan2.Checked Then
        .Value = "1"
      End If
      .EditValue()

      .Field = "interval_persetujuan"
      .Value = cInterval.Text
      .EditValue()

      .Field = "kode_kantor"
      .Value = cKodeKantorInduk.Text
      .EditValue()

      .Field = "service_posting_bunga"
      If optServicePostingBungaTabungan1.Checked Then
        .Value = "0"
      ElseIf optServicePostingBungaTabungan2.Checked Then
        .Value = "1"
      End If
      .EditValue()

      .JenisDatabase = ClassXmlFile.enJenisDatabase.remote
      .Field = "ip"
      .Value = cURL.Text
      .EditValue()
    End With
    My.Settings.Item("appPath") = cPath.Text
    My.Settings.Save()
    LoadData()
    MessageBox.Show("Data saved.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
  End Sub

  Private Sub FrmConfig_Load(sender As Object, e As EventArgs) Handles Me.Load
    LoadPath()
    LoadData()
  End Sub

  Private Sub cmdKeluar_Click(sender As Object, e As EventArgs) Handles cmdKeluar.Click
    Close()
  End Sub
End Class