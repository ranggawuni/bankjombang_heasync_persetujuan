﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PopupForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Me.CancelFormButton = New System.Windows.Forms.Button()
    Me.cmdClose = New System.Windows.Forms.Button()
    Me.cmdKirim = New System.Windows.Forms.Button()
    Me.dTgl = New System.Windows.Forms.DateTimePicker()
    Me.dTgl1 = New System.Windows.Forms.DateTimePicker()
    Me.dTglPosting = New System.Windows.Forms.DateTimePicker()
    Me.Label1 = New System.Windows.Forms.Label()
    Me.Label2 = New System.Windows.Forms.Label()
    Me.Label3 = New System.Windows.Forms.Label()
    Me.cmdPostingBungaAdm = New System.Windows.Forms.Button()
    Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
    Me.cTime = New System.Windows.Forms.TextBox()
    Me.Label4 = New System.Windows.Forms.Label()
    Me.GroupBox1 = New System.Windows.Forms.GroupBox()
    Me.lblStatusAmbil = New System.Windows.Forms.Label()
    Me.lblStatusKirim = New System.Windows.Forms.Label()
    Me.lblAmbil = New System.Windows.Forms.Label()
    Me.pbAmbilPengajuan = New System.Windows.Forms.ProgressBar()
    Me.lblKirim = New System.Windows.Forms.Label()
    Me.pbKirimPengajuan = New System.Windows.Forms.ProgressBar()
    Me.cmdPersetujuanPengajuanKredit = New System.Windows.Forms.Button()
    Me.Label5 = New System.Windows.Forms.Label()
    Me.cmdAmbil = New System.Windows.Forms.Button()
    Me.GroupBox2 = New System.Windows.Forms.GroupBox()
    Me.lblNasabahBunga = New System.Windows.Forms.Label()
    Me.lblNasabahAdm = New System.Windows.Forms.Label()
    Me.lblStatusBunga = New System.Windows.Forms.Label()
    Me.lblBunga = New System.Windows.Forms.Label()
    Me.pbPostingBunga = New System.Windows.Forms.ProgressBar()
    Me.lblStatusAdm = New System.Windows.Forms.Label()
    Me.cmdPostingBunga = New System.Windows.Forms.Button()
    Me.cmdPostingAdm = New System.Windows.Forms.Button()
    Me.lblAdm = New System.Windows.Forms.Label()
    Me.pbPostingAdm = New System.Windows.Forms.ProgressBar()
    Me.Label7 = New System.Windows.Forms.Label()
    Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
    Me.bwKirimPengajuan = New System.ComponentModel.BackgroundWorker()
    Me.Timer3 = New System.Windows.Forms.Timer(Me.components)
    Me.bwAmbilPengajuan = New System.ComponentModel.BackgroundWorker()
    Me.bwPostingAdm = New System.ComponentModel.BackgroundWorker()
    Me.bwPostingBunga = New System.ComponentModel.BackgroundWorker()
    Me.GroupBox1.SuspendLayout()
    Me.GroupBox2.SuspendLayout()
    Me.SuspendLayout()
    '
    'CancelFormButton
    '
    Me.CancelFormButton.Location = New System.Drawing.Point(575, 7)
    Me.CancelFormButton.Name = "CancelFormButton"
    Me.CancelFormButton.Size = New System.Drawing.Size(113, 27)
    Me.CancelFormButton.TabIndex = 6
    Me.CancelFormButton.Text = "Cancel"
    Me.CancelFormButton.UseVisualStyleBackColor = True
    Me.CancelFormButton.Visible = False
    '
    'cmdClose
    '
    Me.cmdClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.cmdClose.Location = New System.Drawing.Point(694, 7)
    Me.cmdClose.Name = "cmdClose"
    Me.cmdClose.Size = New System.Drawing.Size(113, 27)
    Me.cmdClose.TabIndex = 7
    Me.cmdClose.Text = "&Close"
    Me.cmdClose.UseVisualStyleBackColor = True
    '
    'cmdKirim
    '
    Me.cmdKirim.Location = New System.Drawing.Point(6, 60)
    Me.cmdKirim.Name = "cmdKirim"
    Me.cmdKirim.Size = New System.Drawing.Size(113, 35)
    Me.cmdKirim.TabIndex = 1
    Me.cmdKirim.Text = "Kirim"
    Me.cmdKirim.UseVisualStyleBackColor = True
    '
    'dTgl
    '
    Me.dTgl.CustomFormat = "dd"
    Me.dTgl.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.dTgl.Location = New System.Drawing.Point(98, 29)
    Me.dTgl.Margin = New System.Windows.Forms.Padding(4)
    Me.dTgl.Name = "dTgl"
    Me.dTgl.Size = New System.Drawing.Size(49, 20)
    Me.dTgl.TabIndex = 2
    '
    'dTgl1
    '
    Me.dTgl1.CustomFormat = "dd"
    Me.dTgl1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.dTgl1.Location = New System.Drawing.Point(182, 29)
    Me.dTgl1.Name = "dTgl1"
    Me.dTgl1.Size = New System.Drawing.Size(56, 20)
    Me.dTgl1.TabIndex = 3
    '
    'dTglPosting
    '
    Me.dTglPosting.CustomFormat = "dd HH:mm:ss"
    Me.dTglPosting.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.dTglPosting.Location = New System.Drawing.Point(374, 29)
    Me.dTglPosting.Name = "dTglPosting"
    Me.dTglPosting.Size = New System.Drawing.Size(104, 20)
    Me.dTglPosting.TabIndex = 4
    '
    'Label1
    '
    Me.Label1.AutoSize = True
    Me.Label1.Location = New System.Drawing.Point(12, 33)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(80, 13)
    Me.Label1.TabIndex = 6
    Me.Label1.Text = "Tanggal Bunga"
    '
    'Label2
    '
    Me.Label2.AutoSize = True
    Me.Label2.Location = New System.Drawing.Point(153, 33)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(23, 13)
    Me.Label2.TabIndex = 7
    Me.Label2.Text = "s/d"
    '
    'Label3
    '
    Me.Label3.AutoSize = True
    Me.Label3.Location = New System.Drawing.Point(288, 33)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(84, 13)
    Me.Label3.TabIndex = 8
    Me.Label3.Text = "Tanggal Posting"
    '
    'cmdPostingBungaAdm
    '
    Me.cmdPostingBungaAdm.Location = New System.Drawing.Point(6, 56)
    Me.cmdPostingBungaAdm.Name = "cmdPostingBungaAdm"
    Me.cmdPostingBungaAdm.Size = New System.Drawing.Size(113, 35)
    Me.cmdPostingBungaAdm.TabIndex = 5
    Me.cmdPostingBungaAdm.Text = "Enable"
    Me.cmdPostingBungaAdm.UseVisualStyleBackColor = True
    '
    'Timer1
    '
    '
    'cTime
    '
    Me.cTime.Enabled = False
    Me.cTime.Location = New System.Drawing.Point(91, 7)
    Me.cTime.Name = "cTime"
    Me.cTime.Size = New System.Drawing.Size(119, 20)
    Me.cTime.TabIndex = 10
    '
    'Label4
    '
    Me.Label4.AutoSize = True
    Me.Label4.Location = New System.Drawing.Point(5, 10)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(67, 13)
    Me.Label4.TabIndex = 11
    Me.Label4.Text = "System Time"
    '
    'GroupBox1
    '
    Me.GroupBox1.Controls.Add(Me.lblStatusAmbil)
    Me.GroupBox1.Controls.Add(Me.lblStatusKirim)
    Me.GroupBox1.Controls.Add(Me.lblAmbil)
    Me.GroupBox1.Controls.Add(Me.pbAmbilPengajuan)
    Me.GroupBox1.Controls.Add(Me.lblKirim)
    Me.GroupBox1.Controls.Add(Me.pbKirimPengajuan)
    Me.GroupBox1.Controls.Add(Me.cmdPersetujuanPengajuanKredit)
    Me.GroupBox1.Controls.Add(Me.Label5)
    Me.GroupBox1.Controls.Add(Me.cmdAmbil)
    Me.GroupBox1.Controls.Add(Me.cmdKirim)
    Me.GroupBox1.Location = New System.Drawing.Point(8, 33)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(799, 148)
    Me.GroupBox1.TabIndex = 12
    Me.GroupBox1.TabStop = False
    Me.GroupBox1.Text = "Persetujuan Pengajuan Kredit"
    '
    'lblStatusAmbil
    '
    Me.lblStatusAmbil.BackColor = System.Drawing.Color.Transparent
    Me.lblStatusAmbil.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblStatusAmbil.Location = New System.Drawing.Point(125, 107)
    Me.lblStatusAmbil.Name = "lblStatusAmbil"
    Me.lblStatusAmbil.Size = New System.Drawing.Size(154, 23)
    Me.lblStatusAmbil.TabIndex = 13
    Me.lblStatusAmbil.Text = "Idle"
    Me.lblStatusAmbil.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lblStatusKirim
    '
    Me.lblStatusKirim.BackColor = System.Drawing.Color.Transparent
    Me.lblStatusKirim.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblStatusKirim.Location = New System.Drawing.Point(126, 66)
    Me.lblStatusKirim.Name = "lblStatusKirim"
    Me.lblStatusKirim.Size = New System.Drawing.Size(153, 23)
    Me.lblStatusKirim.TabIndex = 12
    Me.lblStatusKirim.Text = "Idle"
    Me.lblStatusKirim.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lblAmbil
    '
    Me.lblAmbil.BackColor = System.Drawing.Color.Transparent
    Me.lblAmbil.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblAmbil.Location = New System.Drawing.Point(670, 107)
    Me.lblAmbil.Name = "lblAmbil"
    Me.lblAmbil.Size = New System.Drawing.Size(129, 23)
    Me.lblAmbil.TabIndex = 11
    Me.lblAmbil.Text = "0 % Complete"
    Me.lblAmbil.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.lblAmbil.Visible = False
    '
    'pbAmbilPengajuan
    '
    Me.pbAmbilPengajuan.Location = New System.Drawing.Point(285, 107)
    Me.pbAmbilPengajuan.Name = "pbAmbilPengajuan"
    Me.pbAmbilPengajuan.Size = New System.Drawing.Size(379, 23)
    Me.pbAmbilPengajuan.TabIndex = 10
    Me.pbAmbilPengajuan.Visible = False
    '
    'lblKirim
    '
    Me.lblKirim.BackColor = System.Drawing.Color.Transparent
    Me.lblKirim.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblKirim.Location = New System.Drawing.Point(670, 66)
    Me.lblKirim.Name = "lblKirim"
    Me.lblKirim.Size = New System.Drawing.Size(129, 23)
    Me.lblKirim.TabIndex = 9
    Me.lblKirim.Text = "0 % Complete"
    Me.lblKirim.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    Me.lblKirim.Visible = False
    '
    'pbKirimPengajuan
    '
    Me.pbKirimPengajuan.Location = New System.Drawing.Point(285, 66)
    Me.pbKirimPengajuan.Name = "pbKirimPengajuan"
    Me.pbKirimPengajuan.Size = New System.Drawing.Size(379, 23)
    Me.pbKirimPengajuan.TabIndex = 8
    Me.pbKirimPengajuan.Visible = False
    '
    'cmdPersetujuanPengajuanKredit
    '
    Me.cmdPersetujuanPengajuanKredit.Location = New System.Drawing.Point(6, 19)
    Me.cmdPersetujuanPengajuanKredit.Name = "cmdPersetujuanPengajuanKredit"
    Me.cmdPersetujuanPengajuanKredit.Size = New System.Drawing.Size(113, 35)
    Me.cmdPersetujuanPengajuanKredit.TabIndex = 6
    Me.cmdPersetujuanPengajuanKredit.Text = "Enable"
    Me.cmdPersetujuanPengajuanKredit.UseVisualStyleBackColor = True
    '
    'Label5
    '
    Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label5.Location = New System.Drawing.Point(125, 25)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(555, 23)
    Me.Label5.TabIndex = 5
    Me.Label5.Text = "Persetujuan Pengajuan Kredit service is active. Disable to manual override"
    Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'cmdAmbil
    '
    Me.cmdAmbil.Location = New System.Drawing.Point(7, 101)
    Me.cmdAmbil.Name = "cmdAmbil"
    Me.cmdAmbil.Size = New System.Drawing.Size(113, 35)
    Me.cmdAmbil.TabIndex = 2
    Me.cmdAmbil.Text = "Ambil"
    Me.cmdAmbil.UseVisualStyleBackColor = True
    '
    'GroupBox2
    '
    Me.GroupBox2.Controls.Add(Me.lblNasabahBunga)
    Me.GroupBox2.Controls.Add(Me.lblNasabahAdm)
    Me.GroupBox2.Controls.Add(Me.lblStatusBunga)
    Me.GroupBox2.Controls.Add(Me.lblBunga)
    Me.GroupBox2.Controls.Add(Me.pbPostingBunga)
    Me.GroupBox2.Controls.Add(Me.lblStatusAdm)
    Me.GroupBox2.Controls.Add(Me.cmdPostingBunga)
    Me.GroupBox2.Controls.Add(Me.cmdPostingAdm)
    Me.GroupBox2.Controls.Add(Me.lblAdm)
    Me.GroupBox2.Controls.Add(Me.pbPostingAdm)
    Me.GroupBox2.Controls.Add(Me.Label7)
    Me.GroupBox2.Controls.Add(Me.cmdPostingBungaAdm)
    Me.GroupBox2.Controls.Add(Me.Label3)
    Me.GroupBox2.Controls.Add(Me.Label2)
    Me.GroupBox2.Controls.Add(Me.Label1)
    Me.GroupBox2.Controls.Add(Me.dTglPosting)
    Me.GroupBox2.Controls.Add(Me.dTgl1)
    Me.GroupBox2.Controls.Add(Me.dTgl)
    Me.GroupBox2.Location = New System.Drawing.Point(8, 187)
    Me.GroupBox2.Name = "GroupBox2"
    Me.GroupBox2.Size = New System.Drawing.Size(799, 213)
    Me.GroupBox2.TabIndex = 13
    Me.GroupBox2.TabStop = False
    Me.GroupBox2.Text = "Administrasi && Bunga Tabungan"
    '
    'lblNasabahBunga
    '
    Me.lblNasabahBunga.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblNasabahBunga.Location = New System.Drawing.Point(285, 178)
    Me.lblNasabahBunga.Name = "lblNasabahBunga"
    Me.lblNasabahBunga.Size = New System.Drawing.Size(379, 23)
    Me.lblNasabahBunga.TabIndex = 21
    Me.lblNasabahBunga.Text = "Nasabah Kantor :  00 - 0 of 0"
    Me.lblNasabahBunga.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.lblNasabahBunga.Visible = False
    '
    'lblNasabahAdm
    '
    Me.lblNasabahAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblNasabahAdm.Location = New System.Drawing.Point(285, 121)
    Me.lblNasabahAdm.Name = "lblNasabahAdm"
    Me.lblNasabahAdm.Size = New System.Drawing.Size(379, 23)
    Me.lblNasabahAdm.TabIndex = 20
    Me.lblNasabahAdm.Text = "Nasabah Kantor :  00 - 0 of 0"
    Me.lblNasabahAdm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.lblNasabahAdm.Visible = False
    '
    'lblStatusBunga
    '
    Me.lblStatusBunga.BackColor = System.Drawing.Color.Transparent
    Me.lblStatusBunga.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblStatusBunga.Location = New System.Drawing.Point(125, 152)
    Me.lblStatusBunga.Name = "lblStatusBunga"
    Me.lblStatusBunga.Size = New System.Drawing.Size(153, 47)
    Me.lblStatusBunga.TabIndex = 19
    Me.lblStatusBunga.Text = "Idle"
    Me.lblStatusBunga.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'lblBunga
    '
    Me.lblBunga.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblBunga.Location = New System.Drawing.Point(670, 152)
    Me.lblBunga.Name = "lblBunga"
    Me.lblBunga.Size = New System.Drawing.Size(129, 47)
    Me.lblBunga.TabIndex = 18
    Me.lblBunga.Text = "0 % Complete"
    Me.lblBunga.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.lblBunga.Visible = False
    '
    'pbPostingBunga
    '
    Me.pbPostingBunga.Location = New System.Drawing.Point(285, 152)
    Me.pbPostingBunga.Name = "pbPostingBunga"
    Me.pbPostingBunga.Size = New System.Drawing.Size(379, 23)
    Me.pbPostingBunga.TabIndex = 17
    Me.pbPostingBunga.Visible = False
    '
    'lblStatusAdm
    '
    Me.lblStatusAdm.BackColor = System.Drawing.Color.Transparent
    Me.lblStatusAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblStatusAdm.Location = New System.Drawing.Point(126, 97)
    Me.lblStatusAdm.Name = "lblStatusAdm"
    Me.lblStatusAdm.Size = New System.Drawing.Size(153, 47)
    Me.lblStatusAdm.TabIndex = 16
    Me.lblStatusAdm.Text = "Idle"
    Me.lblStatusAdm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'cmdPostingBunga
    '
    Me.cmdPostingBunga.Location = New System.Drawing.Point(6, 152)
    Me.cmdPostingBunga.Name = "cmdPostingBunga"
    Me.cmdPostingBunga.Size = New System.Drawing.Size(113, 49)
    Me.cmdPostingBunga.TabIndex = 15
    Me.cmdPostingBunga.Text = "Bunga"
    Me.cmdPostingBunga.UseVisualStyleBackColor = True
    '
    'cmdPostingAdm
    '
    Me.cmdPostingAdm.Location = New System.Drawing.Point(6, 97)
    Me.cmdPostingAdm.Name = "cmdPostingAdm"
    Me.cmdPostingAdm.Size = New System.Drawing.Size(113, 49)
    Me.cmdPostingAdm.TabIndex = 14
    Me.cmdPostingAdm.Text = "Administrasi"
    Me.cmdPostingAdm.UseVisualStyleBackColor = True
    '
    'lblAdm
    '
    Me.lblAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.lblAdm.Location = New System.Drawing.Point(670, 95)
    Me.lblAdm.Name = "lblAdm"
    Me.lblAdm.Size = New System.Drawing.Size(129, 49)
    Me.lblAdm.TabIndex = 13
    Me.lblAdm.Text = "0 % Complete"
    Me.lblAdm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    Me.lblAdm.Visible = False
    '
    'pbPostingAdm
    '
    Me.pbPostingAdm.Location = New System.Drawing.Point(285, 97)
    Me.pbPostingAdm.Name = "pbPostingAdm"
    Me.pbPostingAdm.Size = New System.Drawing.Size(379, 23)
    Me.pbPostingAdm.TabIndex = 12
    Me.pbPostingAdm.Visible = False
    '
    'Label7
    '
    Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.Label7.Location = New System.Drawing.Point(125, 62)
    Me.Label7.Name = "Label7"
    Me.Label7.Size = New System.Drawing.Size(529, 23)
    Me.Label7.TabIndex = 11
    Me.Label7.Text = "Posting Bunga service is active. Disable to manual override"
    Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'Timer2
    '
    Me.Timer2.Interval = 1000
    '
    'bwKirimPengajuan
    '
    '
    'Timer3
    '
    Me.Timer3.Interval = 1000
    '
    'bwAmbilPengajuan
    '
    '
    'bwPostingAdm
    '
    '
    'bwPostingBunga
    '
    '
    'PopupForm
    '
    Me.AcceptButton = Me.CancelFormButton
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(811, 405)
    Me.Controls.Add(Me.CancelFormButton)
    Me.Controls.Add(Me.cmdClose)
    Me.Controls.Add(Me.GroupBox2)
    Me.Controls.Add(Me.GroupBox1)
    Me.Controls.Add(Me.Label4)
    Me.Controls.Add(Me.cTime)
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "PopupForm"
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
    Me.Text = "Manual Override"
    Me.GroupBox1.ResumeLayout(False)
    Me.GroupBox2.ResumeLayout(False)
    Me.GroupBox2.PerformLayout()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Friend WithEvents cmdClose As System.Windows.Forms.Button
  Friend WithEvents CancelFormButton As System.Windows.Forms.Button
  Friend WithEvents cmdKirim As System.Windows.Forms.Button
  Friend WithEvents dTgl As System.Windows.Forms.DateTimePicker
  Friend WithEvents dTgl1 As System.Windows.Forms.DateTimePicker
  Friend WithEvents dTglPosting As System.Windows.Forms.DateTimePicker
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents Label3 As System.Windows.Forms.Label
  Friend WithEvents cmdPostingBungaAdm As System.Windows.Forms.Button
  Friend WithEvents Timer1 As System.Windows.Forms.Timer
  Friend WithEvents cTime As System.Windows.Forms.TextBox
  Friend WithEvents Label4 As System.Windows.Forms.Label
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
  Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
  Friend WithEvents cmdAmbil As System.Windows.Forms.Button
  Friend WithEvents pbKirimPengajuan As System.Windows.Forms.ProgressBar
  Friend WithEvents cmdPersetujuanPengajuanKredit As System.Windows.Forms.Button
  Friend WithEvents Label5 As System.Windows.Forms.Label
  Friend WithEvents Timer2 As System.Windows.Forms.Timer
  Friend WithEvents bwKirimPengajuan As System.ComponentModel.BackgroundWorker
  Friend WithEvents lblKirim As System.Windows.Forms.Label
  Friend WithEvents pbPostingAdm As System.Windows.Forms.ProgressBar
  Friend WithEvents Label7 As System.Windows.Forms.Label
  Friend WithEvents lblAdm As System.Windows.Forms.Label
  Friend WithEvents Timer3 As System.Windows.Forms.Timer
  Friend WithEvents bwAmbilPengajuan As System.ComponentModel.BackgroundWorker
  Friend WithEvents lblAmbil As System.Windows.Forms.Label
  Friend WithEvents pbAmbilPengajuan As System.Windows.Forms.ProgressBar
  Friend WithEvents lblStatusAmbil As System.Windows.Forms.Label
  Friend WithEvents lblStatusKirim As System.Windows.Forms.Label
  Friend WithEvents lblStatusBunga As System.Windows.Forms.Label
  Friend WithEvents lblBunga As System.Windows.Forms.Label
  Friend WithEvents pbPostingBunga As System.Windows.Forms.ProgressBar
  Friend WithEvents lblStatusAdm As System.Windows.Forms.Label
  Friend WithEvents cmdPostingBunga As System.Windows.Forms.Button
  Friend WithEvents cmdPostingAdm As System.Windows.Forms.Button
  Friend WithEvents bwPostingAdm As System.ComponentModel.BackgroundWorker
  Friend WithEvents bwPostingBunga As System.ComponentModel.BackgroundWorker
  Friend WithEvents lblNasabahBunga As System.Windows.Forms.Label
  Friend WithEvents lblNasabahAdm As System.Windows.Forms.Label
End Class
