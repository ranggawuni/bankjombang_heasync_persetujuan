﻿Public Class MainForm
  Private WithEvents Tray As NotifyIcon
  Public Shared cTray As Object
  Private lServiceKirimPersetujuan As Boolean
  Private lServiceAmbilPersetujuan As Boolean
  Private lServicePostingAdm As Boolean
  Private lServicePostingBunga As Boolean
  Private lServicePersetujuanPengajuan As Boolean
  Private lServicePostingBungaAdm As Boolean
  Private lStartServiceKirimPersetujuanPengajuan As Boolean
  Private lStartServiceAmbilPersetujuanPengajuan As Boolean
  Private lStartServicePostingAdm As Boolean
  Private lStartServicePostingBunga As Boolean
  Private lPostingBungaAdmFinish As Boolean
  Private nIntervalPersetujuan As Integer
  Dim cWaktuPostingAdm As String
  Dim cWaktuPostingBunga As String
  Dim cWaktuKirimPengajuan As String
  Dim cWaktuAmbilPengajuan As String
  Dim cDuration As String

  Public Sub New()
    InitializeComponent()
    Application.EnableVisualStyles()
    Icon = My.Resources.trayicon

    Tray = New NotifyIcon() With {.Icon = My.Resources.trayicon, .ContextMenuStrip = ContextMenuStrip1, .Text = "Persetujuan Pengajuan Kredit", .Visible = True}
    cTray = Tray
    lServicePersetujuanPengajuan = CekServicePersetujuanPengajuan()
    lStartServiceKirimPersetujuanPengajuan = True
    lStartServiceAmbilPersetujuanPengajuan = True
    lServiceKirimPersetujuan = True
    lServiceAmbilPersetujuan = False
    timerPersetujuan.Enabled = True
    SetLabelStatusPengajuan(lServicePersetujuanPengajuan)
    lServicePostingBungaAdm = CekServicePostingBunga()
    lStartServicePostingAdm = True
    lStartServicePostingBunga = True
    lServicePostingAdm = True
    lServicePostingBunga = False
    timerPostingBunga.Enabled = True
    SetLabelStatusPostingBunga(lServicePostingBungaAdm)
    lPostingBungaAdmFinish = False
  End Sub

  Private Sub SetLabelStatusPengajuan(ByVal lStatus As Boolean)
    If lStatus Then
      lblStatusKirim.Text = "Service is active"
      lblStatusAmbil.Text = "Service is active"
    Else
      lblStatusKirim.Text = "Service is inactive"
      lblStatusAmbil.Text = "Service is inactive"
    End If
  End Sub

  Private Sub SetLabelStatusPostingBunga(ByVal lStatus As Boolean)
    If lStatus Then
      lblStatusAdm.Text = "Service is active"
      lblStatusBunga.Text = "Service is active"
    Else
      lblStatusAdm.Text = "Service is inactive"
      lblStatusBunga.Text = "Service is inactive"
    End If
  End Sub

  Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
    cTime.Text = Now.ToString
    SetTimeSetting()
    CekURL()
  End Sub

  Private Sub SetTimeSetting()
    Dim nHariPosting As Integer
    Dim dTglTemp As Date
    Dim dTglTemp1 As Date
    Dim cJamPosting As String
    Dim nHariBunga As Integer
    Dim nHariBunga1 As Integer
    Dim nBulan As Integer = Today.Month
    Dim x As New ClassXmlFile()
    'If Debugger.IsAttached Then
    '  nBulan = 4
    'End If
    With x
      .JenisDatabase = ClassXmlFile.enJenisDatabase.local
      .Field = "tgl_bunga"
      nHariBunga = CInt(.GetValue)
      .Field = "tgl_bunga1"
      nHariBunga1 = CInt(.GetValue)
      .Field = "tgl_posting"
      nHariPosting = CInt(.GetValue)
      dTglTemp = DateSerial(Today.Year, nBulan - 1, nHariBunga)
      dTglTemp1 = DateSerial(Today.Year, nBulan, nHariBunga1)
      .Field = "jam_posting"
      cJamPosting = .GetValue
    End With
    Dim dTglPostingTemp As Date = DateSerial(Today.Year, nBulan, nHariPosting)
    Dim cWaktu As String = String.Format("{0} {1}", FormatDateTime(dTglPostingTemp, DateFormat.ShortDate), cJamPosting)
    dTglPosting.Value = CDate(cWaktu)
    dTgl.Value = dTglTemp
    dTgl1.Value = dTglTemp1
  End Sub

  Private Sub Tray_DoubleClick(ByVal sender As Object, ByVal e As EventArgs) Handles Tray.DoubleClick
    Me.Show()
    Me.WindowState = FormWindowState.Normal
    Tray.Visible = False
    Me.ShowInTaskbar = True
  End Sub

  Private Sub SettingToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SettingToolStripMenuItem.Click
    PopupForm.ShowDialog()
  End Sub

  Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
    Tray.Visible = False
    Tray.Dispose()
    Timer1.Enabled = False
    End
  End Sub

  Private Sub SettingToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles SettingToolStripMenuItem1.Click
    FrmConfig.ShowDialog()
  End Sub

  Private Sub ManualOverrideToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ManualOverrideToolStripMenuItem.Click
    PopupForm.ShowDialog()
  End Sub

  Private Sub ExitToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem1.Click
    Tray.Visible = False
    Tray.Dispose()
    Timer1.Enabled = False
    End
  End Sub

  Private Sub ToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem2.Click
    FrmConfig.ShowDialog()
  End Sub

  Sub ToggleHide()
    If Me.WindowState = FormWindowState.Minimized Then
      Tray.Visible = True
      Me.Hide()
      Const cTipText As String = "Service has been activated. Have a nice day..."
      Tray.ShowBalloonTip(1, "Persetujuan Pengajuan Kredit & Posting Bunga Tabungan", cTipText, ToolTipIcon.Info)
      Me.ShowInTaskbar = False
    Else
      Me.ShowInTaskbar = True
    End If
  End Sub

  Private Sub MainForm_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
    StopServiceKirimPersetujuan()
    StopServiceAmbilPersetujuan()
    StopServicePostingAdm()
    StopServicePostingBunga()
  End Sub

  Private Sub MainForm_Load(sender As Object, e As EventArgs) Handles Me.Load
    Dim mc As New DataBaseConnection
    Dim cSQL As String = ""
    Dim x As New ClassXmlFile() With {.JenisDatabase = ClassXmlFile.enJenisDatabase.local, .Field = "databasename"}
    mc.Database = x.GetValue
    x.Field = "ip"
    mc.IP = x.GetValue
    x.Field = "port"
    mc.Port = x.GetValue()
    If mc.Database <> "" Then
      cSQL = "select * from mysql.cabang "
      mc.SQL = cSQL
      Dim db As DataTable = mc.Browse
      If Not IsNothing(db) Then
        Dim cKodeTemp As String = GetNull(db.Rows(0).Item("Kode"), "").ToString
        If cKodeTemp <> "" Then
          UpdCfg(eCfg.msKodeCabang, cKodeTemp)
          cSQL = String.Format("select keterangan, alamat, jeniskantor, induk from cabang where kode = '{0}' ", cKodeTemp)
          mc.SQL = cSQL
          Dim db1 As DataTable = mc.Browse
          If Not IsNothing(db1) Then
            cJenisKantor = db1.Rows(0).Item("JenisKantor").ToString
            cKodeKantorInduk = db1.Rows(0).Item("Induk").ToString
          End If
        Else
          MessageBox.Show("Maaf Kode Kantor belum di Register, Mohon hubungi Administrator!", "Notifikasi", MessageBoxButtons.OK, MessageBoxIcon.Warning)
          End
        End If
      Else
        MessageBox.Show("Maaf Kode Kantor belum di Register, Mohon hubungi Administrator!", "Notifikasi", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End
      End If
      With x
        .Field = "interval_persetujuan"
        Dim cValueTemp As String = .GetValue
        If .GetValue = "" Or .GetValue = "0" Then
          cValueTemp = "1"
        End If
        nIntervalPersetujuan = CInt(cValueTemp)
      End With
      timerPersetujuan.Interval = nIntervalPersetujuan * 1000
      cTime.Text = Now.ToString

      Dim myConnection As New DataBaseConnection
      x.Field = "databasename"
      myConnection.Database = x.GetValue
      x.Field = "ip"
      myConnection.IP = x.GetValue
      x.Field = "port"
      myConnection.Port = x.GetValue
      myConnection.SQL = String.Format("select kasteller,id from username where username = '{0}' ", cUsername)
      Dim dbData As DataTable = myConnection.Browse()
      If Not IsNothing(dbData) Then
        With dbData.Rows(0)
          If .Item("KasTeller").ToString <> "" Then
            cKasTeller = .Item("KasTeller").ToString
            cUserID = .Item("ID").ToString
          End If
        End With
      End If
      SetTimeSetting()
      x.JenisDatabase = ClassXmlFile.enJenisDatabase.remote
      x.Field = "ip"
      cURL.Text = x.GetValue
      CekURL()
      Timer1.Enabled = True
      timerLabel.Enabled = True
    Else
      MessageBox.Show("No database selected, reconfigure system setting.", "Error Database", MessageBoxButtons.OK, MessageBoxIcon.Error)
    End If
  End Sub

  Private Sub CekURL()
    Dim classPersetujuan As New ClassPersetujuanPengajuan
    Dim lConnection As Boolean = classPersetujuan.checkConnection
    If lConnection Then
      lblOnline.Text = "O N L I N E"
      If lblOnline.ForeColor = Color.Black Then
        lblOnline.ForeColor = Color.Blue
      Else
        lblOnline.ForeColor = Color.Black
      End If
    Else
      lblOnline.Text = "O F F L I N E"
      If lblOnline.ForeColor = Color.Black Then
        lblOnline.ForeColor = Color.Red
      Else
        lblOnline.ForeColor = Color.Black
      End If
    End If
  End Sub

  'Private Sub MainForm_Resize(sender As Object, e As EventArgs) Handles Me.Resize
  '  ToggleHide()
  'End Sub

  Private Sub cmdClose_Click(sender As Object, e As EventArgs) Handles cmdClose.Click
    Me.WindowState = FormWindowState.Minimized
    ToggleHide()
  End Sub

  Private Sub StartServiceKirimPersetujuan()
    bwKirimPengajuan.WorkerSupportsCancellation = True
    bwKirimPengajuan.WorkerReportsProgress = True
    bwKirimPengajuan.RunWorkerAsync()
    pbKirimPengajuan.Visible = True
    lblKirim.Visible = pbKirimPengajuan.Visible
    cWaktuKirimPengajuan = cTime.Text
  End Sub

  Private Sub StopServiceKirimPersetujuan()
    bwKirimPengajuan.CancelAsync()
    pbKirimPengajuan.Visible = False
    lblKirim.Visible = pbKirimPengajuan.Visible
    cWaktuKirimPengajuan = ""
  End Sub

  Private Sub StartServiceAmbilPersetujuan()
    bwAmbilPengajuan.WorkerSupportsCancellation = True
    bwAmbilPengajuan.WorkerReportsProgress = True
    bwAmbilPengajuan.RunWorkerAsync()
    pbAmbilPengajuan.Visible = True
    lblAmbil.Visible = pbAmbilPengajuan.Visible
    cWaktuAmbilPengajuan = cTime.Text
  End Sub

  Private Sub StopServiceAmbilPersetujuan()
    bwAmbilPengajuan.CancelAsync()
    pbAmbilPengajuan.Visible = False
    lblAmbil.Visible = pbAmbilPengajuan.Visible
    cWaktuAmbilPengajuan = ""
  End Sub

  Private Sub StartServicePostingAdm()
    bwPostingAdm.WorkerSupportsCancellation = True
    bwPostingAdm.WorkerReportsProgress = True
    bwPostingAdm.RunWorkerAsync()
    pbPostingAdm.Visible = True
    lblAdm.Visible = pbPostingAdm.Visible
    lblNasabahAdm.Visible = pbPostingAdm.Visible
    cWaktuPostingAdm = cTime.Text
  End Sub

  Private Sub StopServicePostingAdm()
    bwPostingAdm.CancelAsync()
    pbPostingAdm.Visible = False
    lblAdm.Visible = pbPostingAdm.Visible
    lblNasabahAdm.Visible = pbPostingAdm.Visible
    cWaktuPostingAdm = ""
  End Sub

  Private Sub StartServicePostingBunga()
    bwPostingBunga.WorkerSupportsCancellation = True
    bwPostingBunga.WorkerReportsProgress = True
    bwPostingBunga.RunWorkerAsync()
    pbPostingBunga.Visible = True
    lblBunga.Visible = pbPostingBunga.Visible
    lblNasabahBunga.Visible = pbPostingBunga.Visible
    cWaktuPostingBunga = cTime.Text
  End Sub

  Private Sub StopServicePostingBunga()
    bwPostingBunga.CancelAsync()
    pbPostingBunga.Visible = False
    lblBunga.Visible = pbPostingBunga.Visible
    lblNasabahBunga.Visible = pbPostingBunga.Visible
    cWaktuPostingBunga = ""
  End Sub

  Private Sub timerPersetujuan_Tick(sender As Object, e As EventArgs) Handles timerPersetujuan.Tick
    lServicePersetujuanPengajuan = CekServicePersetujuanPengajuan()
    If lServicePersetujuanPengajuan Then
      If Not lServiceAmbilPersetujuan And lServiceKirimPersetujuan Then
        If lStartServiceKirimPersetujuanPengajuan Then
          lStartServiceKirimPersetujuanPengajuan = False
          StartServiceKirimPersetujuan()
        End If
      End If
      If Not lServiceKirimPersetujuan And lServiceAmbilPersetujuan Then
        If lStartServiceAmbilPersetujuanPengajuan Then
          lStartServiceAmbilPersetujuanPengajuan = False
          StartServiceAmbilPersetujuan()
        End If
      End If
    End If
  End Sub

  Private Sub bwKirimPengajuan_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bwKirimPengajuan.DoWork
    Threading.Thread.Sleep(1000)
    lServiceKirimPersetujuan = True
    Dim classPersetujuanPengajuan As New ClassPersetujuanPengajuan
    classPersetujuanPengajuan.KirimDataPengajuan(bwKirimPengajuan, e)

    ' '' set the e.Cancel to True to indicate to the RunWorkerCompleted that you cancelled out
    If bwKirimPengajuan.CancellationPending Then
      lServiceKirimPersetujuan = False
      e.Cancel = True
      bwKirimPengajuan.ReportProgress(100, "Cancelled.")
    End If
  End Sub

  Private Sub bwKirimPengajuan_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bwKirimPengajuan.ProgressChanged
    ' This event is fired when you call the ReportProgress method from inside your DoWork.
    ' Any visual indicators about the progress should go here.
    pbKirimPengajuan.Value = e.ProgressPercentage
    Dim cTemp As String = CType(e.UserState, String)
    If cTemp.Substring(0, 1) = "R" Then
      Dim cResult As String() = cTemp.Split(CChar("~"))
      lblStatusKirim.Text = cResult(0)
    Else
      lblStatusKirim.Text = cTemp
    End If
    lblKirim.Text = e.ProgressPercentage.ToString & "% complete."
  End Sub

  Private Sub bwKirimPengajuan_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bwKirimPengajuan.RunWorkerCompleted
    '' This event is fired when your BackgroundWorker exits.
    '' It may have exitted Normally after completing its task, 
    '' or because of Cancellation, or due to any Error.
    lblKirim.Text = ""
    If e.Error IsNot Nothing Then
      '' if BackgroundWorker terminated due to error
      MessageBox.Show(e.Error.Message)
      lblStatusKirim.Text = "Error occurred!"
    ElseIf e.Cancelled Then
      '' otherwise if it was cancelled
      lblStatusKirim.Text = "Task Cancelled!"
    Else
      '' otherwise it completed normally
      lblStatusKirim.Text = "Task completed!"
    End If
    lServiceKirimPersetujuan = False
    lServiceAmbilPersetujuan = True
    lStartServiceAmbilPersetujuanPengajuan = True
    pbKirimPengajuan.Value = 0
    pbKirimPengajuan.Visible = False
    lblKirim.Visible = pbKirimPengajuan.Visible
    SetLabelStatusPengajuan(lServicePersetujuanPengajuan)
    'If lServicePersetujuanPengajuan Then
    '  lblStatusKirim.Text = "Service is active"
    'End If
    cWaktuKirimPengajuan = ""
  End Sub

  Private Sub bwAmbilPengajuan_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bwAmbilPengajuan.DoWork
    'If Not lServiceAmbilPersetujuan Then
    Threading.Thread.Sleep(1000)
    lServiceAmbilPersetujuan = True
    Dim classPersetujuanPengajuan As New ClassPersetujuanPengajuan
    classPersetujuanPengajuan.AmbilDataPengajuan(bwAmbilPengajuan, e)

    ' '' set the e.Cancel to True to indicate to the RunWorkerCompleted that you cancelled out
    If bwAmbilPengajuan.CancellationPending Then
      lServiceAmbilPersetujuan = False
      e.Cancel = True
      bwAmbilPengajuan.ReportProgress(100, "Cancelled.")
    End If
    'End If
  End Sub

  Private Sub bwAmbilPengajuan_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bwAmbilPengajuan.ProgressChanged
    ' This event is fired when you call the ReportProgress method from inside your DoWork.
    ' Any visual indicators about the progress should go here.
    pbAmbilPengajuan.Value = e.ProgressPercentage
    Dim cTemp As String = CType(e.UserState, String)
    If cTemp.Substring(0, 1) = "R" Then
      Dim cResult As String() = cTemp.Split(CChar("~"))
      lblStatusAmbil.Text = cResult(0)
    Else
      lblStatusAmbil.Text = cTemp
    End If
    lblAmbil.Text = e.ProgressPercentage.ToString & "% complete."
    lServicePersetujuanPengajuan = CekServicePersetujuanPengajuan()
    If Not lServicePersetujuanPengajuan Then
      bwAmbilPengajuan.CancelAsync()
    End If
  End Sub

  Private Sub bwAmbilPengajuan_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bwAmbilPengajuan.RunWorkerCompleted
    '' This event is fired when your BackgroundWorker exits.
    '' It may have exitted Normally after completing its task, 
    '' or because of Cancellation, or due to any Error.
    lblAmbil.Text = ""
    If e.Error IsNot Nothing Then
      '' if BackgroundWorker terminated due to error
      MessageBox.Show(e.Error.Message)
      lblStatusAmbil.Text = "Error occurred!"
    ElseIf e.Cancelled Then
      '' otherwise if it was cancelled
      lblStatusAmbil.Text = "Task Cancelled!"
    Else
      '' otherwise it completed normally
      lblStatusAmbil.Text = "Task completed!"
    End If
    lServiceKirimPersetujuan = True
    lServiceAmbilPersetujuan = False
    lStartServiceKirimPersetujuanPengajuan = True
    pbAmbilPengajuan.Value = 0
    pbAmbilPengajuan.Visible = False
    lblAmbil.Visible = pbAmbilPengajuan.Visible
    'If lServicePersetujuanPengajuan Then
    '  lblStatusAmbil.Text = "Service is active"
    'End If
    SetLabelStatusPengajuan(lServicePersetujuanPengajuan)
  End Sub

  Private Sub timerPostingBunga_Tick(sender As Object, e As EventArgs) Handles timerPostingBunga.Tick
    Dim nHari As Integer = CDate(cTime.Text).Day
    Dim nHari1 As Integer = dTglPosting.Value.Day
    Dim dDateTemp As String = FormatDateTime(CDate(cTime.Text), DateFormat.LongTime)
    Dim dDateTemp1 As String = FormatDateTime(dTglPosting.Value, DateFormat.LongTime)
    If CDate(dDateTemp) > CDate("01-08-2015") Then
      lServicePostingBungaAdm = CekServicePostingBunga()
      If nHari = nHari1 And dDateTemp = dDateTemp1 Then
        'If dDateTemp = dDateTemp1 Then
        'MessageBox.Show("Posting")
        If lServicePostingBungaAdm Then
          'If lServicePostingAdm And Not lServicePostingBunga Then
          If lStartServicePostingAdm Then
            lStartServicePostingAdm = False
            StartServicePostingAdm()
          End If
          'End If
          'If Not lServicePostingAdm And lServicePostingBunga Then
          '  If lStartServicePostingBunga Then
          '    lStartServicePostingBunga = False
          '    StartServicePostingBunga()
          '  End If
          'End If
        End If
      Else
        If lPostingBungaAdmFinish Then
          lServicePostingBungaAdm = CekServicePostingBunga()
          If lServicePostingBungaAdm Then
            'If Not lServicePostingBunga And lServicePostingAdm Then
            '  If lStartServicePostingAdm Then
            '    lStartServicePostingAdm = False
            '    StartServicePostingAdm()
            '  End If
            'End If
            'If Not lServicePostingAdm And lServicePostingBunga Then
            If lStartServicePostingBunga Then
              lStartServicePostingBunga = False
              StartServicePostingBunga()
            End If
            'End If
          End If
        End If
      End If
    End If
  End Sub

  Private Sub bwPostingAdm_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bwPostingAdm.DoWork
    Threading.Thread.Sleep(1000)
    lPostingBungaAdmFinish = False
    lServicePostingAdm = True
    Dim classPosting As New ClassPosting() With {.TanggalPosting = dTglPosting.Value,
                                                 .TanggalBungaAwal = dTgl.Value,
                                                 .TanggalBungaAkhir = dTgl1.Value,
                                                 .TrayApp = Tray}
    classPosting.PostingAdm(bwPostingAdm, e)

    ' '' set the e.Cancel to True to indicate to the RunWorkerCompleted that you cancelled out
    If bwPostingAdm.CancellationPending Then
      lServicePostingAdm = False
      e.Cancel = True
      bwPostingAdm.ReportProgress(100, "Cancelled.")
    End If
  End Sub

  <System.Diagnostics.CodeAnalysis.SuppressMessage("CodeRush", "String.Format can be used")>
  Private Sub bwPostingAdm_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bwPostingAdm.ProgressChanged
    ' This event is fired when you call the ReportProgress method from inside your DoWork.
    ' Any visual indicators about the progress should go here.
    pbPostingAdm.Value = e.ProgressPercentage
    Dim cTemp As String = CType(e.UserState, String)
    If cTemp.Substring(0, 1) = "R" Then
      Dim cResult As String() = cTemp.Split(CChar("~"))
      lblStatusAdm.Text = cResult(0)
      lblNasabahAdm.Text = cResult(1)
    Else
      lblStatusAdm.Text = cTemp
    End If
    lblAdm.Text = e.ProgressPercentage.ToString & "% complete."
    lServicePostingAdm = CekServicePostingBunga()
    If Not lServicePostingAdm Then
      bwPostingAdm.CancelAsync()
    End If
  End Sub


  Private Sub bwPostingAdm_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bwPostingAdm.RunWorkerCompleted
    '' This event is fired when your BackgroundWorker exits.
    '' It may have exitted Normally after completing its task, 
    '' or because of Cancellation, or due to any Error.
    lblAdm.Text = ""
    If e.Error IsNot Nothing Then
      '' if BackgroundWorker terminated due to error
      MessageBox.Show(e.Error.Message)
      lblStatusAdm.Text = "Error occurred!"
    ElseIf e.Cancelled Then
      '' otherwise if it was cancelled
      lblStatusAdm.Text = "Task Cancelled!"
    Else
      '' otherwise it completed normally
      lblStatusAdm.Text = "Task completed! - " & cDuration
    End If
    lServicePostingAdm = False
    lServicePostingBunga = True
    lStartServicePostingAdm = True
    pbPostingAdm.Value = 0
    pbPostingAdm.Visible = False
    lblAdm.Visible = pbPostingAdm.Visible
    lblNasabahAdm.Visible = pbPostingAdm.Visible
    SetLabelStatusPostingBunga(lServicePostingBungaAdm)
    lPostingBungaAdmFinish = True
    lStartServicePostingBunga = True
  End Sub

  Private Sub bwPostingBunga_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles bwPostingBunga.DoWork
    Threading.Thread.Sleep(1000)
    lPostingBungaAdmFinish = False
    lServicePostingBunga = True
    Dim classPosting As New ClassPosting() With {.TanggalPosting = dTglPosting.Value,
                                                 .TanggalBungaAwal = dTgl.Value,
                                                 .TanggalBungaAkhir = dTgl1.Value,
                                                 .TrayApp = Tray}
    classPosting.PostingBunga(bwPostingBunga, e)

    ' '' set the e.Cancel to True to indicate to the RunWorkerCompleted that you cancelled out
    If bwPostingBunga.CancellationPending Then
      lServicePostingBunga = False
      e.Cancel = True
      bwPostingBunga.ReportProgress(100, "Cancelled.")
    End If
  End Sub

  Private Sub bwPostingBunga_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles bwPostingBunga.ProgressChanged
    ' This event is fired when you call the ReportProgress method from inside your DoWork.
    ' Any visual indicators about the progress should go here.
    pbPostingBunga.Value = e.ProgressPercentage
    Dim cTemp As String = CType(e.UserState, String)
    If cTemp.Substring(0, 1) = "R" Then
      Dim cResult As String() = cTemp.Split(CChar("~"))
      lblStatusBunga.Text = cResult(0)
      lblNasabahBunga.Text = cResult(1)
    Else
      lblStatusBunga.Text = cTemp
    End If
    lblBunga.Text = e.ProgressPercentage.ToString & "% complete."
    lServicePostingBunga = CekServicePostingBunga()
    If Not lServicePostingBunga Then
      bwPostingBunga.CancelAsync()
    End If
  End Sub

  Private Sub bwPostingBunga_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bwPostingBunga.RunWorkerCompleted
    '' This event is fired when your BackgroundWorker exits.
    '' It may have exitted Normally after completing its task, 
    '' or because of Cancellation, or due to any Error.
    lblBunga.Text = ""
    If e.Error IsNot Nothing Then
      '' if BackgroundWorker terminated due to error
      MessageBox.Show(e.Error.Message)
      lblStatusBunga.Text = "Error occurred!"
    ElseIf e.Cancelled Then
      '' otherwise if it was cancelled
      lblStatusBunga.Text = "Task Cancelled!"
    Else
      '' otherwise it completed normally
      lblStatusBunga.Text = "Task completed!"
    End If
    lServicePostingBunga = False
    lServicePostingAdm = True
    pbPostingBunga.Value = 0
    pbPostingBunga.Visible = False
    lblBunga.Visible = pbPostingBunga.Visible
    lblNasabahBunga.Visible = pbPostingBunga.Visible
    SetLabelStatusPostingBunga(lServicePostingBungaAdm)
    lPostingBungaAdmFinish = True
    lStartServicePostingAdm = True
  End Sub

  Private Sub timerLabel_Tick(sender As Object, e As EventArgs) Handles timerLabel.Tick
    If lblStatusKirim.ForeColor = Color.Black Then
      lblStatusKirim.ForeColor = Color.Blue
    Else
      lblStatusKirim.ForeColor = Color.Black
    End If
    If lblStatusAdm.ForeColor = Color.Blue Then
      lblStatusAdm.ForeColor = Color.Black
    Else
      lblStatusAdm.ForeColor = Color.Blue
    End If
    If lblStatusAmbil.ForeColor = Color.DeepPink Then
      lblStatusAmbil.ForeColor = Color.Black
    Else
      lblStatusAmbil.ForeColor = Color.DeepPink
    End If
    If lblStatusBunga.ForeColor = Color.Black Then
      lblStatusBunga.ForeColor = Color.DeepPink
    Else
      lblStatusBunga.ForeColor = Color.Black
    End If
  End Sub

  Private Sub ShowToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ShowToolStripMenuItem.Click
    Me.Show()
    Me.WindowState = FormWindowState.Normal
    Tray.Visible = False
    Me.ShowInTaskbar = True
  End Sub
End Class