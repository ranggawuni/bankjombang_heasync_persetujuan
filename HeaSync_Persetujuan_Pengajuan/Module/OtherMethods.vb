﻿Imports System.Reflection
Imports System.IO

Friend Module OtherMethods
  Enum formatType
    yyyy_MM_dd = 0
    dd_MM_yyyy = 1
    BilRpPict2 = 2
    BilRpPict = 3
    dd_MMMM_yyyy = 4
    dd_MM_yy_Minus = 5
    dd_MM_yy_Slash = 6
    dd_MM_yy_Dot = 7
    BilRpPict_Default = 8
    string_char = 9
    hh_mm_ss = 10
    yyyy_MM_dd_HH_mm_ss = 11
  End Enum

  Public Enum eCfg
    msKodeKas = 0
    msNama = 1
    msAlamat = 2
    msTelepon = 3
    msFax = 4
    msKota = 5
    msEmail = 6
    msProvinsi = 7
    msKodeCabang = 8
    msLebarGolongan = 9
    msKodeBunga = 10
    msKodePajakBunga = 11
    msRekeningPemindahBukuan = 12
    msKodePenarikanPemindahBukuan = 13
    msKodePenyetoranPemindahbukuan = 14
    msKodeAdministrasi = 15
    msKodeGolonganNasabahTerkait = 16
    msKodeLaba = 17

    msTopMargin1 = 25
    msTopMargin2 = 26
    msLeftMargin = 27
    msLebarNomor = 28
    msLebarTgl = 29
    msLebarSandi = 30
    msLebarMutasi = 31
    msLebarKredit = 32
    msLebarSaldo = 33
    msPaperHight = 34
    msPaperWidth = 35

    ' Cetak Header Tabungan
    msTopHeaderTabungan = 36
    msLeftHeaderTabungan = 37
    msTopValidasiTabungan = 38
    msLeftValidasiTabungan = 39

    msSaldoMinimumKenaPajak = 40

    msKodePenarikanTunai = 43
    msVersion = 44
    msSandiBank = 51
    msSandiCabang = 52

    msDirectoryFoto = 53
    msKodeSetoranTunai = 54

    ' Cetak Buku Tabungan Yang Di Bank
    msTopMarginBank1 = 55
    msTopMarginBank2 = 56
    msLeftMarginBank = 57
    msLebarNomorBank = 58
    msLebarTglBank = 59
    msLebarSandiBank = 60
    msLebarMutasiBank = 61
    msLebarKreditBank = 62
    msLebarSaldoBank = 63
    msPaperHightBank = 64
    msPaperWidthBank = 65

    ' Cetak Header Tabungan Yang Di Bank
    msTopHeaderTabunganBank = 66
    msLeftHeaderTabunganBank = 67

    msKodeAdmPemeliharaan = 77
    msJabatanDirut = 78
    msAlamatDirut = 79
    msNamaDirut = 81
    msKodeLabaTahunLalu = 82

    ' Cetak Kartu Angsuran
    msTopMarginKredit = 83
    msLeftMarginKredit = 84
    msPaperHeightKredit = 85
    msPaperWidthKredit = 86
    msLebarNomorKredit = 87
    msLebarTglKredit = 88
    msLebarPokokKredit = 89
    msLebarBungaKredit = 90
    msLebarJumlahKredit = 91
    msLebarUserNameKredit = 92

    ' Cetak Header Kartu Angsuran Halaman 1
    msTopHeaderKredit = 93
    msLeftHeaderKredit = 94
    msPaperHightHeaderKredit = 95
    msPaperWidthHeaderKredit = 96

    ' Penjualan aktiva
    msRekPenjualanAktiva = 97

    ' file cabang & pusat
    msFileCabang = 98
    msFilePusat = 99

    msKecamatan = 100

    msPejabatBank = 101
    msNamaDirektur = 102

    ' Tempat direktori database report
    msDirektoriDatabaseReport = 105

    ' Cetak Header Kartu Angsuran Halaman 2
    msTopHeaderKredit1 = 106
    msLeftHeaderKredit1 = 107
    msPaperHightHeaderKredit1 = 108
    msPaperWidthHeaderKredit1 = 109

    ' Rekening jurnal setoran tabungan jika setoran pemindahbukuan
    msRekeningSetoranTabunganPB = 111

    ' cetak nota kredit deposito
    msKeteranganTabungan = 112
    msKeteranganTabunganPPh = 113
    msKeteranganPPh = 114

    ' Rekening Taksiran Pajak Pph 25
    msKodeTaksiranPphPasal25 = 115

    ' Preview cabang di laporan
    msPreviewSemuaCabang = 117

    ' cetak nota kredit deposito
    msKeteranganKas = 118
    msKeteranganPB = 119
    msKeteranganPBBunga = 120
    msKeteranganKasBunga = 121
    msKeteranganNamaDeposan = 122

    ' untuk cek versi
    msAutoVersion = 123

    ' rekening jurnal untuk RRP Bunga Yang Masih Harus Dibayar
    msRekeningTitipanPajakTabungan = 125
    msRekeningTitipanPajakDeposito = 126

    ' rekening pendapatan simarmas
    msRekeningPendapatanSimarMas = 127

    ' Kode penutupan tabungan
    msKodePenutupanTabungan = 128

    ' Rekening Jurnal Untuk Taksiran Pajak Yang ditampilkan di Laba Rugi Bulanan
    msRekeningJurnalTaksiranPajak = 130

    ' Paper size Aplikasi Tabungan
    msTopHeaderAplikasiTabungan = 133
    msLeftHeaderAplikasiTabungan = 134
    msWidthAplikasiTabungan = 135
    msHeightAplikasiTabungan = 136

    ' Paper size Data Tambahan Tabungan
    msTopHeaderDataTambahanTabungan = 137
    msLeftHeaderDataTambahanTabungan = 138
    msWidthDataTambahanTabungan = 139
    msHeightDataTambahanTabungan = 140

    ' Paper size Kartu Tanda Tangan
    msTopHeaderKartuTandaTangan = 141
    msLeftHeaderKartuTandaTangan = 142
    msWidthKartuTandaTangan = 143
    msHeightKartuTandaTangan = 144

    ' Paper size Aplikasi Deposito
    msTopHeaderAplikasiDeposito = 145
    msLeftHeaderAplikasiDeposito = 146
    msWidthAplikasiDeposito = 147
    msHeightAplikasiDeposito = 148

    ' Paper size Data Tambahan Deposito
    msTopHeaderDataTambahanDeposito = 149
    msLeftHeaderDataTambahanDeposito = 150
    msWidthDataTambahanDeposito = 151
    msHeightDataTambahanDeposito = 152

    ' Paper size Bukti Kas
    msTopHeaderBuktiKas = 153
    msLeftHeaderBuktiKas = 154
    msWidthBuktiKas = 155
    msHeightBuktiKas = 156

    ' Paper size Bilyet Deposito
    msTopHeaderBilyetDeposito = 157
    msLeftHeaderBilyetDeposito = 158
    msWidthBilyetDeposito = 159
    msHeightBilyetDeposito = 160

    ' Paper size Validasi Tabungan
    msTopHeaderValidasiTabungan = 161
    msLeftHeaderValidasiTabungan = 162
    msWidthValidasiTabungan = 163
    msHeightValidasiTabungan = 164

    ' Paper size Tanda Terima Bunga Deposito
    msTopHeaderTandaTerimaBungaDeposito = 165
    msLeftHeaderTandaTerimaBungaDeposito = 166
    msWidthTandaTerimaBungaDeposito = 167
    msHeightTandaTerimaBungaDeposito = 168

    ' Paper size Nota Kredit Deposito
    msTopHeaderNotaKreditDeposito = 169
    msLeftHeaderNotaKreditDeposito = 170
    msWidthNotaKreditDeposito = 171
    msHeightNotaKreditDeposito = 172

    ' Paper size Pencairan Pokok Deposito
    msTopHeaderPencairanPokokDeposito = 173
    msLeftHeaderPencairanPokokDeposito = 174
    msWidthPencairanPokokDeposito = 175
    msHeightPencairanPokokDeposito = 176

    ' Paper size Penerimaan Pinjaman Kredit
    msTopHeaderPenerimaanPinjamanKredit = 177
    msLeftHeaderPenerimaanPinjamanKredit = 178
    msWidthPenerimaanPinjamanKredit = 179
    msHeightPenerimaanPinjamanKredit = 180

    ' Paper size Slip Angsuran Kredit
    msTopHeaderSlipAngsuran = 181
    msLeftHeaderSlipAngsuran = 182
    msWidthSlipAngsuran = 183
    msHeightSlipAngsuran = 184

    ' Paper size Validasi Angsuran Kredit
    msTopHeaderValidasiAngsuran = 185
    msLeftHeaderValidasiAngsuran = 186
    msWidthValidasiAngsuran = 187
    msHeightValidasiAngsuran = 188

    ' Paper size Nota Debet Titipan
    msTopHeaderNotaDebetTitipan = 189
    msLeftHeaderNotaDebetTitipan = 190
    msWidthNotaDebetTitipan = 191
    msHeightNotaDebetTitipan = 192

    ' Paper size Validasi Jurnal Lain Lain
    msTopHeaderValidasiJurnalLainLain = 193
    msLeftHeaderValidasiJurnalLainLain = 194
    msWidthValidasiJurnalLainLain = 195
    msHeightValidasiJurnalLainLain = 196

    ' Lebar kolom user name saat cetak mutasi ke buku tabungan
    msUserNameBukuTabungan = 198

    ' Konfigurasi Kredit PRK
    msKodeAdministrasiAngsuran = 199
    msKodeAngsuranBungaKredit = 200
    msKodeDendaKredit = 201

    ' Paper size Cetak Kwitansi
    msTopHeaderCetakKwitansi = 203
    msLeftHeaderCetakKwitansi = 204
    msWidthCetakKwitansi = 205
    msHeightCetakKwitansi = 206

    ' Paper size Cetak Perincian Bunga Deposito
    msTopHeaderCetakPerincianBungaDeposito = 207
    msLeftHeaderCetakPerincianBungaDeposito = 208
    msWidthCetakPerincianBungaDeposito = 209
    msHeightCetakPerincianBungaDeposito = 210

    ' Paper size Nota Debet Deposito
    msTopHeaderNotaDebetDeposito = 211
    msLeftHeaderNotaDebetDeposito = 212
    msWidthNotaDebetDeposito = 213
    msHeightNotaDebetDeposito = 214

    ' Rekening Antar Kantor
    msRekeningAntarKantorAktiva = 215
    msRekeningAntarKantorPasiva = 216

    ' Rekening Amortisasi Provisi
    msRekeningAmortisasiProvisi = 217

    msRekeningPendapatan = 218

    ' Paper size Tanda Terima Pengambilan Jaminan
    msTopHeaderTandaTerimaPengambilanJaminan = 219
    msLeftHeaderTandaTerimaPengambilanJaminan = 220
    msWidthTandaTerimaPengambilanJaminan = 221
    msHeightTandaTerimaPengambilanJaminan = 222

    ' Nama Kepala Cabang
    msNamaKepalaCabang = 223

    ' Jarak Tanda Tangan Pengesahan Buku Tabungan
    msJarakPengesahan = 224

    ' Jarak Antara Nama Pejabat Sampai No. Rekening Nasabah
    msJarakRekening = 225

    ' Lokasi File Update
    msFileUpdate = 226

    msKodePembayaranPokokRekeningKoran = 227
    msKodePenarikanPokokRekeningKoran = 228
    msKodeBungaRekeningKoran = 229

    msPembulatanPPAP = 230

    ' kode rekening jurnal untuk penjualan aktiva tetap
    msKodeRekeningDebetAktivaTetap = 231
    msKodeRekeningKreditAktivaTetap = 232

    ' kode rekening hapus buku kredit
    msRekeningHapusBuku = 233

    ' kode rekening ayda
    msRekeningAYDA = 234

    'kode Rekening tunggakan bunga
    msRekeningTunggakanBunga = 235

    'kode Rekening OverDraft
    msRekeningOverdraft = 236

    ' kode rekening pendapatan dan biaya operasional / non operasional
    msRekeningPendapatanOperasional = 237
    msRekeningPendapatanOperasional1 = 238
    msRekeningPendapatanNonOperasional = 239
    msRekeningPendapatanNonOperasional1 = 240
    msRekeningBiayaOperasional = 241
    msRekeningBiayaOperasional1 = 242
    msRekeningBiayaNonOperasional = 243
    msRekeningBiayaNonOperasional1 = 244

    ' kode wilayah default
    msKodeDefaultWilayah = 245

    ' kode biaya IPTW
    msKodeBiayaIPTW = 246

    ' kode transaksi ATM
    msKodeTransaksiATMDebet = 147
    msKodeTransaksiATMKredit = 148

    ' persetujuan pengajuan kredit
    msKodePersetujuanPengajuanKredit = 149

    ' maksimum lama kredit KKB
    msMaksimumLamaKreditKKB = 150

    ' URL koneksi API ATM
    msURLATM = 151

    ' Kode group ATM
    msKodeGroupATM = 152

    ' Nominal untuk Pengurang saldo ATM
    ' Karena munurut OJK, BPR tidak boleh menunjukkan saldo Nasabah ke QNB, sehingga setting ini digunakan supaya saldo di ATM tidak sama
    msNominalPengurangSaldoATM = 153
  End Enum

  Private PF As PopupForm
  Public cJenisKantor As String = ""
  Public cKodeKantorInduk As String = ""
  Public cUsername As String = "SYSTEM"
  Public cUserID As String = ""
  Public cKasTeller As String = ""

  Public Sub ExitApplication()
    'Perform any clean-up here
    'Then exit the application
    Application.Exit()
  End Sub

  Public Sub ShowDialog()
    If PF IsNot Nothing AndAlso Not PF.IsDisposed Then Exit Sub

    Dim CloseApp As Boolean = False

    PF = New PopupForm
    PF.ShowDialog()
    CloseApp = (PF.DialogResult = DialogResult.Abort)
    PF = Nothing

    If CloseApp Then Application.Exit()
  End Sub

  'Function GetAppPath() As String
  '  Dim location = Assembly.GetExecutingAssembly().Location
  '  Dim cPath As String = Path.GetDirectoryName(location)
  '  GetAppPath = cPath
  'End Function

  Function formatValue(ByVal Value As Object, ByVal bFormat As formatType, Optional ByVal cNegatifSeparator As String = "") As String
    Dim vaFormat() As String = {"yyyy-MM-dd", "dd-MM-yyyy", "###,###,###,###,###,##0.00", _
                                "###,###,###,###,###,##0", "dd-MMMM-yyyy", "dd-MM-YY", "dd/MM/YY", _
                                "dd.MM.yy", "#####", "", "hh:mm:ss", "yyyy-MM-dd HH:mm:ss"}
    If Len(cNegatifSeparator) > 0 Then
      If Val(Value) >= 0 Then
        cNegatifSeparator = Space(Len(cNegatifSeparator))
      End If

      formatValue = Left(cNegatifSeparator, 1) & Format(Math.Abs(Val(Value)), vaFormat(bFormat)) & Right(cNegatifSeparator, 1)
    Else
      Dim cFormatType As String = vaFormat(bFormat)
      If bFormat = formatType.BilRpPict Or bFormat = formatType.BilRpPict_Default Or bFormat = formatType.BilRpPict2 Or bFormat = formatType.string_char Then
        formatValue = Format(Value, cFormatType)
      Else
        formatValue = Convert.ToDateTime(Value).ToString(cFormatType)
      End If
    End If
  End Function

  Public Function GetNull(ByRef pValue As Object, Optional ByVal pDefault As Object = 0) As Object
    Return IIf(IsDBNull(pValue) Or IsNothing(pValue), pDefault, pValue)
  End Function

  Function aCfg(ByVal Par As eCfg, Optional ByVal cDefault As String = "") As Object
    Const cAppExeName As String = "BPR" 'GetAppExeName()
    Dim vaRetval As Object = GetSetting("Heasoft", cAppExeName, "Cfg" & Par, "[C]" & CStr(cDefault))
    Dim cTipe As String = vaRetval.ToString.Substring(0, 3) ' Mid(CStr(vaRetval), 1, 3)
    Dim cValue As String = vaRetval.ToString.Substring(3) ' Mid(CStr(vaRetval), 4)
    If vaRetval.ToString.Substring(0, 2) = "[[" Then
      cTipe = vaRetval.ToString.Substring(1, 3)
      cValue = vaRetval.ToString.Substring(5)
    End If
    Select Case cTipe
      Case "[D]"
        Return DateSerial(CInt(Left(cValue, 4)), CInt(Mid(cValue, 5, 2)), CInt(Mid(cValue, 7, 2)))
      Case Else
        Return cValue
    End Select
  End Function

  <System.Diagnostics.CodeAnalysis.SuppressMessage("CodeRush", "String.Format can be used")>
  Sub UpdCfg(ByVal par As eCfg, ByVal Keterangan As Object, Optional ByVal cCabang As String = "", Optional ByVal lLocalConnection As Boolean = True)
    Dim myConnection As New DataBaseConnection
    Dim cSQL As String = ""
    Dim cTipe As String = "C"
    Const cAppExeName As String = "BPR.exe" 'GetAppExeName()
    Dim cKodeCabang As String = "01"
    If VarType(Keterangan) = VariantType.Date Then
      Keterangan = Format(Keterangan, "yyyymmdd")
      cTipe = "D"
    End If
    Try
      If cCabang = "" Then
        cKodeCabang = CStr(aCfg(eCfg.msKodeCabang))
      Else
        cKodeCabang = cCabang
      End If
      SaveSetting("Heasoft", cAppExeName, "Cfg" & CInt(par), String.Format("[{0}]{1}", cTipe, Keterangan))
      Const cTable As String = "konfig"
      Dim x As New ClassXmlFile()
      If lLocalConnection Then
        x.JenisDatabase = ClassXmlFile.enJenisDatabase.local
      Else
        x.JenisDatabase = ClassXmlFile.enJenisDatabase.remote
      End If
      x.Field = "databasename"
      myConnection.Database = x.GetValue
      x.Field = "ip"
      myConnection.IP = x.GetValue
      x.Field = "port"
      myConnection.Port = x.GetValue()
      cSQL = "delete from " & cTable & " where jenis = " & CInt(par)
      myConnection.SQL = cSQL
      myConnection.Delete()

      cSQL = "insert into " & cTable & " (jenis, keterangan, tipe, cabangentry) "
      cSQL = cSQL & "values (@jenis, @keterangan, @tipe, @cabangentry) "
      myConnection.InitConnection()
      Dim cmd As MySqlCommand = New MySqlCommand(cSQL, myConnection.OpenConnection)
      With cmd
        .Parameters.AddWithValue("@jenis", CInt(par))
        .Parameters.AddWithValue("@keterangan", Keterangan)
        .Parameters.AddWithValue("@tipe", cTipe)
        .Parameters.AddWithValue("@cabangentry", cKodeCabang)
        .ExecuteNonQuery()
        .Dispose()
      End With
      myConnection.CloseConnection()
      myConnection.Dispose()
    Catch ex As Exception
      'MessageBox.Show(ex.Message)
      Console.WriteLine(ex.Message)
    End Try
  End Sub

  'Function GetAppExeName() As String
  '  Dim asmExecuting As Assembly = Assembly.GetExecutingAssembly
  '  Dim cAppExeName As String = Path.GetFileName(asmExecuting.Location)
  '  Return cAppExeName
  'End Function

  Function BOM(Optional ByVal dTanggal As Date = #1/1/1900#) As Date
    If dTanggal = #1/1/1900# Then
      dTanggal = Date.Today
    End If
    BOM = DateSerial(Year(dTanggal), Month(dTanggal), 1)
  End Function

  Function EOM(Optional ByVal dTanggal As Date = #1/1/1900#) As Date
    If dTanggal = #1/1/1900# Then
      dTanggal = Date.Today
    End If
    EOM = DateSerial(Year(dTanggal), Month(dTanggal) + 1, 0)
  End Function

  Function Padl(Optional ByVal cCharacter As String = "", Optional ByVal nLen As Integer = 0, Optional ByVal cTipeChar As Object = " ") As String
    Dim n As Integer
    Dim x As String = ""
    If Len(cCharacter) < nLen Then
      For n = 1 To nLen - Len(cCharacter)
        x = CStr(cTipeChar) & x
      Next
      Padl = x & cCharacter
    Else
      Padl = Mid(cCharacter, 1, nLen)
    End If
  End Function

  Function ReplaceSpecialCharacter(ByVal cKeterangan As String) As String
    ReplaceSpecialCharacter = Replace(cKeterangan, "'", "''")
  End Function

  Sub InitGauge(ByVal pr As ProgressBar, ByVal nMax As Double, Optional ByVal objLabel As Label = Nothing)
    If Not IsNothing(pr) Then
      pr.Visible = True
      pr.Value = 0
      pr.Step = 1
      pr.Minimum = 0
      pr.Maximum = CInt(Max(nMax, 1))
      If Not IsNothing(objLabel) Then
        objLabel.Visible = True
        objLabel.Text = "0 of " & nMax.ToString
      End If
    End If
  End Sub

  Sub RunGauge(ByVal pr As ProgressBar, Optional ByVal objLabel As Label = Nothing)
    If Not IsNothing(pr) Then
      pr.PerformStep()
      pr.Update()
      If Not IsNothing(objLabel) Then
        objLabel.Text = String.Format("{0} of {1}", pr.Value, pr.Maximum)
      End If
    End If
  End Sub

  Sub EndGauge(ByVal pr As ProgressBar, Optional ByVal objLabel As Label = Nothing)
    If Not IsNothing(pr) Then
      pr.Visible = False
      If Not IsNothing(objLabel) Then
        objLabel.Visible = False
        objLabel.Text = "0 of 0"
      End If
    End If
  End Sub

  Function Max(n As Double, a As Double) As Double
    Max = Val(IIf(n < a, a, n))
  End Function

  <System.Diagnostics.CodeAnalysis.SuppressMessage("CodeRush", "String.Format can be used")>
  Function HitungDurasi(ByVal tAwal As DateTime) As String
    Dim cWaktu As DateTime = tAwal
    Dim nDuration As TimeSpan = Now - cWaktu
    Dim cDuration As String = Padl(nDuration.Hours.ToString, 2, "0") & ":" & Padl(nDuration.Minutes.ToString, 2, "0") & ":" & Padl(nDuration.Seconds.ToString, 2, "0")
    Dim cResult As String = "Running - " & cDuration
    Return cResult
  End Function

  Function CekServicePersetujuanPengajuan() As Boolean
    Dim x As New ClassXmlFile() With {.JenisDatabase = ClassXmlFile.enJenisDatabase.local, .Field = "service_persetujuan_pengajuan"}
    Dim cService As String = x.GetValue.ToString
    If cService = "0" Then
      Return True
    Else
      Return False
    End If
  End Function

  Function CekServicePostingBunga() As Boolean
    Dim x As New ClassXmlFile() With {.JenisDatabase = ClassXmlFile.enJenisDatabase.local, .Field = "service_posting_bunga"}
    Dim cService As String = x.GetValue.ToString
    If cService = "0" Then
      Return True
    Else
      Return False
    End If
  End Function

End Module
