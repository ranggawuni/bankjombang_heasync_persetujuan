﻿Imports System.IO

Public Class ClassIniFile
  Private ReadOnly _settings As IDictionary(Of String, String)

  Private Sub New(settings As IDictionary(Of String, String))
    _settings = settings
  End Sub

  Default Public ReadOnly Property Item(name As String) As String
    Get
      Dim value As String = Nothing
      _settings.TryGetValue(name, value)
      Return value
    End Get
  End Property

  Public Shared Function Load(fileName As String) As ClassIniFile
    Dim settings = New Dictionary(Of String, String)()
    For Each line In File.ReadLines(fileName).Where(Function(x) x.Contains("="))
      Dim parts = line.Split("="c)
      If parts.Count = 2 Then
        settings(parts(0).Trim()) = parts(1).Trim()
      End If
    Next
    Return New ClassIniFile(settings)
  End Function
End Class
