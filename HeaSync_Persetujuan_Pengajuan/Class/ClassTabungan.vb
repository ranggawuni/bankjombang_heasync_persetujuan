﻿Imports HeaSync_Persetujuan_Pengajuan.DataTableTool

Public Class ClassTabungan
  Private SqlString As String = ""

  Private _cRekening As String = ""
  Private _cKodeRegister As String = ""
  Private _dTanggal As Date = Today.Date
  Private _cFaktur As String = ""
  Private _cCabang As String = ""
  Private _cKodeTransaksi As String = ""
  Private _nMutasi As Double = 0
  Private _lDeleteExistingData As Boolean = False
  Private _cKeterangan As String = ""
  Private _cUsername As String = "system"
  Private _lUpdateBukuBesar As Boolean = True
  Private _cWaktu As String = Now.ToString
  Private _cRekeningJurnal As String = ""
  Private _lUpdateRekeningKoran As Boolean = False
  Private _cCaraBayarRK As String = "K"
  Private _lUpdateRekeningKoranOverDraft As Boolean = False
  Private _lDeleteExistingBukuBesar As Boolean = True
  Private _cGolonganNasabah As String = ""
  Private _cGolonganTabungan As String = ""
  Private _cAO As String = ""
  Private _cWilayah As String = ""
  Private _cKeterkaitan As String = ""
  Private _cJenisNasabah As String = ""
  Private _cKaryawan As String = ""
  Private _lWithSaldoMinimum As Boolean = True
  Private _dTanggalAwal As Date = Today.Date
  Private _dTanggalAkhir As Date = Today.Date
  Private _nTotalBunga As Double = 0
  Private _nTotalPajak As Double = 0
  Private _nSaldoAwal As Double = 0
  Private _nSaldoAkhir As Double = 0
  Private _nMutasiDebet As Double = 0
  Private _nMutasiKredit As Double = 0
  Private _lPajakOnly As Boolean = False
  Private _nTotalSaldoTabungan As Double = 0
  Private _nTotalSaldoDeposito As Double = 0
  Private _nTotalSaldoTabunganDeposito As Double = 0
  Private _lProsesPostingBunga As Boolean = False
  Private _nSaldoTabungan As Double = 0
  Private _cDatabase As String = ""
  Private _cIPNumber As String = ""
  Private _cPort As String = ""

  Enum enJenisNasabahTabungan
    Non_Instansi = 0
    Instansi = 1
  End Enum

  Public Property Port As String
    Get
      Return _cPort
    End Get
    Set(value As String)
      _cPort = value
    End Set
  End Property

  Public Property IPNumber As String
    Get
      Return _cIPNumber
    End Get
    Set(value As String)
      _cIPNumber = value
    End Set
  End Property

  Public Property Database As String
    Get
      Return _cDatabase
    End Get
    Set(value As String)
      _cDatabase = value
    End Set
  End Property

  Public Property SaldoTabungan As Double
    Get
      Return _nSaldoTabungan
    End Get
    Set(value As Double)
      _nSaldoTabungan = value
    End Set
  End Property

  Public Property ProsesPostingBunga As Boolean
    Get
      Return _lProsesPostingBunga
    End Get
    Set(value As Boolean)
      _lProsesPostingBunga = value
    End Set
  End Property

  Public Property TotalSaldoTabunganDeposito As Double
    Get
      Return _nTotalSaldoTabunganDeposito
    End Get
    Set(value As Double)
      _nTotalSaldoTabunganDeposito = value
    End Set
  End Property

  Public Property TotalSaldoDeposito As Double
    Get
      Return _nTotalSaldoDeposito
    End Get
    Set(value As Double)
      _nTotalSaldoDeposito = value
    End Set
  End Property

  Public Property TotalSaldoTabungan As Double
    Get
      Return _nTotalSaldoTabungan
    End Get
    Set(value As Double)
      _nTotalSaldoTabungan = value
    End Set
  End Property

  Public Property PajakOnly As Boolean
    Get
      Return _lPajakOnly
    End Get
    Set(value As Boolean)
      _lPajakOnly = value
    End Set
  End Property

  Public Property MutasiKredit As Double
    Get
      Return _nMutasiKredit
    End Get
    Set(value As Double)
      _nMutasiKredit = 0
    End Set
  End Property

  Public Property MutasiDebet As Double
    Get
      Return _nMutasiDebet
    End Get
    Set(value As Double)
      _nMutasiDebet = value
    End Set
  End Property

  Public Property SaldoAkhir As Double
    Get
      Return _nSaldoAkhir
    End Get
    Set(value As Double)
      _nSaldoAkhir = value
    End Set
  End Property

  Public Property SaldoAwal As Double
    Get
      Return _nSaldoAwal
    End Get
    Set(value As Double)
      _nSaldoAwal = value
    End Set
  End Property

  Public Property TotalPajak As Double
    Get
      Return _nTotalPajak
    End Get
    Set(value As Double)
      _nTotalPajak = value
    End Set
  End Property

  Public Property TotalBunga As Double
    Get
      Return _nTotalBunga
    End Get
    Set(value As Double)
      _nTotalBunga = value
    End Set
  End Property

  Public Property TanggalAkhir As Date
    Get
      Return _dTanggalAkhir
    End Get
    Set(value As Date)
      _dTanggalAkhir = value
    End Set
  End Property

  Public Property TanggalAwal As Date
    Get
      Return _dTanggalAwal
    End Get
    Set(value As Date)
      _dTanggalAwal = value
    End Set
  End Property

  Public Property WithSaldoMinimum As Boolean
    Get
      Return _lWithSaldoMinimum
    End Get
    Set(value As Boolean)
      _lWithSaldoMinimum = value
    End Set
  End Property

  Public Property Karyawan As String
    Get
      Return _cKaryawan
    End Get
    Set(value As String)
      _cKaryawan = value
    End Set
  End Property

  Public Property JenisNasabah As String
    Get
      Return _cJenisNasabah
    End Get
    Set(value As String)
      _cJenisNasabah = value
    End Set
  End Property

  Public Property Keterkaitan As String
    Get
      Return _cKeterkaitan
    End Get
    Set(value As String)
      _cKeterkaitan = value
    End Set
  End Property

  Public Property Wilayah As String
    Get
      Return _cWilayah
    End Get
    Set(value As String)
      _cWilayah = value
    End Set
  End Property

  Public Property AO As String
    Get
      Return _cAO
    End Get
    Set(value As String)
      _cAO = value
    End Set
  End Property

  Public Property GolonganTabungan As String
    Get
      Return _cGolonganTabungan
    End Get
    Set(value As String)
      _cGolonganTabungan = value
    End Set
  End Property

  Public Property GolonganNasabah As String
    Get
      Return _cGolonganNasabah
    End Get
    Set(value As String)
      _cGolonganNasabah = value
    End Set
  End Property

  Public Property DeleteExistingBukuBesar As Boolean
    Get
      Return _lDeleteExistingBukuBesar
    End Get
    Set(value As Boolean)
      _lDeleteExistingBukuBesar = value
    End Set
  End Property

  Public Property UpdateRekeningKoranOverDraft As Boolean
    Get
      Return _lUpdateRekeningKoranOverDraft
    End Get
    Set(value As Boolean)
      _lUpdateRekeningKoranOverDraft = value
    End Set
  End Property
  Public Property CaraBayarRekeningKoran As String
    Get
      Return _cCaraBayarRK
    End Get
    Set(value As String)
      _cCaraBayarRK = value
    End Set
  End Property

  Public Property UpdateRekeningKoran As Boolean
    Get
      Return _lUpdateRekeningKoran
    End Get
    Set(value As Boolean)
      _lUpdateRekeningKoran = value
    End Set
  End Property

  Public Property RekeningJurnal As String
    Get
      Return _cRekeningJurnal
    End Get
    Set(value As String)
      _cRekeningJurnal = value
    End Set
  End Property

  Public Property Waktu As String
    Get
      Return _cWaktu
    End Get
    Set(value As String)
      _cWaktu = value
    End Set
  End Property

  Public Property UpdateBukuBesar As Boolean
    Get
      Return _lUpdateBukuBesar
    End Get
    Set(value As Boolean)
      _lUpdateBukuBesar = value
    End Set
  End Property

  Public Property Username As String
    Get
      Return _cUsername
    End Get
    Set(value As String)
      _cUsername = value
    End Set
  End Property

  Public Property Keterangan As String
    Get
      Return _cKeterangan
    End Get
    Set(value As String)
      _cKeterangan = value
    End Set
  End Property

  Public Property DeleteExistingData As Boolean
    Get
      Return _lDeleteExistingData
    End Get
    Set(value As Boolean)
      _lDeleteExistingData = value
    End Set
  End Property

  Public Property Mutasi As Double
    Get
      Return _nMutasi
    End Get
    Set(value As Double)
      _nMutasi = value
    End Set
  End Property

  Public Property KodeTransaksi As String
    Get
      Return _cKodeTransaksi
    End Get
    Set(value As String)
      _cKodeTransaksi = value
    End Set
  End Property

  Public Property Cabang As String
    Get
      Return _cCabang
    End Get
    Set(value As String)
      _cCabang = value
    End Set
  End Property

  Public Property Faktur As String
    Get
      Return _cFaktur
    End Get
    Set(value As String)
      _cFaktur = value
    End Set
  End Property

  Public Property Tanggal As Date
    Get
      Return _dTanggal
    End Get
    Set(value As Date)
      _dTanggal = value
    End Set
  End Property

  Public Property KodeRegister As String
    Get
      Return _cKodeRegister
    End Get
    Set(value As String)
      _cKodeRegister = value
    End Set
  End Property
  Public Property Rekening As String
    Get
      Return _cRekening
    End Get
    Set(value As String)
      _cRekening = value
    End Set
  End Property

  Sub UpdMutasiTabungan()
    Dim myConnection As New DataBaseConnection()
    Dim classHistory As New ClassHistory
    Dim cDK As String = ""
    Dim cKas As String = ""
    Dim nDebet As Double = 0
    Dim nKredit As Double = 0

    If Right(_cWaktu, 1) = "M" Then
      _cWaktu = Format(_cWaktu, "yyyy-MM-dd hh:mm:ss")
    End If

    myConnection.Database = _cDatabase
    myConnection.IP = _cIPNumber
    myConnection.Port = _cPort
    If _lDeleteExistingData Then
      myConnection.UpdateHistory = True
      myConnection.SQL = String.Format("delete from mutasitabungan where faktur = '{0}'", _cFaktur)
      myConnection.Delete()
    End If
    If _nMutasi <> 0 Then
      myConnection.SQL = String.Format("select dk,kas,rekening from kodetransaksi where kode = '{0}'", _cKodeTransaksi)
      Dim cDataTable As DataTable = myConnection.Browse()
      If Not IsNothing(cDataTable) Then
        With cDataTable.Rows(0)
          cDK = .Item("DK").ToString
          cKas = .Item("Kas").ToString
          If cKas = "K" Then
            _cRekeningJurnal = cKasTeller
          End If
          If Left(_cFaktur, 2) = "AG" Then
            _cRekeningJurnal = .Item("rekening").ToString
          End If
        End With
      End If
      If cDK = "D" Then
        nDebet = _nMutasi
        nKredit = 0
      Else
        nDebet = 0
        nKredit = _nMutasi
      End If
      myConnection.InitConnection()
      SqlString = "insert into mutasitabungan (faktur, tgl, kodetransaksi, rekening, jumlah, keterangan, DK, RekeningJurnal, Debet, Kredit, Username, DateTime, CabangEntry, KasRK) "
      SqlString = SqlString & "values (@faktur, @tgl, @kodetransaksi, @rekening, @jumlah, @keterangan, @dk, @rekeningjurnal, @debet, @kredit, @username, @datetime, @cabangentry, @kasrk)"
      Dim cmd As MySqlCommand = New MySqlCommand(SqlString, myConnection.OpenConnection)
      With cmd.Parameters
        .AddWithValue("@faktur", _cFaktur)
        .AddWithValue("@tgl", formatValue(_dTanggal, formatType.yyyy_MM_dd))
        .AddWithValue("@kodetransaksi", _cKodeTransaksi)
        .AddWithValue("@rekening", _cRekening)
        .AddWithValue("@jumlah", _nMutasi)
        .AddWithValue("@keterangan", _cKeterangan)
        .AddWithValue("@dk", cDK)
        .AddWithValue("@rekeningjurnal", _cRekeningJurnal)
        .AddWithValue("@debet", nDebet)
        .AddWithValue("@kredit", nKredit)
        .AddWithValue("@username", _cUsername)
        .AddWithValue("@datetime", formatValue(_cWaktu, formatType.yyyy_MM_dd_HH_mm_ss))
        .AddWithValue("@cabangentry", _cCabang)
        .AddWithValue("@kasrk", _cCaraBayarRK)
      End With
      cmd.ExecuteNonQuery()
      classHistory.SQL = SqlString
      classHistory.ObjCmd = cmd
      cmd.Dispose()
      myConnection.CloseConnection()
      myConnection.Dispose()

      classHistory.Database = _cDatabase
      classHistory.IPNumber = _cIPNumber
      classHistory.Port = _cPort
      classHistory.Username = _cUsername
      classHistory.UpdateHistory()

      If Not _lUpdateRekeningKoran Then
        If _lUpdateBukuBesar Then
          updRekTabungan()
        End If
        'Else
        'If _lUpdateRekeningKoranOverDraft Then
        '  ClassKredit.Faktur = _cFaktur
        '  ClassKredit.updRekOverDraft()
        'Else
        '  If _lUpdateBukuBesar Then
        '    ClassKredit.Faktur = _cFaktur
        '    ClassKredit.Rekening = _cRekening
        '    ClassKredit.UpdPencairanRekeningKoran()
        '  End If
        'End If
      End If
    End If
  End Sub

  Sub updRekTabungan()
    Dim myConnection As New DataBaseConnection()
    Dim classAkuntansi As New ClassAkuntansi
    myConnection.Database = _cDatabase
    myConnection.IP = _cIPNumber
    myConnection.Port = _cPort
    myConnection.Username = _cUsername
    myConnection.UpdateHistory = True
    If _lDeleteExistingBukuBesar Then
      myConnection.SQL = String.Format("delete from bukubesar where faktur = '{0}' ", _cFaktur)
      myConnection.Delete()
    End If
    Dim cSQL As String = ""
    cSQL = cSQL & " Select m.*, t.Rekening as RekeningTabungan,t.Kode,t.TglPenutupan,"
    cSQL = cSQL & " k.Kas,k.Rekening as RekeningKodeTransaksi,k.Keterangan as KeteranganKodeTransaksi,"
    cSQL = cSQL & " g.Rekening as RekeningPerkiraanTabungan,r.Nama as NamaNasabah,g.RekeningBunga "
    cSQL = cSQL & " From MutasiTabungan m"
    cSQL = cSQL & " Left Join Tabungan t on m.Rekening = t.Rekening"
    cSQL = cSQL & " Left Join GolonganTabungan g on g.Kode = t.GolonganTabungan"
    cSQL = cSQL & " Left Join KodeTransaksi k on k.Kode = m.KodeTransaksi"
    cSQL = cSQL & " Left Join RegisterNasabah r on r.Kode = t.Kode"
    cSQL = String.Format("{0} Where m.Faktur='{1}' ", cSQL, _cFaktur)
    cSQL = cSQL & " Group by m.Faktur,m.Tgl,m.ID "
    myConnection.SQL = cSQL
    Dim cDataTable As DataTable = myConnection.Browse()
    If Not IsNothing(cDataTable) Then
      Dim nBaris As Integer = cDataTable.Rows.Count
      Dim n As Integer = 1
      Do Until n > nBaris
        With cDataTable.Rows(0)
          Dim cCabang As String = .Item("CabangEntry").ToString
          Dim dTgl As Date = CDate(.Item("Tgl").ToString)
          Dim cDK As String = .Item("DK").ToString
          Dim cKas As String = CStr(GetNull(.Item("Kas").ToString))
          Dim cRekeningTabungan As String = .Item("RekeningPerkiraanTabungan").ToString
          Dim nNominalTabungan As Double = CDbl(.Item("Jumlah").ToString)
          Dim lTutup As Boolean = False
          Dim cKeterangan As String = .Item("Keterangan").ToString
          Dim cNamaUser As String = .Item("UserName").ToString
          Dim cRekeningPendapatanSimarmas As String
          Dim cKodeTransaksiTemp As String = CStr(GetNull(.Item("KodeTransaksi").ToString))
          If CDate(.Item("TglPenutupan").ToString) = CDate(.Item("Tgl").ToString) Then
            lTutup = True
            cRekeningPendapatanSimarmas = CStr(aCfg(eCfg.msRekeningPendapatanSimarMas))
          Else
            cRekeningPendapatanSimarmas = ""
          End If

          ' ambil rekening dari table kode transaksi
          Dim cRekeningKodeTransaksi As String
          If CStr(GetNull(.Item("RekeningJurnal").ToString, "")) = "" Then
            cRekeningKodeTransaksi = CStr(GetNull(.Item("RekeningJurnal").ToString, ""))
          Else
            cRekeningKodeTransaksi = CStr(GetNull(.Item("RekeningKodeTransaksi").ToString, ""))
          End If

          ' Jika Kode Transaksi bunga maka akan melihat Kode Rekening Bunga
          ' pada Table Golongan Tabungan.
          ' Karena kalau tidak maka Rekening bunga akan menjadi satu pada 1 Rekening
          If cKodeTransaksiTemp = CStr(aCfg(eCfg.msKodeBunga)) And Trim(.Item("RekeningBunga").ToString) <> "" Then
            cRekeningKodeTransaksi = .Item("RekeningBunga").ToString
          End If

          'ambil rekening akuntansi kas teller
          If cKas = "K" Then
            myConnection.SQL = String.Format("select KasTeller from username where username = '{0}' ", .Item("Username"))
            Dim cDataTableUser As DataTable = myConnection.Browse()
            If Not IsNothing(cDataTableUser) Then
              If cDataTableUser.Rows(0).Item("KasTeller").ToString <> "" Then
                cRekeningKodeTransaksi = cDataTableUser.Rows(0).Item("KasTeller").ToString
              End If
            End If
          End If
          Dim cKodeCabang As String = CStr(aCfg(eCfg.msKodeCabang))
          Dim lPusat As Boolean = False
          Dim lInduk As Boolean
          Dim cInduk As String
          myConnection.SQL = String.Format("select kode, jeniskantor, induk from cabang where kode = '{0}' ", cKodeCabang)
          Dim cDataTableKantor As DataTable = myConnection.Browse()
          If Not IsNothing(cDataTableKantor) Then
            With cDataTableKantor.Rows(0)
              cInduk = .Item("Induk").ToString
              If .Item("JenisKantor").ToString = "P" Then
                lPusat = True
              End If
            End With
          Else
            cInduk = ""
          End If
          Dim cRekeningAntarKantor As String
          If lPusat Then
            cRekeningAntarKantor = aCfg(eCfg.msRekeningAntarKantorAktiva).ToString
          Else
            cRekeningAntarKantor = CStr(aCfg(eCfg.msRekeningAntarKantorPasiva))
          End If
          ' jika ada transaksi dari kantor kas ke kantor cabang maka
          ' transaksi yang terjadi bukan transaksi antar kantor seperti
          ' antar kantor pusat dan cabang
          If cInduk <> "" Then
            lInduk = True
          Else
            lInduk = False
          End If
          ' jika ada transaksi antar kantor
          Dim cDebet As String = ""
          Dim cKredit As String = ""
          Dim cDebet1 As String = ""
          Dim cKredit1 As String = ""
          classAkuntansi.ID = CDbl(.Item("ID"))
          classAkuntansi.Cabang = cCabang
          classAkuntansi.Faktur = _cFaktur
          classAkuntansi.Tanggal = dTgl
          classAkuntansi.Keterangan = cKeterangan
          classAkuntansi.Username = cNamaUser
          classAkuntansi.Database = _cDatabase
          classAkuntansi.IPNumber = _cIPNumber
          classAkuntansi.Port = _cPort
          If cCabang <> cKodeCabang And Not lInduk Then
            ' jika pada saat diposting cabang entry sama dengan kode cabang yang aktif 
            If cCabang = cKodeCabang Then
              If cDK = "D" Then
                ' Dr. Antar Bank Pasiva         nNominalTabungan
                '     Cr. Rekening Kredit           nNominalTabungan
                cDebet = cRekeningAntarKantor
                cKredit = GetNull(cRekeningKodeTransaksi.ToString, "").ToString
              Else
                ' Dr. Rekening Debet            nNominalTabungan
                '     Cr. Antar Bank Pasiva         nNominalTabungan
                cDebet = GetNull(cRekeningKodeTransaksi.ToString, "").ToString
                cKredit = cRekeningAntarKantor
              End If
              'Debet
              classAkuntansi.RekeningJurnal = cDebet
              classAkuntansi.Debet = nNominalTabungan
              classAkuntansi.UpdBukuBesar()
              'Kredit
              classAkuntansi.RekeningJurnal = cKredit
              classAkuntansi.Kredit = nNominalTabungan
              classAkuntansi.UpdBukuBesar()
              If cKodeTransaksiTemp = aCfg(eCfg.msKodePenutupanTabungan).ToString Then
                'Debet
                classAkuntansi.RekeningJurnal = cRekeningAntarKantor
                classAkuntansi.Debet = nNominalTabungan
                classAkuntansi.UpdBukuBesar()
                'Kredit
                classAkuntansi.RekeningJurnal = GetNull(.Item("RekeningKodeTransaksi"), "").ToString
                classAkuntansi.Kredit = nNominalTabungan
                classAkuntansi.UpdBukuBesar()
              End If
            Else
              If cDK = "D" Then
                ' Dr. Antar Kantor Aktiva       nNominalTabungan
                '     Cr. Rekening Kredit           nNominalTabungan
                ' Dr. Tabungan                  nNominalTabungan
                '     Cr. Antar Kantor Aktiva       nNominalTabungan
                cDebet = cRekeningAntarKantor
                cKredit = GetNull(cRekeningKodeTransaksi.ToString, "").ToString
                cDebet1 = cRekeningTabungan
                cKredit1 = cRekeningAntarKantor
              Else
                ' Dr. Rekening Debet            nNominalTabungan
                '     Cr. Antar Kantor Aktiva       nNominalTabungan
                ' Dr. Antar Kantor Aktiva       nNominalTabungan
                '     Cr. Tabungan                  nNominalTabungan
                cDebet = GetNull(cRekeningKodeTransaksi.ToString, "").ToString
                cKredit = cRekeningAntarKantor
                cDebet1 = cRekeningAntarKantor
                cKredit1 = cRekeningTabungan
              End If
              If Left(_cFaktur, 2) = "DP" Then
                cDebet = cKasTeller
                cKredit = cRekeningTabungan

                'Debet
                classAkuntansi.RekeningJurnal = cDebet
                classAkuntansi.Debet = nNominalTabungan
                classAkuntansi.UpdBukuBesar()
                'Kredit
                classAkuntansi.RekeningJurnal = cKredit
                classAkuntansi.Kredit = nNominalTabungan
                classAkuntansi.UpdBukuBesar()
              Else
                'Debet
                classAkuntansi.RekeningJurnal = cDebet
                classAkuntansi.Debet = nNominalTabungan
                classAkuntansi.UpdBukuBesar()
                'Kredit
                classAkuntansi.RekeningJurnal = cKredit
                classAkuntansi.Kredit = nNominalTabungan
                classAkuntansi.UpdBukuBesar()
                'Debet
                classAkuntansi.RekeningJurnal = cDebet1
                classAkuntansi.Debet = nNominalTabungan
                classAkuntansi.UpdBukuBesar()
                'Kredit
                classAkuntansi.RekeningJurnal = cKredit1
                classAkuntansi.Kredit = nNominalTabungan
                classAkuntansi.UpdBukuBesar()
              End If
            End If
          Else
            If cDK = "D" Then
              ' Dr. Tabungan     nNominalTabungan
              '     Cr. Rekening Kredit    nJumlah
              cDebet = cRekeningTabungan
              cKredit = GetNull(cRekeningKodeTransaksi.ToString, "").ToString
            Else
              ' Dr. Rekening Debet     nJumlah
              '     Cr. Tabungan          nNominalTabungan
              cDebet = GetNull(cRekeningKodeTransaksi.ToString, "").ToString
              cKredit = cRekeningTabungan
            End If
            'Debet
            classAkuntansi.RekeningJurnal = cDebet
            classAkuntansi.Debet = nNominalTabungan
            classAkuntansi.Kredit = 0
            classAkuntansi.UpdBukuBesar()
            'Kredit
            classAkuntansi.RekeningJurnal = cKredit
            classAkuntansi.Debet = 0
            classAkuntansi.Kredit = nNominalTabungan
            classAkuntansi.UpdBukuBesar()
            If cKodeTransaksiTemp = aCfg(eCfg.msKodePenutupanTabungan).ToString Then
              'Debet
              classAkuntansi.RekeningJurnal = cRekeningKodeTransaksi
              classAkuntansi.Debet = nNominalTabungan
              classAkuntansi.UpdBukuBesar()
              'Kredit
              classAkuntansi.RekeningJurnal = GetNull(.Item("RekeningKodeTransaksi"), "").ToString
              classAkuntansi.Kredit = nNominalTabungan
              classAkuntansi.UpdBukuBesar()
            End If
          End If
        End With
        n = n + 1
      Loop
    End If
  End Sub

  Sub UpdPembukaanTabungan()
    Dim myConnection As New DataBaseConnection() With {.Database = _cDatabase,
                                                       .IP = _cIPNumber,
                                                       .Port = _cPort,
                                                       .SQL = String.Format("delete from tabungan where rekening = '{0}'", _cRekening)}
    myConnection.Delete()

    myConnection.InitConnection()
    SqlString = "insert into tabungan (rekening, tgl, golongannasabah, golongantabungan, kode, ao, wilayah, keterkaitan, cabangentry, username, jenisnasabah, karyawan) "
    SqlString = SqlString & "values (@rekening, @tgl, @golongannasabah, @golongantabungan, @kode, @ao, @wilayah, @keterkaitan, @cabangentry, @username, @jenisnasabah, @karyawan)"
    Dim cmd As MySqlCommand = New MySqlCommand(SqlString, myConnection.OpenConnection)
    With cmd.Parameters
      .AddWithValue("@rekening", _cRekening)
      .AddWithValue("@tgl", _dTanggal)
      .AddWithValue("@golongannasabah", _cGolonganNasabah)
      .AddWithValue("@golongantabungan", _cGolonganTabungan)
      .AddWithValue("@kode", _cKodeRegister)
      .AddWithValue("@ao", _cAO)
      .AddWithValue("@wilayah", _cWilayah)
      .AddWithValue("@keterkaitan", _cKeterkaitan)
      .AddWithValue("@cabangentry", _cCabang)
      .AddWithValue("@username", _cUsername)
      .AddWithValue("@jenisnasabah", _cJenisNasabah)
      .AddWithValue("@karyawan", _cKaryawan)
    End With
    cmd.ExecuteNonQuery()
    cmd.Dispose()
    myConnection.CloseConnection()
    myConnection.Dispose()
  End Sub

  Function GetSaldoEfektif() As Double
    Dim myConnection As New DataBaseConnection()
    Dim nSaldoMinimum As Double = 0
    myConnection.Database = _cDatabase
    myConnection.IP = _cIPNumber
    myConnection.Port = _cPort
    Dim cSQL As String = "select g.saldominimum from tabungan t "
    cSQL = cSQL & "left join golongantabungan g on t.golongantabungan = g.kode "
    cSQL = String.Format("{0}where t.rekening = '{1}' ", cSQL, _cRekening)
    myConnection.SQL = cSQL
    Dim cDataTable As DataTable = myConnection.Browse()
    If Not IsNothing(cDataTable) Then
      nSaldoMinimum = CDbl(cDataTable.Rows(0).Item("saldominimum").ToString)
    End If
    Dim nSaldoTabungan As Double = GetSaldoTabungan()
    Dim nTemp As Double
    If _lWithSaldoMinimum Then
      nTemp = nSaldoMinimum
    Else
      nTemp = 0
    End If
    GetSaldoEfektif = nSaldoTabungan - nTemp
  End Function

  Function GetSaldoTabungan(Optional ByVal nID As Double = 999999999999.0#, Optional ByVal lTabungan As Boolean = True) As Double
    Dim myConnection As New DataBaseConnection()
    Dim nAkhir As Double

    myConnection.Database = _cDatabase
    myConnection.IP = _cIPNumber
    myConnection.Port = _cPort
    ' ambil saldo sampai kemarin
    myConnection.SQL = String.Format("select sum(kredit-debet) as mutasi from mutasitabungan where rekening = '{0}' and tgl < '{1}' ", _cRekening, formatValue(_dTanggal, formatType.yyyy_MM_dd))
    Dim cDataTable As DataTable = myConnection.Browse()
    If Not IsNothing(cDataTable) Then
      nAkhir = Val(GetNull(cDataTable.Rows(0).Item("mutasi").ToString))
    Else
      nAkhir = 0
    End If

    ' ambil saldo hari ini
    myConnection.SQL = String.Format("select sum(kredit-debet) as mutasi from mutasitabungan where rekening = '{0}' and tgl = '{1}' and ID < {2}", _cRekening, formatValue(_dTanggal, formatType.yyyy_MM_dd), nID)
    cDataTable = myConnection.Browse()
    If Not IsNothing(cDataTable) Then
      nAkhir = nAkhir + Val(GetNull(cDataTable.Rows(0).Item("mutasi").ToString))
    End If

    If lTabungan And nAkhir < 0 Then
      nAkhir = 0
    End If
    cDataTable.Dispose()
    GetSaldoTabungan = nAkhir
  End Function

  Sub DelMutasiTabungan()
    Dim myConnection As New DataBaseConnection()
    Dim classAkuntansi As New ClassAkuntansi
    If Trim(_cFaktur) <> "" Then
      ' 1. Buka Kembali Status Rekening
      ' 2. Hapus pada table Mutasi
      ' 3. Hapus pada Table Buku Besar

      ' Buka Status Rekening jika jenis transaksi adalah
      ' penutupan rekening
      myConnection.Database = _cDatabase
      myConnection.IP = _cIPNumber
      myConnection.Port = _cPort
      If _cKodeTransaksi = aCfg(eCfg.msKodeAdministrasi).ToString Then
        myConnection.SQL = String.Format("select rekening from mutasitabungan where faktur = '{0}' ", _cFaktur)
        Dim db As DataTable = myConnection.Browse()
        If Not IsNothing(db) Then
          For n As Integer = 0 To db.Rows.Count - 1
            SqlString = String.Format("update tabungan set tglpenutupan = @TglPenutupan, close = @close where rekening = '{0}'", db.Rows(n).Item("rekening").ToString)
            myConnection.InitConnection()
            Dim cmd As MySqlCommand = New MySqlCommand(SqlString, myConnection.OpenConnection)
            With cmd.Parameters
              .AddWithValue("@TglPenutupan", "9999-12-31")
              .AddWithValue("@Close", "0")
            End With
            cmd.ExecuteNonQuery()
            cmd.Dispose()
            myConnection.CloseConnection()
            myConnection.Dispose()
          Next
        End If
        db.Dispose()
      End If
      ' Hapus Table Mutasi
      myConnection.SQL = String.Format("delete from mutasitabungan where faktur = '{0}' ", _cFaktur)
      myConnection.Delete()

      ' Hapus Table Buku Besar
      If _lDeleteExistingBukuBesar Then
        classAkuntansi.Database = _cDatabase
        classAkuntansi.IPNumber = _cIPNumber
        classAkuntansi.Port = _cPort
        classAkuntansi.Faktur = _cFaktur
        classAkuntansi.DelBukuBesar()
      End If
    End If
  End Sub

  Function GetRptBungaHarian() As DataTable
    Dim myConnection As New DataBaseConnection()
    Dim classOther As New ClassOther
    Dim n As Integer
    Dim vaArray As New DataTable
    Dim nAwal As Double = 0
    Dim nSaldoPajak As Double = 0
    Dim nSaldoPajakTanpaGabungan As Double = 0
    Dim nSaldoKenaPajak As Double
    Dim cKodeSukuBunga As String
    Dim cWajibPajak As String = "Y"
    Dim nSaldoMinimum As Double
    Dim nPajakBunga As Single
    Dim nAdministrasiPasif As Double
    Dim db As New DataTable
    Dim nSaldoMinimumDapatBunga As Double
    Dim dbData As New DataTable

    myConnection.Database = _cDatabase
    myConnection.IP = _cIPNumber
    myConnection.Port = _cPort
    Dim cSQL As String = "select saldominimumdapatbunga, sukubunga, pajakbunga, saldominimum, adminpasif, saldominimumkenapajak "
    cSQL = cSQL & "from golongantabungan "
    cSQL = String.Format("{0}where kode = '{1}' ", cSQL, _cGolonganTabungan)
    myConnection.SQL = cSQL
    db = myConnection.Browse()
    If Not IsNothing(db) Then
      With db.Rows(0)
        nSaldoMinimumDapatBunga = CDbl(.Item("SaldoMinimumDapatBunga"))
        cKodeSukuBunga = .Item("SukuBunga").ToString
        nPajakBunga = CSng(.Item("PajakBunga"))
        cWajibPajak = IIf(CDbl(.Item("PajakBunga")) = 0, "T", "Y").ToString
        nSaldoMinimum = CDbl(GetNull(.Item("SaldoMinimum")))
        nAdministrasiPasif = CDbl(GetNull(.Item("AdminPasif"), 0))
        nSaldoKenaPajak = CDbl(GetNull(.Item("SaldoMinimumKenaPajak")))
      End With
    End If

    ' Ambil Saldo Awal
    myConnection.SQL = String.Format("select sum(kredit-debet) as awal from mutasitabungan where rekening = '{0}' and tgl < '{1}' ", _cRekening, formatValue(_dTanggalAwal, formatType.yyyy_MM_dd))
    dbData = myConnection.Browse()
    If Not IsNothing(dbData) Then
      nAwal = CDbl(GetNull(dbData.Rows(0).Item("Awal")))
    End If
    _nSaldoAwal = nAwal
    Dim nHari As Integer = CInt(DateDiff(DateInterval.Day, _dTanggalAwal, _dTanggalAkhir))
    vaArray.Reset()
    objDataTable = vaArray
    AddColumn("tgl", eGridColumnType.eDatetime)
    AddColumn("saldo", eGridColumnType.eDouble)
    AddColumn("sukubunga", eGridColumnType.eSingle)
    AddColumn("rumus", eGridColumnType.eString)
    AddColumn("bunga", eGridColumnType.eDouble)
    AddColumn("pajak", eGridColumnType.eDouble)
    AddColumn("bungabersih", eGridColumnType.eDouble)
    AddColumn("saldogabungantabungan", eGridColumnType.eDouble)
    AddColumn("saldogabungandeposito", eGridColumnType.eDouble)
    AddColumn("jumlahsaldogabungan", eGridColumnType.eDouble)
    Dim row As DataRow
    For n = 0 To nHari
      row = vaArray.NewRow()
      row("tgl") = CDate("1900-01-01")
      row("saldo") = 0
      row("sukubunga") = 0
      row("rumus") = ""
      row("bunga") = 0
      row("pajak") = 0
      row("bungabersih") = 0
      row("saldogabungantabungan") = 0
      row("saldogabungandeposito") = 0
      row("jumlahsaldogabungan") = 0
      vaArray.Rows.Add(row)
    Next
    ' Ambil Mutasi Tabungan
    n = 0
    _nMutasiDebet = 0
    _nMutasiKredit = 0
    cSQL = "select tgl, kredit, debet from mutasitabungan "
    cSQL = String.Format("{0}where rekening = '{1}' and tgl >= '{2}' ", cSQL, _cRekening, formatValue(_dTanggalAwal, formatType.yyyy_MM_dd))
    cSQL = String.Format("{0}and tgl <= '{1}' ", cSQL, formatValue(_dTanggalAkhir, formatType.yyyy_MM_dd))
    cSQL = String.Format("{0}and kodetransaksi <> '{1}' and kodetransaksi <> '{2}' ", cSQL, aCfg(eCfg.msKodeBunga), aCfg(eCfg.msKodePajakBunga))
    cSQL = cSQL & "order by rekening, tgl, id "
    myConnection.SQL = cSQL
    dbData = myConnection.Browse()
    If Not IsNothing(dbData) Then
      For m = 0 To dbData.Rows.Count - 1
        With dbData.Rows(m)
          If CDate(.Item("Tgl").ToString) < _dTanggalAwal Then
            n = 0
          Else
            n = CInt(DateDiff(DateInterval.Day, _dTanggalAwal, CDate(.Item("Tgl").ToString)))
          End If
          vaArray.Rows(n).Item("saldo") = CDbl(GetNull(vaArray.Rows(n).Item("saldo"))) + CDbl(.Item("Kredit")) - CDbl(.Item("debet"))
          _nMutasiDebet += CDbl(.Item("debet"))
          _nMutasiKredit += CDbl(.Item("Kredit"))
        End With
      Next
    End If

    Dim nSaldo As Double = _nSaldoAwal
    _nTotalBunga = 0
    _nTotalPajak = 0
    'Tanggal,saldo,rate,rumus,bunga,pajak,bungabersih
    For n = 0 To vaArray.Rows.Count - 1
      With vaArray.Rows(n)
        nSaldo += CDbl(GetNull(.Item("saldo")))
        .Item(0) = DateAdd(DateInterval.Day, n, _dTanggalAwal)
        classOther.Database = _cDatabase
        classOther.IPNumber = _cIPNumber
        classOther.Port = _cPort
        classOther.Tanggal = CDate(.Item("tgl").ToString)
        classOther.KodeRegister = _cKodeRegister
        If Not _lProsesPostingBunga Then
          classOther.HitungSaldoPerModul = True
          classOther.HitungSaldoTabungan = True
          classOther.HitungSaldoDeposito = False
          .Item("saldogabungantabungan") = classOther.GetSaldoNasabah
          _nTotalSaldoTabungan = CDbl(.Item("saldogabungantabungan"))
          classOther.HitungSaldoTabungan = False
          classOther.HitungSaldoDeposito = True
          .Item("saldogabungandeposito") = classOther.GetSaldoNasabah()
          _nTotalSaldoDeposito = CDbl(.Item("saldogabungandeposito"))
          .Item("jumlahsaldogabungan") = CDbl(.Item("saldogabungantabungan")) + CDbl(.Item("saldogabungandeposito"))
        Else
          .Item("saldogabungantabungan") = 0
          .Item("saldogabungandeposito") = 0
          ' saldo gabungan yang dihitung sementara ini adalah hanya modul tabungan saja, bukan tabungan dan deposito
          classOther.Tanggal = CDate(.Item("tgl").ToString)
          .Item("jumlahsaldogabungan") = classOther.GetSaldoSimpananRegister()
        End If
        nSaldoPajak = CDbl(.Item("jumlahsaldogabungan"))
        nSaldoPajakTanpaGabungan = nSaldo
        _nTotalSaldoTabunganDeposito = CDbl(.Item("jumlahsaldogabungan"))
        .Item("saldo") = nSaldo
        _nSaldoTabungan = nSaldo
        .Item("sukubunga") = GetSukuBunga()
        .Item("rumus") = String.Format("{0} * {1} % / 365", Format(CDbl(.Item("saldo")), "###,###,###,###,##0.00"), Format(CDbl(.Item("sukubunga")), "##0.00"))
        Dim nBungaTemp As Double = Math.Round(CDbl(.Item("saldo")) * CDbl(.Item("sukubunga")) / 36500, 2)
        .Item("bunga") = nBungaTemp
        ' perhitungan pajak dengan menggunakan saldo gabungan sementara ini tidak digunakan sampai ada kepastian dari pihak bank jombang
        Dim nPajakTemp As Double = Math.Round(CDbl(IIf(nSaldoPajak >= nSaldoKenaPajak, CDbl(.Item("bunga")) * 0.2, 0)), 2)
        .Item("pajak") = nPajakTemp
        .Item("bungabersih") = CDbl(.Item("bunga")) - CDbl(.Item("pajak"))
        If _lPajakOnly = True Then
          _nTotalBunga += CDbl(IIf(nSaldo >= nSaldoMinimumDapatBunga, CDbl(.Item("bunga")), 0))
          ' perhitungan pajak dengan menggunakan saldo gabungan sementara ini tidak digunakan sampai ada kepastian dari pihak bank jombang
          _nTotalPajak += CDbl(IIf(nSaldoPajak >= nSaldoKenaPajak, CDbl(.Item("pajak")), 0))
        Else
          _nTotalBunga += CDbl(.Item("bunga"))
          _nTotalPajak += CDbl(.Item("pajak"))
        End If
      End With
    Next
    _nTotalBunga = Math.Round(_nTotalBunga)
    If cWajibPajak <> "Y" Then
      _nTotalPajak = 0
    Else
      _nTotalPajak = Math.Round(_nTotalPajak)
    End If
    _nSaldoAkhir = nSaldo
    GetRptBungaHarian = vaArray
    If Not IsNothing(db) Then
      db.Dispose()
    End If
    If Not IsNothing(dbData) Then
      dbData.Dispose()
    End If
  End Function

  Function GetSukuBunga() As Double
    Dim myConnection As New DataBaseConnection()
    Dim db As New DataTable
    Dim nSaldo As Double

    If _nSaldoTabungan = 0 Then
      nSaldo = GetSaldoTabungan()
    Else
      nSaldo = _nSaldoTabungan
    End If
    myConnection.Database = _cDatabase
    myConnection.IP = _cIPNumber
    myConnection.Port = _cPort
    If _cGolonganTabungan = "" Then
      myConnection.SQL = String.Format("select golongantabungan from tabungan where rekening = '{0}' ", _cRekening)
      db = myConnection.Browse()
      If db.Rows.Count > 0 Then
        _cGolonganTabungan = db.Rows(0)(0).ToString
      End If
    End If
    Dim cSQL As String = String.Format("select sukubunga from detailsukubunga where kode = '{0}' ", _cGolonganTabungan)
    cSQL = String.Format("{0}and tgl <= '{1}' and minimum <= {2} and maximum >= {2} ", cSQL, formatValue(_dTanggal, formatType.yyyy_MM_dd), nSaldo)
    cSQL = cSQL & "order by kode, tgl desc, maximum limit 1"
    myConnection.SQL = cSQL
    db = myConnection.Browse()
    If Not IsNothing(db) Then
      GetSukuBunga = CDbl(GetNull(db.Rows(0).Item("SukuBunga")))
      db.Dispose()
    Else
      GetSukuBunga = 0
    End If
  End Function

  Function GetRekeningPassif() As Boolean
    Dim myConnection As New DataBaseConnection()
    Dim dOldTgl As Date
    Dim dbPasif As New DataTable
    Dim nSaldoMinimumDapatBunga As Double = 0
    Dim nSaldo As Double
    Dim dbData As New DataTable

    myConnection.Database = _cDatabase
    myConnection.IP = _cIPNumber
    myConnection.Port = _cPort
    GetRekeningPassif = True
    Dim cSQL As String = "select g.lamapasif,g.saldominimumdapatbunga from tabungan t "
    cSQL = cSQL & "left join golongantabungan g on g.kode = t.golongantabungan "
    cSQL = String.Format("{0}where t.rekening = '{1}'", cSQL, _cRekening)
    myConnection.SQL = cSQL
    dbPasif = myConnection.Browse()
    If Not IsNothing(dbPasif) Then
      dOldTgl = DateAdd(DateInterval.Month, -CDbl(GetNull(dbPasif.Rows(0).Item("LamaPasif"))), _dTanggal)
      nSaldoMinimumDapatBunga = CDbl(GetNull(dbPasif.Rows(0).Item("SaldoMinimumDapatBunga")))
    End If

    cSQL = "select kodetransaksi, tgl, jumlah from mutasitabungan "
    cSQL = String.Format("{0}where tgl >= '{1}' ", cSQL, formatValue(_dTanggal, formatType.yyyy_MM_dd))
    cSQL = String.Format("{0}and tgl <= '{1}' ", cSQL, formatValue(_dTanggal, formatType.yyyy_MM_dd))
    cSQL = String.Format("{0} and rekening = '{1}' ", cSQL, _cRekening)
    cSQL = String.Format("{0} and kodetransaksi <> '{1}' ", cSQL, aCfg(eCfg.msKodeAdmPemeliharaan))
    cSQL = String.Format("{0} and kodetransaksi <> '{1}' ", cSQL, aCfg(eCfg.msKodeBunga))
    cSQL = String.Format("{0} and kodetransaksi <> '{1}'", cSQL, aCfg(eCfg.msKodePajakBunga))
    cSQL = cSQL & "group by tgl, kodetransaksi order by rekening"
    myConnection.SQL = cSQL
    dbPasif = myConnection.Browse()
    If Not IsNothing(dbPasif) Then
      nSaldo = GetSaldoTabungan()
      cSQL = "select kodetransaksi, jumlah from mutasitabungan "
      cSQL = String.Format("{0}where rekening = '{1}' ", cSQL, _cRekening)
      cSQL = String.Format("{0}and tgl='{1}' ", cSQL, formatValue(_dTanggal, formatType.yyyy_MM_dd))
      cSQL = String.Format("{0}and (kodetransaksi ='{1}' ", cSQL, aCfg(eCfg.msKodeAdmPemeliharaan))
      cSQL = String.Format("{0}or kodetransaksi = '{1}' ", cSQL, aCfg(eCfg.msKodeBunga))
      cSQL = String.Format("{0}or kodetransaksi = '{1}')", cSQL, aCfg(eCfg.msKodePajakBunga))
      myConnection.SQL = cSQL
      dbData = myConnection.Browse()
      If Not IsNothing(dbData) Then
        For n = 0 To dbData.Rows.Count - 1
          If dbData.Rows(n).Item("KodeTransaksi").ToString = aCfg(eCfg.msKodeAdmPemeliharaan).ToString Then
            nSaldo += CDbl(GetNull(dbData.Rows(n).Item("Jumlah")))
          ElseIf dbData.Rows(n).Item("KodeTransaksi").ToString = aCfg(eCfg.msKodeBunga).ToString Then
            nSaldo -= CDbl(GetNull(dbData.Rows(n).Item("Jumlah")))
          ElseIf dbData.Rows(n).Item("KodeTransaksi").ToString = aCfg(eCfg.msKodePajakBunga).ToString Then
            nSaldo += CDbl(GetNull(dbData.Rows(n).Item("Jumlah")))
          End If
        Next
      End If
      ' jika saldo dibawah saldo minimum dapat bunga maka statusnya langsung pasif
      If nSaldo <= nSaldoMinimumDapatBunga Then
        GetRekeningPassif = True
      Else
        GetRekeningPassif = False
      End If
    End If
    ' jika ada transaksi di bulan tanggal awal parameter, maka masih dianggap aktif
    cSQL = "select faktur, debet, kredit from mutasitabungan "
    cSQL = String.Format("{0}where rekening = '{1}' ", cSQL, _cRekening)
    cSQL = String.Format("{0} and tgl>='{1}' ", cSQL, formatValue(BOM(dOldTgl), formatType.yyyy_MM_dd))
    cSQL = String.Format("{0} and tgl <= '{1}' ", cSQL, formatValue(EOM(dOldTgl), formatType.yyyy_MM_dd))
    cSQL = String.Format("{0} and kodetransaksi <> '{1}' ", cSQL, aCfg(eCfg.msKodeAdmPemeliharaan))
    cSQL = String.Format("{0} and kodetransaksi <> '{1}' ", cSQL, aCfg(eCfg.msKodeBunga))
    cSQL = String.Format("{0} and kodetransaksi <> '{1}'", cSQL, aCfg(eCfg.msKodePajakBunga))
    myConnection.SQL = cSQL
    dbPasif = myConnection.Browse()
    If Not IsNothing(dbPasif) And GetRekeningPassif Then
      If nSaldo > nSaldoMinimumDapatBunga Then
        GetRekeningPassif = False
      End If
    End If
    If Not IsNothing(dbPasif) Then
      dbPasif.Dispose()
    End If
  End Function
End Class
