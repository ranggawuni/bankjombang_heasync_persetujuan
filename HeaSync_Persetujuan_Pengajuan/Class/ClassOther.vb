﻿Public Class ClassOther
  Private _cDatabase As String = ""
  Private _cIPNumber As String = ""
  Private _cPort As String = ""
  Private _dTgl As Date = Today.Date
  Private _cKode As String = ""
  Private _dJthTmp As Date = #1/1/1900#
  Private _lPajakSaatJatuhTempo As Boolean = False
  Private _lSaldoPerModul As Boolean = False
  Private _lTabungan As Boolean = True
  Private _lDeposito As Boolean = True

  Public Property HitungSaldoDeposito As Boolean
    Get
      Return _lDeposito
    End Get
    Set(value As Boolean)
      _lDeposito = value
    End Set
  End Property

  Public Property HitungSaldoTabungan As Boolean
    Get
      Return _lTabungan
    End Get
    Set(value As Boolean)
      _lTabungan = value
    End Set
  End Property

  Public Property HitungSaldoPerModul As Boolean
    Get
      Return _lSaldoPerModul
    End Get
    Set(value As Boolean)
      _lSaldoPerModul = value
    End Set
  End Property

  Public Property HitungPajakSaatJatuhTempo As Boolean
    Get
      Return _lPajakSaatJatuhTempo
    End Get
    Set(value As Boolean)
      _lPajakSaatJatuhTempo = value
    End Set
  End Property

  Public Property JatuhTempo As Date
    Get
      Return _dJthTmp
    End Get
    Set(value As Date)
      _dJthTmp = value
    End Set
  End Property

  Public Property KodeRegister As String
    Get
      Return _cKode
    End Get
    Set(value As String)
      _cKode = value
    End Set
  End Property

  Public Property Tanggal As Date
    Get
      Return _dTgl
    End Get
    Set(value As Date)
      _dTgl = value
    End Set
  End Property

  Public Property Port As String
    Get
      Return _cPort
    End Get
    Set(value As String)
      _cPort = value
    End Set
  End Property

  Public Property IPNumber As String
    Get
      Return _cIPNumber
    End Get
    Set(value As String)
      _cIPNumber = value
    End Set
  End Property

  Public Property Database As String
    Get
      Return _cDatabase
    End Get
    Set(value As String)
      _cDatabase = value
    End Set
  End Property


  Function GetSaldoSimpananRegister() As Double
    Dim db As New DataTable
    Dim myConnection As New DataBaseConnection
    Dim classTabungan As New ClassTabungan
    Dim cSQL As String = ""
    Dim nSaldo As Double = 0
    Dim dTanggalcair As Date
    Dim db1 As New DataTable
    Dim dTglTemp As Date
    Dim dbRegister As New DataTable
    If _lPajakSaatJatuhTempo Then
      dTglTemp = _dJthTmp
    Else
      dTglTemp = _dTgl
    End If

    myConnection.Database = _cDatabase
    myConnection.IP = _cIPNumber
    myConnection.Port = _cPort
    myConnection.SQL = String.Format("select kode, kodeinduk from registernasabah where kodeinduk = '{0}' ", _cKode)
    dbRegister = myConnection.Browse
    If Not IsNothing(dbRegister) Then
      For n As Integer = 0 To dbRegister.Rows.Count - 1
        myConnection.SQL = String.Format("select rekening from tabungan where kode = '{0}' ", dbRegister.Rows(n)("Kode"))
        db = myConnection.Browse
        If Not IsNothing(db) Then
          For i As Integer = 0 To db.Rows.Count - 1
            classTabungan.Database = _cDatabase
            classTabungan.IPNumber = _cIPNumber
            classTabungan.Port = _cPort
            classTabungan.Rekening = db.Rows(i)("Rekening").ToString
            classTabungan.Tanggal = dTglTemp
            nSaldo = nSaldo + classTabungan.GetSaldoTabungan
          Next
        End If

        cSQL = "select rekening, tglcair, tgl, status, nominal from deposito "
        cSQL = String.Format("{0}where kode = '{1}'", cSQL, dbRegister.Rows(n).Item("Kode").ToString)
        myConnection.SQL = cSQL
        db = myConnection.Browse
        If Not IsNothing(db) Then
          For i As Integer = 0 To db.Rows.Count - 1
            With db.Rows(i)
              dTanggalcair = CDate(IIf(GetNull(.Item("TglCair").ToString, "9999-12-31").ToString = "9999-99-99", "9999-12-31", .Item("TglCair")))
              If CDate(.Item("Tgl").ToString) <= _dTgl Then
                If Format(dTanggalcair, "yyyymm") >= Format(_dTgl, "yyyymm") Then
                  nSaldo = nSaldo + CDbl(GetNull(.Item("Nominal")))
                  cSQL = "select Sum(SetoranPlafond) as SetoranPlafond, Max(Tgl) as Tgl, Max(JTHTMP) as JT, "
                  cSQL = cSQL & "max(CaraPencairan) as CaraPencairan, Sum(PencairanPlafond) as PencairanPlafond "
                  cSQL = cSQL & "from mutasideposito "
                  cSQL = String.Format("{0}where carapencairan = 'E' and Rekening = '{1}' ", cSQL, .Item("Rekening"))
                  cSQL = String.Format("{0}and date_format(Tgl, '%Y%m') <= date_format('{1}', '%Y%m')", cSQL, formatValue(_dTgl, formatType.yyyy_MM_dd))
                  myConnection.SQL = cSQL
                  db1 = myConnection.Browse
                  If Not IsNothing(db1) Then
                    nSaldo = nSaldo + CDbl(GetNull(db1.Rows(0).Item("SetoranPlafond")))
                  End If
                End If
              End If
            End With
          Next
        End If
      Next
      If Not IsNothing(db1) Then
        db1.Dispose()
      End If
      If Not IsNothing(db) Then
        db.Dispose()
      End If
      If Not IsNothing(dbRegister) Then
        dbRegister.Dispose()
      End If
    End If
    GetSaldoSimpananRegister = nSaldo
  End Function

  Function GetSaldoNasabah() As Double
    Dim db As New DataTable
    Dim classTabungan As New ClassTabungan
    Dim myConnection As New DataBaseConnection
    Dim nTotal As Double = 0
    Dim dTanggalcair As Date
    Dim db1 As New DataTable
    Dim dTglTemp As Date
    Const cFieldCriteria As String = "Kode"
    Dim nSaldoTabungan As Double = 0
    Dim nSaldoDeposito As Double = 0
    Dim n As Integer
    Dim cSQL As String = ""

    If Not _lSaldoPerModul Then
      _lTabungan = True
      _lDeposito = True
    End If
    If _lPajakSaatJatuhTempo Then
      dTglTemp = _dJthTmp
    Else
      dTglTemp = _dTgl
    End If
    myConnection.Database = _cDatabase
    myConnection.IP = _cIPNumber
    myConnection.Port = _cPort
    'myConnection.SQL = String.Format("select rekening from tabungan where {0} = '{1}' ", cFieldCriteria, _cKode)
    'db = myConnection.Browse
    'If Not IsNothing(db) Then
    '  For n = 0 To db.Rows.Count - 1
    '    classTabungan.Database = _cDatabase
    '    classTabungan.IPNumber = _cIPNumber
    '    classTabungan.Port = _cPort
    '    classTabungan.Rekening = db.Rows(n)("Rekening").ToString
    '    classTabungan.Tanggal = dTglTemp
    '    nTotal = nTotal + classTabungan.GetSaldoTabungan
    '    nSaldoTabungan = nTotal
    '  Next
    'End If
    If _lTabungan Then
      cSQL = "select t.rekening, ifnull(sum(m.kredit-m.debet),0) as saldo from tabungan t "
      cSQL = cSQL & "left join mutasitabungan m on m.rekening = t.rekening and m.tgl <= '" & formatValue(dTglTemp, formatType.yyyy_MM_dd) & "' "
      cSQL = cSQL & "where kode = '" & _cKode & "' "
      ' untuk mengecek saldo per rekening remarknya dihilangkan
      'cSQL = cSQL & "group by t.rekening"
      myConnection.SQL = cSQL
      db = myConnection.Browse
      If Not IsNothing(db) Then
        nSaldoTabungan = CDbl(db.Rows(0).Item("saldo"))
        nTotal = nSaldoTabungan
      End If
    End If

    If _lDeposito Then
      Dim dTglCair As String
      myConnection.SQL = String.Format("select rekening, tglcair, tgl, status, nominal from deposito where {0} = '{1}' ", cFieldCriteria, _cKode)
      db = myConnection.Browse
      If Not IsNothing(db) Then
        For n = 0 To db.Rows.Count - 1
          With db.Rows(n)
            dTglCair = GetNull(.Item("TglCair"), "9999-12-31").ToString
            If dTglCair.ToString = "9999-99-99" Then
              dTanggalcair = CDate("9999-12-31")
            Else
              dTanggalcair = CDate(.Item("TglCair").ToString)
            End If
            If CDate(.Item("Tgl").ToString) <= _dTgl Then
              Dim dTanggalCairTemp As String = Format(dTanggalcair, "yyyyMM")
              Dim dTanggalTemp As String = Format(_dTgl, "yyyyMM")
              If dTanggalCairTemp >= dTanggalTemp Then
                nTotal = nTotal + Val(GetNull(.Item("Nominal")))
                cSQL = "select Sum(SetoranPlafond) as SetoranPlafond, Max(Tgl) as Tgl, Max(JTHTMP) as JT, "
                cSQL = cSQL & "max(CaraPencairan) as CaraPencairan, Sum(PencairanPlafond) as PencairanPlafond "
                cSQL = cSQL & "from mutasideposito "
                cSQL = cSQL & "where carapencairan = 'I' "
                cSQL = String.Format("{0}and Rekening = '{1}' ", cSQL, .Item("Rekening"))
                cSQL = String.Format("{0}and date_format(Tgl,'%Y%m') <= date_format('{1}', '%Y%m')", cSQL, formatValue(_dTgl, formatType.yyyy_MM_dd))
                myConnection.SQL = cSQL
                db1 = myConnection.Browse
                If Not IsNothing(db1) Then
                  nTotal = nTotal + Val(GetNull(db1.Rows(0).Item("SetoranPlafond")))
                  nSaldoDeposito = nTotal
                End If
              End If
            End If
          End With
        Next
      End If
    End If
    'If _lSaldoPerModul Then
    '  If _lTabungan Then
    '    nTotal = nSaldoTabungan
    '  End If
    '  If _lDeposito Then
    '    nTotal = nSaldoDeposito
    '  End If
    'End If
    GetSaldoNasabah = nTotal
  End Function
End Class
