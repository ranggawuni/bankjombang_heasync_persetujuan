﻿Imports System.Text.RegularExpressions

Public Class ClassHistory
  Private _cDatabase As String = ""
  Private _cIPNumber As String = ""
  Private _cPort As String = ""
  Private _cUserName As String = ""
  Private _cSQL As String = ""
  Private _objCmd As MySqlCommand = Nothing

  Public Property ObjCmd As MySqlCommand
    Get
      Return _objCmd
    End Get
    Set(value As MySqlCommand)
      _objCmd = value
    End Set
  End Property

  Public Property SQL As String
    Get
      Return _cSQL
    End Get
    Set(value As String)
      _cSQL = value
    End Set
  End Property

  Public Property Username As String
    Get
      Return _cUserName
    End Get
    Set(value As String)
      _cUserName = value
    End Set
  End Property

  Public Property Port As String
    Get
      Return _cPort
    End Get
    Set(value As String)
      _cPort = value
    End Set
  End Property

  Public Property IPNumber As String
    Get
      Return _cIPNumber
    End Get
    Set(value As String)
      _cIPNumber = value
    End Set
  End Property

  Public Property Database As String
    Get
      Return _cDatabase
    End Get
    Set(value As String)
      _cDatabase = value
    End Set
  End Property

  Private Sub checkTableName1()
    Static lCheck As Boolean
    Dim lTableFound As Boolean
    Dim cSQL As String
    Dim dbData As New DataTable
    Dim myConnection As New DataBaseConnection

    If Not lCheck Then
      lTableFound = False
      myConnection.Database = _cDatabase
      myConnection.IP = _cIPNumber
      myConnection.Port = _cPort
      myConnection.SQL = "show Tables from mysql"
      dbData = myConnection.Browse
      If Not IsNothing(dbData) Then
        Dim cTableName As String = ""
        For n = 0 To dbData.Rows.Count - 1
          cTableName = dbData.Rows(n).Item(0).ToString
          If cTableName.ToLower.Trim = "db_history" Then
            lTableFound = True
            Exit For
          End If
        Next
      End If
      If Not lTableFound Then
        cSQL = "CREATE TABLE `mysql`.`db_history` ("
        cSQL = cSQL & "`ID` VARCHAR(100) NULL DEFAULT '',"
        cSQL = cSQL & "`IDCabang` BIGINT(20) NOT NULL AUTO_INCREMENT,"
        cSQL = cSQL & "`Cabang` CHAR(2) NOT NULL DEFAULT '',"
        cSQL = cSQL & "`Tgl` DATE NULL DEFAULT '0000-00-00',"
        cSQL = cSQL & "`DataBaseName` VARCHAR(100) NULL DEFAULT '',"
        cSQL = cSQL & "`TableName` VARCHAR(100) NULL DEFAULT '',"
        cSQL = cSQL & "`Activity` CHAR(1) NULL DEFAULT '',"
        cSQL = cSQL & "`History` TEXT NULL,"
        cSQL = cSQL & "`UserName` VARCHAR(100) NULL DEFAULT '',"
        cSQL = cSQL & "`DateTime` DATETIME NULL DEFAULT '0000-00-00 00:00:00',"
        cSQL = cSQL & "PRIMARY KEY (`Cabang`, `IDCabang`),"
        cSQL = cSQL & "INDEX `UserTgl` (`UserName`, `Tgl`, `DataBaseName`, `TableName`, `Activity`),"
        cSQL = cSQL & "INDEX `Database` (`DataBaseName`, `TableName`, `Tgl`, `ID`),"
        cSQL = cSQL & "INDEX `DatabaseActivity` (`DataBaseName`, `TableName`, `Activity`, `Tgl`, `ID`))"
        cSQL = cSQL & "COLLATE='utf8_general_ci'"
        cSQL = cSQL & "ENGINE = MyISAM AUTO_INCREMENT=0;"
        myConnection.SQL = cSQL
        myConnection.Query()
      End If
      lTableFound = False

      myConnection.SQL = "show tables from mysql"
      dbData = myConnection.Browse
      If Not IsNothing(dbData) Then
        Dim cTableName As String = ""
        For n = 0 To dbData.Rows.Count - 1
          cTableName = dbData.Rows(n).Item(0).ToString
          If cTableName.ToLower.Trim = "db_lastid" Then
            lTableFound = True
            Exit For
          End If
        Next
      End If
      If Not lTableFound Then
        cSQL = "CREATE TABLE `mysql`.`db_lastid` ("
        cSQL = cSQL & "`Cabang` CHAR(2) NULL DEFAULT '',"
        cSQL = cSQL & "`DataBaseName` VARCHAR(100) NULL DEFAULT '',"
        cSQL = cSQL & "`ID` BIGINT(20) NOT NULL DEFAULT '0')"
        cSQL = cSQL & "COLLATE='utf8_general_ci'"
        cSQL = cSQL & "ENGINE=MyISAM;"
        myConnection.SQL = cSQL
        myConnection.Query()
      End If
      lCheck = True
    End If

  End Sub

  Private Sub checkTableName()
    Static lCheck As Boolean
    Dim lTableFound As Boolean
    Dim cSQL As String
    Dim dbData As New DataTable
    Dim myConnection As New DataBaseConnection
    Dim lFoundHistory As Boolean = False
    Dim lFoundLastID As Boolean = False

    If Not lCheck Then
      lTableFound = False
      myConnection.Database = _cDatabase
      myConnection.IP = _cIPNumber
      myConnection.Port = _cPort
      cSQL = "show tables from mysql where tables_in_mysql='db_history' or tables_in_mysql='db_lastid'"
      myConnection.SQL = cSQL
      dbData = myConnection.Browse
      If Not IsNothing(dbData) Then
        Dim cTableName As String = ""
        For n = 0 To dbData.Rows.Count - 1
          cTableName = dbData.Rows(n).Item(0).ToString
          If cTableName.ToLower.Trim = "db_history" Then
            lFoundHistory = True
          ElseIf cTableName.ToLower.Trim = "db_lastid" Then
            lFoundLastID = True
          End If
        Next
      End If
      If Not lFoundHistory Then
        cSQL = "CREATE TABLE `mysql`.`db_history` ("
        cSQL = cSQL & "`ID` VARCHAR(100) NULL DEFAULT '',"
        cSQL = cSQL & "`IDCabang` BIGINT(20) NOT NULL AUTO_INCREMENT,"
        cSQL = cSQL & "`Cabang` CHAR(2) NOT NULL DEFAULT '',"
        cSQL = cSQL & "`Tgl` DATE NULL DEFAULT '0000-00-00',"
        cSQL = cSQL & "`DataBaseName` VARCHAR(100) NULL DEFAULT '',"
        cSQL = cSQL & "`TableName` VARCHAR(100) NULL DEFAULT '',"
        cSQL = cSQL & "`Activity` CHAR(1) NULL DEFAULT '',"
        cSQL = cSQL & "`History` TEXT NULL,"
        cSQL = cSQL & "`UserName` VARCHAR(100) NULL DEFAULT '',"
        cSQL = cSQL & "`DateTime` DATETIME NULL DEFAULT '0000-00-00 00:00:00',"
        cSQL = cSQL & "PRIMARY KEY (`Cabang`, `IDCabang`),"
        cSQL = cSQL & "INDEX `UserTgl` (`UserName`, `Tgl`, `DataBaseName`, `TableName`, `Activity`),"
        cSQL = cSQL & "INDEX `Database` (`DataBaseName`, `TableName`, `Tgl`, `ID`),"
        cSQL = cSQL & "INDEX `DatabaseActivity` (`DataBaseName`, `TableName`, `Activity`, `Tgl`, `ID`))"
        cSQL = cSQL & "COLLATE='utf8_general_ci'"
        cSQL = cSQL & "ENGINE = MyISAM AUTO_INCREMENT=0;"
        myConnection.SQL = cSQL
        myConnection.Query()
      End If
      If Not lFoundLastID Then
        cSQL = "CREATE TABLE `mysql`.`db_lastid` ("
        cSQL = cSQL & "`Cabang` CHAR(2) NULL DEFAULT '',"
        cSQL = cSQL & "`DataBaseName` VARCHAR(100) NULL DEFAULT '',"
        cSQL = cSQL & "`ID` BIGINT(20) NOT NULL DEFAULT '0')"
        cSQL = cSQL & "COLLATE='utf8_general_ci'"
        cSQL = cSQL & "ENGINE=MyISAM;"
        myConnection.SQL = cSQL
        myConnection.Query()
      End If
      lCheck = True
    End If

  End Sub

  Sub UpdateHistory()
    Dim myConnection As New DataBaseConnection() With {.Database = "mysql", .IP = _cIPNumber, .Port = _cPort}
    myConnection.InitConnection()
    Dim cSQLTemp As String = ""
    Dim cSQL As String = ""

    If Not IsNothing(ObjCmd) Then
      Dim cSqlString As String = _cSQL.Replace("@", "x")
      Dim cTemp As Object
      Dim cValue As Object
      For d As Integer = 0 To ObjCmd.Parameters.Count - 1
        cTemp = ObjCmd.Parameters(d).Value
        If TypeOf cTemp Is Double Or TypeOf cTemp Is Single Or TypeOf cTemp Is Integer Then
          cValue = cTemp
        Else
          cValue = String.Format("'{0}'", cTemp)
        End If
        Dim cParameter As String = ObjCmd.Parameters(d).ParameterName.Replace("@", "x")
        Dim cPattern As String = String.Format("\b{0}\b", cParameter)
        Dim cReplace As String = cValue.ToString
        cSqlString = Regex.Replace(cSqlString, cPattern, cReplace)
      Next
      cSQLTemp = cSqlString.ToLower
    Else
      cSQLTemp = _cSQL.ToLower
    End If

    cSQL = cSQLTemp
    checkTableName()

    Do While InStr(1, cSQLTemp, "  ") <> 0
      cSQLTemp = Replace(cSQLTemp, "  ", " ")
    Loop

    ' Check Jika Insert,UPdate,Delete
    Dim lUpdate As Boolean = False
    Dim cActivity As String = ""
    Dim cTableName As String = ""
    If InStr(1, cSQLTemp, "insert into ") <> 0 Then        ' Insert
      lUpdate = True
      cActivity = "1"

      cSQLTemp = Replace(cSQLTemp, "insert into ", "")
      cTableName = Split(cSQLTemp, " ")(0)
    ElseIf InStr(1, cSQLTemp, "update ") <> 0 Then        ' Update
      lUpdate = True
      cActivity = "2"

      cSQLTemp = Replace(cSQLTemp, "update ", "")
      cTableName = Split(cSQLTemp, " ")(0)
    ElseIf InStr(1, cSQLTemp, "delete from ") <> 0 Then    ' Delete
      lUpdate = True
      cActivity = "3"

      cSQLTemp = Replace(cSQLTemp, "delete from ", "")
      cTableName = Split(cSQLTemp, " ")(0)
    End If
    cTableName = cTableName.ToLower.Trim
    If lUpdate And cTableName <> "menunumber" And cTableName <> "request" And _
      cTableName <> "konfig" And cTableName <> "konfig" And cTableName <> "durasi" And _
      cTableName <> "pengesahan" And cTableName <> "userlogin" And _
      cTableName <> "userlogin_detail" And cTableName <> "laporan_kredit" And cTableName <> "laporan_tabungan" And _
      cTableName <> "laporan_deposito" And cTableName <> "laporan_neraca" And cTableName <> "mysql.db_lastid" And _
      cTableName <> "referensi_ver" And cTableName <> "debitur_kolek" And cTableName <> "`referensi_ver`" And _
      cTableName <> "mysql.cabang" And cTableName <> "tmpkolek" And cTableName <> "kolek" Then

      Dim cHistory As String = cSQL
      cHistory = Text2SQL(cHistory)

      ' Ambil Nama Database
      myConnection.Database = _cDatabase
      myConnection.IP = _cIPNumber
      myConnection.Port = _cPort
      myConnection.InitConnection()
      Dim cQuery As String = "insert into mysql.db_history (ID, Tgl, DatabaseName, Tablename, activity, history, username, datetime, cabang) "
      cQuery = cQuery & "values (concat(curdate(),' ', curtime()), @tgl, @databasename, @tablename, @activity, @history, @username, @datetime, @cabang) "
      Dim cmd As MySqlCommand = New MySqlCommand(cQuery, myConnection.OpenConnection)
      With cmd
        .Parameters.AddWithValue("@tgl", Today.Date)
        .Parameters.AddWithValue("@databasename", _cDatabase)
        .Parameters.AddWithValue("@tablename", cTableName)
        .Parameters.AddWithValue("@activity", cActivity)
        .Parameters.AddWithValue("@history", cHistory)
        .Parameters.AddWithValue("@username", _cUserName)
        .Parameters.AddWithValue("@datetime", formatValue(Now, formatType.yyyy_MM_dd_HH_mm_ss))
        .Parameters.AddWithValue("@cabang", aCfg(eCfg.msKodeCabang).ToString)
        .ExecuteNonQuery()
        .Dispose()
      End With
      myConnection.CloseConnection()
      myConnection.Dispose()
    End If
  End Sub

  Private Function Text2SQL(ByVal cText As String) As String
    cText = Replace(cText, Chr(0), "\0")
    cText = Replace(cText, "\", "\\")
    cText = Replace(cText, """", "\""")
    cText = Replace(cText, "'", "\'")
    Text2SQL = cText
  End Function
End Class
