﻿Public Class ClassPosting
  Private nBaris As DataRow()
  Private WithEvents Tray As NotifyIcon
  Private cSQL As String = ""

  Private _dTglPosting As Date
  Private _dTglAwalBunga As Date
  Private _dTglAkhirBunga As Date

  Public Property TrayApp As NotifyIcon
    Get
      Return Tray
    End Get
    Set(value As NotifyIcon)
      Tray = value
    End Set
  End Property

  Public Property TanggalBungaAkhir As Date
    Get
      Return _dTglAkhirBunga
    End Get
    Set(value As Date)
      _dTglAkhirBunga = value
    End Set
  End Property

  Public Property TanggalBungaAwal As Date
    Get
      Return _dTglAwalBunga
    End Get
    Set(value As Date)
      _dTglAwalBunga = value
    End Set
  End Property

  Public Property TanggalPosting As Date
    Get
      Return _dTglPosting
    End Get
    Set(value As Date)
      _dTglPosting = value
    End Set
  End Property

  <System.Diagnostics.CodeAnalysis.SuppressMessage("CodeRush", "String.Format can be used")>
  Public Sub PostingBunga1(Optional ByVal worker As System.ComponentModel.BackgroundWorker = Nothing,
                          Optional ByVal e As System.ComponentModel.DoWorkEventArgs = Nothing)
    Dim dbData As New DataTable
    Dim mc As New DataBaseConnection
    Dim classTabungan As New ClassTabungan
    Dim classAkuntansi As New ClassAkuntansi
    Dim nTotalBunga As Double
    Dim nTotalPajak As Double
    Dim n As Integer
    Dim cFaktur As String
    Dim cKodeCabang As String = aCfg(eCfg.msKodeCabang).ToString
    Dim cKodeBunga As String
    Dim cKodePajak As String
    Dim cF As String = String.Format("BT{0}{1}{2}", cKodeKantorInduk, Format(EOM(_dTglPosting), "yyMMdd"), cUserID)
    Dim vaGolonganTabungan As New DataTable
    Dim db As New DataTable
    Dim m As Integer
    Dim cRekeningJurnalPajak As String
    Dim vaArray As New DataTable
    Dim cFTemp As String = String.Format("BT{0}{1}", cKodeKantorInduk, Format(EOM(_dTglPosting), "yyMMdd"))
    Dim vaSukuBunga As New DataTable
    Dim nSaldoAkhir As Double
    Dim dbKas As New DataTable
    Dim cRekening As String = ""
    Dim cKodeTemp As String = ""
    Dim cGolonganTabungan As String = ""
    Dim cKeteranganPosting As String = ""

    If worker.CancellationPending Then
      e.Cancel = True
    Else

      cFaktur = cF & Padl("1", 4, "0")
      Dim cWaktu As DateTime = Now

      Dim x As New ClassXmlFile() With {.JenisDatabase = ClassXmlFile.enJenisDatabase.local, .Field = "databasename"}
      Dim cDatabase As String = x.GetValue
      x.Field = "ip"
      Dim cIP As String = x.GetValue
      x.Field = "port"
      Dim cPort As String = x.GetValue
      x.Field = "kode_kantor"
      cKodeCabang = x.GetValue
      mc.Database = cDatabase
      mc.IP = cIP
      mc.Port = cPort
      mc.Username = cUsername
      mc.UpdateHistory = True
      mc.SQL = String.Format("delete from mutasitabungan where faktur like '{0}%' ", cFTemp)
      mc.Delete()
      mc.SQL = String.Format("delete from bukubesar where faktur like '{0}%' ", cFTemp)
      mc.Delete()
      If cKodeCabang = "00" Then
        mc.SQL = "select kode from cabang"
      Else
        mc.SQL = String.Format("select kode from cabang where induk = '{0}' ", cKodeCabang)
      End If
      dbKas = mc.Browse
      If Not IsNothing(dbKas) Then
        mc.SQL = "select kode, rekening, rekeningbunga from golongantabungan order by kode "
        db = mc.Browse
        If Not IsNothing(db) Then
          vaGolonganTabungan = db.Copy
        Else
          vaGolonganTabungan.Reset()
        End If
        cKodeBunga = aCfg(eCfg.msKodeBunga).ToString
        cKodePajak = aCfg(eCfg.msKodePajakBunga).ToString

        mc.SQL = "select kode, max(sukubunga) as sukubunga from detailsukubunga group by kode having sukubunga > 0 "
        db = mc.Browse
        If Not IsNothing(db) Then
          vaSukuBunga = db.Copy
        Else
          vaSukuBunga.Reset()
        End If

        For n = 0 To dbKas.Rows.Count - 1
          Dim cKantor As String = GetNull(dbKas.Rows(n).Item("Kode")).ToString
          cSQL = "select rekening, golongantabungan, tglpenutupan, kode, close from tabungan "
          cSQL = String.Format("{0}where tglpenutupan >= '{1}' and Left(rekening, 2) = '{2}' ", cSQL, formatValue(_dTglAwalBunga, formatType.yyyy_MM_dd), cKantor)
          cSQL = cSQL & " and rekening = '01.11.003201'"
          cSQL = cSQL & "order by rekening "
          mc.SQL = cSQL
          dbData = mc.Browse
          If Not IsNothing(dbData) Then
            Dim dTanggalPenutupan As Date = CDate(formatValue(dbData.Rows(0).Item("TglPenutupan"), formatType.dd_MM_yyyy))
            Dim cClose As String = dbData.Rows(0).Item("Close").ToString
            Dim lHitungBunga As Boolean = True
            If dTanggalPenutupan > _dTglPosting And cClose = "1" Then
              lHitungBunga = True
            Else
              If cClose = "1" Then
                lHitungBunga = False
              Else
                lHitungBunga = True
              End If
            End If
            Dim nMax As Integer = dbData.Rows.Count - 1
            Dim nResult As Integer = 0
            Dim cReportStatus As String = ""
            Dim cReportStatusNasabah As String
            worker.ReportProgress(nResult, "Preparing...")
            For m = 0 To nMax
              nResult = CInt(Math.Round(m + 1) / (nMax + 1) * 100)
              If worker.CancellationPending Then
                worker.ReportProgress(nResult, "Cancelling...")
                Exit For
              End If
              cReportStatusNasabah = "Nasabah Kantor : " & cKantor & " - " & m + 1 & " of " & dbData.Rows.Count
              cReportStatus = HitungDurasi(cWaktu) & "~" & cReportStatusNasabah
              worker.ReportProgress(nResult, cReportStatus)
              With dbData.Rows(m)
                If lHitungBunga Then
                  nBaris = vaGolonganTabungan.Select(String.Format("kode = {0}", .Item("GolonganTabungan")))
                  If nBaris.Count > 0 Then
                    Dim nPersen As Single = CSng(Math.Round((m + 1) / dbData.Rows.Count * 100))
                    Tray.Text = cReportStatusNasabah & " ( " & nPersen & " % )"
                    'Tray.ShowBalloonTip(10, "Posting Bunga Tabungan", Tray.Text, ToolTipIcon.Info)
                    cRekening = .Item("Rekening").ToString
                    cKodeTemp = .Item("Kode").ToString
                    cGolonganTabungan = .Item("GolonganTabungan").ToString
                    classTabungan.Database = cDatabase
                    classTabungan.IPNumber = cIP
                    classTabungan.Port = cPort
                    classTabungan.Rekening = cRekening
                    Dim dTanggalAwalBungaTemp As Date = _dTglAwalBunga
                    'dTanggalAwalBungaTemp = DateAdd(DateInterval.Day, 1, _dTglAwalBunga)
                    classTabungan.TanggalAwal = dTanggalAwalBungaTemp
                    classTabungan.TanggalAkhir = _dTglAkhirBunga
                    classTabungan.KodeRegister = cKodeTemp
                    classTabungan.GolonganTabungan = cGolonganTabungan
                    classTabungan.ProsesPostingBunga = False
                    vaArray = classTabungan.GetRptBungaHarian
                    nTotalBunga = classTabungan.TotalBunga
                    nTotalPajak = classTabungan.TotalPajak
                    nSaldoAkhir = classTabungan.SaldoAkhir

                    classAkuntansi.Database = cDatabase
                    classAkuntansi.IPNumber = cIP
                    classAkuntansi.Port = cPort


                    If nTotalBunga + nTotalPajak > 0 And cGolonganTabungan <> "17" Then
                      ' Update Bunga Tabungan
                      cKeteranganPosting = String.Format("Bunga Tgl {0} {1}", Format(_dTglPosting, "MM-yy"), cRekening)
                      classTabungan.Faktur = cFaktur
                      classTabungan.Tanggal = _dTglPosting
                      classTabungan.Cabang = cKodeCabang
                      classTabungan.KodeTransaksi = cKodeBunga
                      classTabungan.Mutasi = nTotalBunga
                      classTabungan.Keterangan = cKeteranganPosting
                      classTabungan.UpdateBukuBesar = False
                      classTabungan.DeleteExistingBukuBesar = False
                      classTabungan.UpdMutasiTabungan()

                      cSQL = "select id, username, keterangan, cabangentry, keterangan, datetime "
                      cSQL = cSQL & "from mutasitabungan "
                      cSQL = String.Format("{0}where rekening = '{1}' ", cSQL, cRekening)
                      cSQL = String.Format("{0}and tgl = '{1}' and kodetransaksi = '{2}'", cSQL, formatValue(_dTglPosting, formatType.yyyy_MM_dd), cKodeBunga)
                      mc.SQL = cSQL
                      db = mc.Browse
                      If Not IsNothing(db) Then
                        nBaris = vaGolonganTabungan.Select(String.Format("kode = {0}", cGolonganTabungan))
                        If nBaris.Count > 0 Then
                          classAkuntansi.Cabang = db.Rows(0).Item("CabangEntry").ToString
                          classAkuntansi.Faktur = cFaktur
                          classAkuntansi.Tanggal = _dTglPosting
                          classAkuntansi.Waktu = db.Rows(0).Item("DateTime").ToString
                          classAkuntansi.Username = db.Rows(0).Item("UserName").ToString
                          classAkuntansi.ID = CInt(db.Rows(0).Item("Id"))
                          classAkuntansi.Keterangan = db.Rows(0).Item("Keterangan").ToString

                          classAkuntansi.RekeningJurnal = nBaris(0).Item(1).ToString
                          classAkuntansi.Debet = nTotalBunga
                          classAkuntansi.UpdBukuBesar()

                          classAkuntansi.RekeningJurnal = nBaris(0).Item(2).ToString
                          classAkuntansi.Kredit = nTotalBunga
                          classAkuntansi.UpdBukuBesar()
                        End If
                      End If

                      ' Update Pajak Tabungan
                      If nTotalPajak > 0 And .Item("GolonganTabungan").ToString <> "17" Then
                        classTabungan.Rekening = .Item("Rekening").ToString
                        classTabungan.Faktur = cFaktur
                        classTabungan.Tanggal = _dTglPosting
                        classTabungan.Cabang = cKodeCabang
                        classTabungan.KodeTransaksi = cKodePajak
                        classTabungan.Mutasi = nTotalPajak
                        classTabungan.Keterangan = String.Format("Pajak Bunga Tgl {0} {1}", Format(_dTglPosting, "MM-yyyy"), .Item("Rekening").ToString)
                        classTabungan.UpdateBukuBesar = False
                        classTabungan.DeleteExistingBukuBesar = False
                        classTabungan.UpdMutasiTabungan()

                        cSQL = "select m.id, m.username, m.keterangan, m.cabangentry, m.keterangan, m.datetime, k.rekening as RekeningKodeTransaksi "
                        cSQL = cSQL & "from mutasitabungan m "
                        cSQL = cSQL & "left join kodetransaksi k on k.kode = m.kodetransaksi "
                        cSQL = String.Format("{0}where m.rekening = '{1}' ", cSQL, .Item("Rekening"))
                        cSQL = String.Format("{0}and m.tgl = '{1}' and m.kodetransaksi = '{2}' ", cSQL, formatValue(_dTglPosting, formatType.yyyy_MM_dd), cKodePajak)
                        mc.SQL = cSQL
                        db = mc.Browse
                        If Not IsNothing(db) Then
                          cRekeningJurnalPajak = GetNull(db.Rows(0).Item("RekeningKodeTransaksi"), "").ToString
                          nBaris = vaGolonganTabungan.Select(String.Format("kode = {0}", cGolonganTabungan))
                          If nBaris.Count > 0 Then
                            classAkuntansi.Cabang = db.Rows(0).Item("CabangEntry").ToString
                            classAkuntansi.Faktur = cFaktur
                            classAkuntansi.Tanggal = _dTglPosting
                            classAkuntansi.Keterangan = db.Rows(0).Item("Keterangan").ToString
                            classAkuntansi.Waktu = db.Rows(0).Item("DateTime").ToString
                            classAkuntansi.Username = db.Rows(0).Item("UserName").ToString
                            classAkuntansi.ID = CInt(db.Rows(0).Item("Id"))

                            classAkuntansi.RekeningJurnal = nBaris(0).Item(1).ToString
                            classAkuntansi.Debet = nTotalPajak
                            classAkuntansi.UpdBukuBesar()

                            classAkuntansi.RekeningJurnal = cRekeningJurnalPajak
                            classAkuntansi.Kredit = nTotalPajak
                            classAkuntansi.UpdBukuBesar()
                          End If
                        End If
                      End If
                    End If
                  End If
                End If
              End With
            Next
          End If
        Next
      End If
      If Not IsNothing(dbData) Then
        dbData.Dispose()
      End If
      If Not IsNothing(db) Then
        db.Dispose()
      End If
      If Not IsNothing(dbKas) Then
        dbKas.Dispose()
      End If
      vaArray.Dispose()
      vaSukuBunga.Dispose()
      Tray.Text = "Persetujuan Pengajuan Kredit"
    End If
  End Sub

  <System.Diagnostics.CodeAnalysis.SuppressMessage("CodeRush", "String.Format can be used")>
  Public Sub PostingBunga(Optional ByVal worker As System.ComponentModel.BackgroundWorker = Nothing,
                          Optional ByVal e As System.ComponentModel.DoWorkEventArgs = Nothing)
    Dim dbData As New DataTable
    Dim mc As New DataBaseConnection
    Dim classTabungan As New ClassTabungan
    Dim classAkuntansi As New ClassAkuntansi
    Dim nTotalBunga As Double
    Dim nTotalPajak As Double
    Dim n As Integer
    Dim cFaktur As String
    Dim cKodeCabang As String = aCfg(eCfg.msKodeCabang).ToString
    Dim cKodeBunga As String
    Dim cKodePajak As String
    Dim cF As String = String.Format("BT{0}{1}{2}", cKodeKantorInduk, Format(EOM(_dTglPosting), "yyMMdd"), cUserID)
    Dim vaGolonganTabungan As New DataTable
    Dim db As New DataTable
    Dim m As Integer
    Dim cRekeningJurnalPajak As String = ""
    Dim vaArray As New DataTable
    Dim cFTemp As String = String.Format("BT{0}{1}", cKodeKantorInduk, Format(EOM(_dTglPosting), "yyMMdd"))
    Dim vaSukuBunga As New DataTable
    Dim nSaldoAkhir As Double
    Dim dbKas As New DataTable
    Dim cRekening As String = ""
    Dim cKodeTemp As String = ""
    Dim cGolonganTabungan As String = ""
    Dim cKeteranganPosting As String = ""

    If worker.CancellationPending Then
      e.Cancel = True
    Else

      cFaktur = cF & Padl("1", 4, "0")
      Dim cWaktu As DateTime = Now

      Dim x As New ClassXmlFile() With {.JenisDatabase = ClassXmlFile.enJenisDatabase.local, .Field = "databasename"}
      Dim cDatabase As String = x.GetValue
      x.Field = "ip"
      Dim cIP As String = x.GetValue
      x.Field = "port"
      Dim cPort As String = x.GetValue
      x.Field = "kode_kantor"
      cKodeCabang = x.GetValue
      mc.Database = cDatabase
      mc.IP = cIP
      mc.Port = cPort
      mc.Username = cUsername
      mc.UpdateHistory = True
      mc.SQL = String.Format("delete from mutasitabungan where faktur like '{0}%' ", cFTemp)
      mc.Delete()
      mc.SQL = String.Format("delete from bukubesar where faktur like '{0}%' ", cFTemp)
      mc.Delete()
      If cKodeCabang = "00" Then
        mc.SQL = "select kode from cabang"
      Else
        mc.SQL = String.Format("select kode from cabang where induk = '{0}' ", cKodeCabang)
      End If
      dbKas = mc.Browse
      If Not IsNothing(dbKas) Then
        mc.SQL = "select kode, rekening, rekeningbunga from golongantabungan order by kode "
        db = mc.Browse
        If Not IsNothing(db) Then
          vaGolonganTabungan = db.Copy
        Else
          vaGolonganTabungan.Reset()
        End If
        cKodeBunga = aCfg(eCfg.msKodeBunga).ToString
        cKodePajak = aCfg(eCfg.msKodePajakBunga).ToString

        mc.SQL = "select kode, max(sukubunga) as sukubunga from detailsukubunga group by kode having sukubunga > 0 "
        db = mc.Browse
        If Not IsNothing(db) Then
          vaSukuBunga = db.Copy
        Else
          vaSukuBunga.Reset()
        End If

        For n = 0 To dbKas.Rows.Count - 1
          Dim cKantor As String = GetNull(dbKas.Rows(n).Item("Kode")).ToString
          cSQL = "select rekening, golongantabungan, tglpenutupan, kode, close from tabungan "
          cSQL = String.Format("{0}where tglpenutupan >= '{1}' and Left(rekening, 2) = '{2}' ", cSQL, formatValue(_dTglAwalBunga, formatType.yyyy_MM_dd), cKantor)
          'cSQL = cSQL & " and rekening = '01.11.003201'"
          'cSQL = cSQL & " and rekening ='01.11.002311' "
          cSQL = cSQL & "order by rekening "
          mc.SQL = cSQL
          dbData = mc.Browse
          If Not IsNothing(dbData) Then
            Dim nMax As Integer = dbData.Rows.Count - 1
            Dim nResult As Integer = 0
            Dim cReportStatus As String = ""
            Dim cReportStatusNasabah As String
            worker.ReportProgress(nResult, "Preparing...")
            For m = 0 To nMax
              nResult = CInt(Math.Round(m + 1) / (nMax + 1) * 100)
              If worker.CancellationPending Then
                worker.ReportProgress(nResult, "Cancelling...")
                Exit For
              End If
              cReportStatusNasabah = "Nasabah Kantor : " & cKantor & " - " & m + 1 & " of " & dbData.Rows.Count
              cReportStatus = HitungDurasi(cWaktu) & "~" & cReportStatusNasabah
              worker.ReportProgress(nResult, cReportStatus)
              With dbData.Rows(m)
                'If Debugger.IsAttached And .Item("Rekening").ToString = "01.11.002311" Then
                '  Stop
                'End If
                Dim dTanggalPenutupan As Date = CDate(formatValue(.Item("TglPenutupan"), formatType.dd_MM_yyyy))
                Dim cClose As String = .Item("Close").ToString
                Dim lHitungBunga As Boolean = True
                If dTanggalPenutupan > _dTglPosting And cClose = "1" Then
                  lHitungBunga = True
                Else
                  If cClose = "1" Then
                    lHitungBunga = False
                  Else
                    lHitungBunga = True
                  End If
                End If
                If lHitungBunga Then
                  nBaris = vaGolonganTabungan.Select(String.Format("kode = {0}", .Item("GolonganTabungan")))
                  If nBaris.Count > 0 Then
                    Dim nPersen As Single = CSng(Math.Round((m + 1) / dbData.Rows.Count * 100))
                    Tray.Text = cReportStatusNasabah & " ( " & nPersen & " % )"
                    'Tray.ShowBalloonTip(10, "Posting Bunga Tabungan", Tray.Text, ToolTipIcon.Info)
                    cRekening = .Item("Rekening").ToString
                    cKodeTemp = .Item("Kode").ToString
                    cGolonganTabungan = .Item("GolonganTabungan").ToString
                    classTabungan.Database = cDatabase
                    classTabungan.IPNumber = cIP
                    classTabungan.Port = cPort
                    classTabungan.Rekening = cRekening
                    Dim dTanggalAwalBungaTemp As Date = _dTglAwalBunga
                    'dTanggalAwalBungaTemp = DateAdd(DateInterval.Day, 1, _dTglAwalBunga)
                    classTabungan.TanggalAwal = dTanggalAwalBungaTemp
                    classTabungan.TanggalAkhir = _dTglAkhirBunga
                    classTabungan.KodeRegister = cKodeTemp
                    classTabungan.GolonganTabungan = cGolonganTabungan
                    classTabungan.ProsesPostingBunga = False
                    vaArray = classTabungan.GetRptBungaHarian
                    nTotalBunga = classTabungan.TotalBunga
                    nTotalPajak = classTabungan.TotalPajak
                    nSaldoAkhir = classTabungan.SaldoAkhir

                    classAkuntansi.Database = cDatabase
                    classAkuntansi.IPNumber = cIP
                    classAkuntansi.Port = cPort


                    If nTotalBunga + nTotalPajak > 0 And cGolonganTabungan <> "17" Then
                      ' Update Bunga Tabungan
                      cKeteranganPosting = String.Format("Bunga Tgl {0} {1}", Format(_dTglPosting, "MM-yy"), cRekening)
                      classTabungan.Faktur = cFaktur
                      classTabungan.Tanggal = _dTglPosting
                      classTabungan.Cabang = cKodeCabang
                      classTabungan.KodeTransaksi = cKodeBunga
                      classTabungan.Mutasi = nTotalBunga
                      classTabungan.Keterangan = cKeteranganPosting
                      classTabungan.UpdateBukuBesar = False
                      classTabungan.DeleteExistingBukuBesar = False
                      classTabungan.UpdMutasiTabungan()

                      Dim nIDBunga As Double
                      Dim nIDPajak As Double
                      Dim cWaktuBunga As String = formatValue(Now, formatType.yyyy_MM_dd_HH_mm_ss)
                      Dim cWaktuPajak As String = formatValue(Now, formatType.yyyy_MM_dd_HH_mm_ss)
                      Dim cUserNameMutasi As String = cUsername
                      Dim lUpdateBunga As Boolean = False
                      Dim lUpdatePajak As Boolean = False
                      cSQL = "select ifnull(m.id,0) as idBunga, m.datetime as waktuBunga, ifnull(m1.id,0) as idPajak, "
                      cSQL = cSQL & "m1.datetime as waktuPajak, m.username, k.rekening as rekeningKodeTransaksi "
                      cSQL = cSQL & "from mutasitabungan m "
                      cSQL = cSQL & "left join mutasitabungan m1 on m1.rekening = m.rekening and m1.kodetransaksi = '" & cKodePajak & "' and m1.tgl = m.tgl "
                      cSQL = cSQL & "left join kodetransaksi k on k.kode = m1.kodetransaksi "
                      cSQL = cSQL & "where m.rekening = '" & cRekening & "' and m.kodetransaksi = '" & cKodeBunga & "' "
                      cSQL = cSQL & "and m.tgl = '" & formatValue(_dTglPosting, formatType.yyyy_MM_dd) & "' "
                      mc.SQL = cSQL
                      db = mc.Browse
                      If Not IsNothing(db) Then
                        nIDBunga = CDbl(db.Rows(0).Item("idBunga"))
                        nIDPajak = CDbl(db.Rows(0).Item("idPajak"))
                        cWaktuBunga = db.Rows(0).Item("waktuBunga").ToString
                        cWaktuPajak = db.Rows(0).Item("waktuPajak").ToString
                        cUserNameMutasi = db.Rows(0).Item("username").ToString
                        cRekeningJurnalPajak = db.Rows(0).Item("rekeningKodeTransaksi").ToString
                        lUpdateBunga = nIDBunga <> 0
                        lUpdatePajak = nIDPajak <> 0
                      End If

                      If lUpdateBunga Then
                        nBaris = vaGolonganTabungan.Select(String.Format("kode = {0}", cGolonganTabungan))
                        If nBaris.Count > 0 Then
                          classAkuntansi.Cabang = cKodeCabang
                          classAkuntansi.Faktur = cFaktur
                          classAkuntansi.Tanggal = _dTglPosting
                          classAkuntansi.Waktu = cWaktuBunga
                          classAkuntansi.Username = cUserNameMutasi
                          classAkuntansi.ID = CInt(nIDBunga)
                          classAkuntansi.Keterangan = cKeteranganPosting

                          classAkuntansi.RekeningJurnal = nBaris(0).Item(1).ToString
                          classAkuntansi.Debet = nTotalBunga
                          classAkuntansi.UpdBukuBesar()

                          classAkuntansi.RekeningJurnal = nBaris(0).Item(2).ToString
                          classAkuntansi.Kredit = nTotalBunga
                          classAkuntansi.UpdBukuBesar()
                        End If
                      End If

                      ' Update Pajak Tabungan
                      If nTotalPajak > 0 And .Item("GolonganTabungan").ToString <> "17" Then
                        classTabungan.Rekening = .Item("Rekening").ToString
                        classTabungan.Faktur = cFaktur
                        classTabungan.Tanggal = _dTglPosting
                        classTabungan.Cabang = cKodeCabang
                        classTabungan.KodeTransaksi = cKodePajak
                        classTabungan.Mutasi = nTotalPajak
                        classTabungan.Keterangan = String.Format("Pajak Bunga Tgl {0} {1}", Format(_dTglPosting, "MM-yyyy"), .Item("Rekening").ToString)
                        classTabungan.UpdateBukuBesar = False
                        classTabungan.DeleteExistingBukuBesar = False
                        classTabungan.UpdMutasiTabungan()

                        If lUpdatePajak Then
                          nBaris = vaGolonganTabungan.Select(String.Format("kode = {0}", cGolonganTabungan))
                          If nBaris.Count > 0 Then
                            classAkuntansi.Cabang = cKodeCabang
                            classAkuntansi.Faktur = cFaktur
                            classAkuntansi.Tanggal = _dTglPosting
                            classAkuntansi.Keterangan = cKeteranganPosting
                            classAkuntansi.Waktu = cWaktuPajak
                            classAkuntansi.Username = cUserNameMutasi
                            classAkuntansi.ID = nIDPajak

                            classAkuntansi.RekeningJurnal = nBaris(0).Item(1).ToString
                            classAkuntansi.Debet = nTotalPajak
                            classAkuntansi.UpdBukuBesar()

                            classAkuntansi.RekeningJurnal = cRekeningJurnalPajak
                            classAkuntansi.Kredit = nTotalPajak
                            classAkuntansi.UpdBukuBesar()
                          End If
                        End If
                      End If
                    End If
                  End If
                End If
              End With
            Next
          End If
        Next
      End If
      If Not IsNothing(dbData) Then
        dbData.Dispose()
      End If
      If Not IsNothing(db) Then
        db.Dispose()
      End If
      If Not IsNothing(dbKas) Then
        dbKas.Dispose()
      End If
      vaArray.Dispose()
      vaSukuBunga.Dispose()
      Tray.Text = "Persetujuan Pengajuan Kredit"
    End If
  End Sub

  <System.Diagnostics.CodeAnalysis.SuppressMessage("CodeRush", "String.Format can be used")>
  Public Sub PostingAdm(Optional ByVal worker As System.ComponentModel.BackgroundWorker = Nothing,
                               Optional ByVal e As System.ComponentModel.DoWorkEventArgs = Nothing)
    Dim dbData As New DataTable
    Dim mc As New DataBaseConnection
    Dim classHistory As New ClassHistory
    Dim classTabungan As New ClassTabungan
    Dim classAkuntansi As New ClassAkuntansi
    Dim lUpdateBukuBesar As Boolean
    Dim n As Integer
    Dim cFaktur As String
    Dim nSaldoAkhir As Double
    Dim cF As String = String.Format("TA{0}{1}{2}", cKodeKantorInduk, Format(EOM(_dTglPosting), "yyMMdd"), cUserID)
    Dim nAdminPasif As Double
    Dim lAdminPasif As Boolean
    Dim nSaldoMinimum As Double
    Dim cKodeCabang As String = aCfg(eCfg.msKodeCabang).ToString
    Dim vaGolonganTabungan As New DataTable
    Dim db As New DataTable
    Dim i As Integer
    Dim cFTemp As String = String.Format("TA{0}{1}", cKodeKantorInduk, Format(EOM(_dTglPosting), "yyMMdd"))
    Dim dbKas As New DataTable
    Dim cKodeTransaksi As String = ""
    Dim cRekeningJurnal As String = ""
    Dim cFakturTemp As String = ""
    Dim a As Integer = 0

    If worker.CancellationPending Then
      e.Cancel = True
    Else

      cFaktur = cF & Padl("1", 4, "0")

      Dim x As New ClassXmlFile() With {.JenisDatabase = ClassXmlFile.enJenisDatabase.local, .Field = "databasename"}
      Dim cDatabase As String = x.GetValue
      x.Field = "ip"
      Dim cIP As String = x.GetValue
      x.Field = "port"
      Dim cPort As String = x.GetValue
      x.Field = "kode_kantor"
      cKodeCabang = x.GetValue
      mc.Database = cDatabase
      mc.IP = cIP
      mc.Port = cPort
      mc.SQL = String.Format("select rekening from mutasitabungan where keterangan like 'Tutup %' and tgl='{0}' and faktur like '{1}%'", formatValue(_dTglPosting, formatType.yyyy_MM_dd), cFTemp)
      dbData = mc.Browse
      If Not IsNothing(dbData) Then
        For n = 0 To dbData.Rows.Count - 1
          cSQL = "update tabungan set close = @close, tglpenutupan = @tglpenutupan "
          cSQL = cSQL & "where rekening = @rekening "
          mc.InitConnection()
          Dim cmd As MySqlCommand = New MySqlCommand(cSQL, mc.OpenConnection)
          With cmd
            .Parameters.AddWithValue("@close", "0")
            .Parameters.AddWithValue("@tglpenutupan", CDate("9999-12-31"))
            .Parameters.AddWithValue("@rekening", dbData.Rows(n).Item("Rekening").ToString)
            .ExecuteNonQuery()
            classHistory.SQL = cSQL
            classHistory.ObjCmd = cmd
            .Dispose()
          End With
          mc.CloseConnection()
          mc.Dispose()
          classHistory.Database = cDatabase
          classHistory.IPNumber = cIP
          classHistory.Port = cPort
          classHistory.Username = cUsername
          classHistory.UpdateHistory()
        Next
      End If

      Dim cWaktu As DateTime = Now
      mc.UpdateHistory = True
      mc.SQL = String.Format("delete from mutasitabungan where faktur like '{0}%' ", cFTemp)
      mc.Delete()
      mc.SQL = String.Format("delete from bukubesar where faktur like '{0}%' ", cFTemp)
      mc.Delete()
      If cKodeCabang = "00" Then
        mc.SQL = "select kode from cabang"
      Else
        mc.SQL = String.Format("select kode from cabang where induk = '{0}' ", cKodeCabang)
      End If
      dbKas = mc.Browse
      If Not IsNothing(dbKas) Then
        For n = 0 To dbKas.Rows.Count - 1
          Dim cKantor As String = GetNull(dbKas.Rows(n).Item("Kode")).ToString
          cSQL = "select t.Rekening, g.AdministrasiBulanan, g.AdminPasif, r.nama, g.lamapasif, t.golongantabungan, "
          cSQL = cSQL & "g.saldominimum, g.administrasitutup "
          cSQL = cSQL & "from tabungan t "
          cSQL = cSQL & "left join golongantabungan g on t.golongantabungan = g.kode "
          cSQL = cSQL & "left join registernasabah r on r.kode= t.kode "
          cSQL = cSQL & "where t.golongantabungan in ('11','18') "
          If Debugger.IsAttached Then
            'cSQL = cSQL & " and t.rekening = '01.11.003201'"
          End If
          cSQL = String.Format("{0}and t.tglpenutupan >= '{1}' ", cSQL, formatValue(_dTglAkhirBunga, formatType.yyyy_MM_dd))
          cSQL = String.Format("{0}and Left(t.rekening,2) = '{1}'", cSQL, cKantor)
          mc.SQL = cSQL
          dbData = mc.Browse
          If Not IsNothing(dbData) Then
            mc.SQL = "select kode, rekening, rekeningbunga from golongantabungan order by kode "
            db = mc.Browse
            If Not IsNothing(db) Then
              vaGolonganTabungan = db.Copy
            Else
              vaGolonganTabungan.Reset()
            End If
            cKodeTransaksi = aCfg(eCfg.msKodeAdmPemeliharaan).ToString
            lUpdateBukuBesar = False
            Dim nMax As Integer = dbData.Rows.Count - 1
            Dim nResult As Integer = 0
            Dim cReportStatus As String = "Preparing..."
            Dim cReportStatusNasabah As String = ""
            worker.ReportProgress(nResult, cReportStatus)
            For i = 0 To nMax
              nResult = CInt(Math.Round(i + 1) / (nMax + 1) * 100)
              If worker.CancellationPending Then
                worker.ReportProgress(nResult, "Cancelling...")
                Exit For
              End If
              cReportStatusNasabah = "Nasabah Kantor : " & cKantor & " - " & i + 1 & " of " & dbData.Rows.Count
              cReportStatus = HitungDurasi(cWaktu) & "~" & cReportStatusNasabah
              worker.ReportProgress(nResult, cReportStatus)
              Dim nPersen As Single = CSng(Math.Round((i + 1) / dbData.Rows.Count * 100))
              Tray.Text = cReportStatusNasabah & " ( " & nPersen & " % )"
              'Tray.ShowBalloonTip(1000, "Posting Administrasi Bunga Tabungan", Tray.Text, ToolTipIcon.Info)
              classTabungan.Database = cDatabase
              classTabungan.IPNumber = cIP
              classTabungan.Port = cPort
              classTabungan.Rekening = dbData.Rows(i)("Rekening").ToString
              classTabungan.Tanggal = _dTglPosting
              lAdminPasif = classTabungan.GetRekeningPassif
              nSaldoAkhir = classTabungan.GetSaldoTabungan
              nSaldoMinimum = CDbl(GetNull(dbData.Rows(i).Item("AdministrasiTutup")))
              nAdminPasif = CDbl(GetNull(dbData.Rows(i).Item("AdministrasiBulanan")))
              ' Update Administrasi Tabungan
              Dim cGolTabungan As String = dbData.Rows(i).Item("GolonganTabungan").ToString
              If nSaldoAkhir > nSaldoMinimum And (cGolTabungan = "11" Or cGolTabungan = "18") Then
                If nAdminPasif > 0 Then
                  classTabungan.Faktur = cFaktur
                  classTabungan.Cabang = cKodeCabang
                  classTabungan.KodeTransaksi = cKodeTransaksi
                  classTabungan.Mutasi = nAdminPasif
                  classTabungan.Keterangan = String.Format("Biaya Adm. {0} {1}", Format(_dTglPosting, "MM-yyyy"), dbData.Rows(i)("Rekening"))
                  classTabungan.UpdateBukuBesar = False
                  classTabungan.DeleteExistingBukuBesar = False
                  classTabungan.UpdMutasiTabungan()

                  cSQL = "select m.id, m.username, m.keterangan, m.cabangentry, m.keterangan, m.datetime, k.rekening as RekeningKodeTransaksi "
                  cSQL = cSQL & "from mutasitabungan m "
                  cSQL = cSQL & "left join kodetransaksi k on k.kode = m.kodetransaksi "
                  cSQL = String.Format("{0}where m.rekening = '{1}' ", cSQL, dbData.Rows(i)("Rekening"))
                  cSQL = String.Format("{0}and m.tgl = '{1}' and m.kodetransaksi = '{2}'", cSQL, formatValue(_dTglPosting, formatType.yyyy_MM_dd), cKodeTransaksi)
                  mc.SQL = cSQL
                  db = mc.Browse
                  If Not IsNothing(db) Then
                    cRekeningJurnal = GetNull(db.Rows(0).Item("RekeningKodeTransaksi"), "").ToString
                    nBaris = vaGolonganTabungan.Select(String.Format("kode = {0}", dbData.Rows(0).Item("GolonganTabungan")))
                    If nBaris.Count > 0 Then
                      classAkuntansi.Database = cDatabase
                      classAkuntansi.Port = cPort
                      classAkuntansi.IPNumber = cIP
                      classAkuntansi.Cabang = db.Rows(0).Item("CabangEntry").ToString
                      classAkuntansi.Faktur = cFaktur
                      classAkuntansi.Tanggal = _dTglPosting
                      classAkuntansi.Keterangan = db.Rows(0).Item("Keterangan").ToString
                      classAkuntansi.Waktu = db.Rows(0).Item("DateTime").ToString
                      classAkuntansi.Username = db.Rows(0).Item("UserName").ToString
                      classAkuntansi.ID = CInt(db.Rows(0).Item("Id"))

                      classAkuntansi.RekeningJurnal = nBaris(0).Item(1).ToString
                      classAkuntansi.Debet = nAdminPasif
                      classAkuntansi.UpdBukuBesar()

                      classAkuntansi.RekeningJurnal = cRekeningJurnal
                      classAkuntansi.Kredit = nAdminPasif
                      classAkuntansi.UpdBukuBesar()
                    End If
                  End If
                End If
              ElseIf nSaldoAkhir > 0 Then
                If nSaldoAkhir < nSaldoMinimum Then
                  a += 1
                  ' Edit Tabungan
                  cSQL = "update tabungan set close = @close, tglpenutupan = @tglpenutupan "
                  cSQL = String.Format("{0}where rekening = '{1}' ", cSQL, dbData.Rows(i)("Rekening"))
                  mc.InitConnection()
                  Dim cmd As MySqlCommand = New MySqlCommand(cSQL, mc.OpenConnection)
                  With cmd
                    .Parameters.AddWithValue("@close", 1)
                    .Parameters.AddWithValue("@tglpenutupan", formatValue(_dTglPosting, formatType.yyyy_MM_dd))
                    .ExecuteNonQuery()
                    classHistory.SQL = cSQL
                    classHistory.ObjCmd = cmd
                    .Dispose()
                  End With
                  mc.CloseConnection()
                  mc.Dispose()
                  classHistory.Database = cDatabase
                  classHistory.IPNumber = cIP
                  classHistory.Port = cPort
                  classHistory.Username = cUsername
                  classHistory.UpdateHistory()

                  ' Tutup tabungan ( saldo ditarik tunai )
                  cFakturTemp = cF & Padl(a.ToString, 4, "0")
                  classTabungan.Faktur = cFakturTemp
                  classTabungan.Cabang = cKodeCabang
                  classTabungan.KodeTransaksi = aCfg(eCfg.msKodePenarikanTunai).ToString
                  classTabungan.Mutasi = nSaldoAkhir
                  classTabungan.Keterangan = "Tutup Rekening an. " & dbData.Rows(i)("Nama").ToString
                  classTabungan.UpdMutasiTabungan()
                End If
              End If
              'Console.WriteLine(Tray.Text)
            Next
          End If
        Next
      End If
      Tray.Text = "Persetujuan Pengajuan Kredit"
      vaGolonganTabungan.Dispose()
    End If
  End Sub
End Class
