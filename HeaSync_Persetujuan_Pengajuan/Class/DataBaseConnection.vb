﻿
Public Class DataBaseConnection
  Implements IDisposable
  Dim cConectionString As String = GetDSN()
  'Dim cConectionString As String = "Database=bpr_private;Data Source=localhost;User Id=Heasoft;Password=HeaI"
  'Private conn As New MySqlConnection(My.Settings.MySQLConnectionString)
  'ReadOnly conn As New MySqlConnection("Database=bpr_private;Data Source=localhost;User Id=Heasoft;Password=HeaI")
  ReadOnly conn As New MySqlConnection(cConectionString)
  Private _cSQL As String = ""

  Private _cDatabase As String = ""
  Private _cIP As String = ""
  Private _cPort As String = ""
  Private _lUpdateHistory As Boolean = True
  Private _cUsername As String = ""

  Public Property Username As String
    Get
      Return _cUsername
    End Get
    Set(value As String)
      _cUsername = value
    End Set
  End Property

  Public Property UpdateHistory As Boolean
    Get
      Return _lUpdateHistory
    End Get
    Set(value As Boolean)
      _lUpdateHistory = value
    End Set
  End Property

  Public Property Port As String
    Get
      Return _cPort
    End Get
    Set(value As String)
      _cPort = value
    End Set
  End Property

  Public Property IP As String
    Get
      Return _cIP
    End Get
    Set(value As String)
      _cIP = value
    End Set
  End Property

  Public Property Database As String
    Get
      Return _cDatabase
    End Get
    Set(value As String)
      _cDatabase = value
    End Set
  End Property

  Function GetDSN() As String
    GetDSN = String.Format("Database={0}; Convert Zero Datetime=True; Allow Zero Datetime=True; Data Source={1};User Id=Heasoft;Password=HeaI;Port={2}", _cDatabase, _cIP, _cPort)
  End Function

  Public Property SQL As String
    Get
      Return _cSQL
    End Get
    Set(value As String)
      _cSQL = value
    End Set
  End Property

  Public Sub Dispose() Implements IDisposable.Dispose
    If conn IsNot Nothing Then
      conn.Dispose()
    End If
  End Sub

  Public Sub InitConnection()
    If conn.State <> ConnectionState.Open Then
      conn.ConnectionString = GetDSN() 'cConnection
    End If
  End Sub

  Public Function OpenConnection() As MySqlConnection
    Try
      If conn.State <> ConnectionState.Open Then
        conn.Open()
      End If
    Catch ex As MySqlException ' Exception
      Dim cKeterangan As String = String.Format("{0} - {1}", ex.Number, ex.Message)
      If ex.Number = 1046 Then
        cKeterangan = String.Format("{0} - {1}.{2}Service will be terminated.", ex.Number, ex.Message, vbCrLf)
        MessageBox.Show(cKeterangan, "Error Database Connection", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End
      End If
      MessageBox.Show(cKeterangan, "Error Database Connection", MessageBoxButtons.OK, MessageBoxIcon.Error)
      'Catch ex As MySqlException
      '  MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Try
    Return conn
  End Function

  Public Function CloseConnection() As MySqlConnection
    conn.Close()
    Return conn
  End Function

  Sub Delete()
    Dim classHistory As New ClassHistory
    InitConnection()
    Try
      Dim cmd As MySqlCommand = New MySqlCommand(_cSQL, OpenConnection)
      cmd.ExecuteNonQuery()
      cmd.Dispose()
      CloseConnection()
      Dispose()
      If _lUpdateHistory Then
        classHistory.Database = _cDatabase
        classHistory.IPNumber = _cIP
        classHistory.Port = _cPort
        classHistory.Username = _cUsername
        classHistory.SQL = _cSQL
        classHistory.UpdateHistory()
      End If
    Catch ex As MySqlException
      Dim cKeterangan As String = String.Format("{0} - {1}", ex.Number, ex.Message)
      If ex.Number = 1046 Then
        cKeterangan = String.Format("{0} - {1}.{2}Service will be terminated.", ex.Number, ex.Message, vbCrLf)
        MessageBox.Show(cKeterangan, "Error Database Connection", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End
      End If
      MessageBox.Show(cKeterangan, "Error Database Connection", MessageBoxButtons.OK, MessageBoxIcon.Error)
    End Try

  End Sub

  Function Browse() As DataTable
    Try
      InitConnection()
      Dim cmd As MySqlCommand = New MySqlCommand(_cSQL, OpenConnection) With {.CommandType = CommandType.Text, .CommandTimeout = 100}
      Dim dataReader As MySqlDataReader = cmd.ExecuteReader
      Dim table As New DataTable
      If dataReader.HasRows Then
        Dim dataAdapter As MySqlDataAdapter = New MySqlDataAdapter(cmd)
        If Not dataReader.IsClosed Then
          dataReader.Close()
        End If
        dataAdapter.Fill(table)
        dataAdapter.Dispose()
      Else
        table = Nothing
      End If
      cmd.Dispose()
      dataReader.Close()
      dataReader.Dispose()
      CloseConnection()
      Dispose()
      Return table
    Catch ex As MySqlException ' Exception
      Dim cKeterangan As String = String.Format("{0} - {1}", ex.Number, ex.Message)
      If ex.Number = 1046 Then
        cKeterangan = String.Format("{0} - {1}.{2}Service will be terminated.", ex.Number, ex.Message, vbCrLf)
        MessageBox.Show(cKeterangan, "Error Database Connection", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End
      End If
      MessageBox.Show(cKeterangan, "Error Database Connection", MessageBoxButtons.OK, MessageBoxIcon.Error)
      Return Nothing
    End Try
  End Function

  Sub Query()
    Try
      InitConnection()
      Dim cmd As MySqlCommand = New MySqlCommand(_cSQL, OpenConnection) With {.CommandTimeout = 100}
      cmd.ExecuteNonQuery()
      cmd.Dispose()
      CloseConnection()
      Dispose()
    Catch ex As MySqlException ' Exception
      Dim cKeterangan As String = String.Format("{0} - {1}", ex.Number, ex.Message)
      If ex.Number = 1046 Then
        cKeterangan = String.Format("{0} - {1}.{2}Service will be terminated.", ex.Number, ex.Message, vbCrLf)
        MessageBox.Show(cKeterangan, "Error Database Connection", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End
      End If
      MessageBox.Show(cKeterangan, "Error Database Connection", MessageBoxButtons.OK, MessageBoxIcon.Error)
      'MessageBox.Show(ex.Message)
    End Try
  End Sub
End Class
