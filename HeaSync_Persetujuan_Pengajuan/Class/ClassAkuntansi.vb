﻿Imports System.Text.RegularExpressions

Public Class ClassAkuntansi
  Public Enum eTrigger
    msTabungan = 1
    msRealisasiKredit = 2
    msAngsuranKredit = 3
    msTitipanAngsuran = 4
    msKasKeluar = 5
    msKasMasuk = 6
    msDeposito = 7
    msRekeningRRP = 8
    msJurnalLain = 9
    msPenyusutanAktiva = 10
    msAmortisasiProvisi = 11
    msPenambahanPlafond = 12
    msSaldoAwalRekening = 13
    msCadanganBungaTabungan = 14
    msCadanganBungaDeposito = 15
    msAkhirTahun = 16
    msAccualKredit = 17
    msAccrualDeposito = 18
    msJurnalLainEtab = 19
    msAdmProvisiPerpanjanganKredit = 20
  End Enum

  Public Enum eDeposito
    trPembukaan = 1
    trPencairanPokok = 2
    trPencairanBunga = 3
    trPinalti = 4
    trKoreksiBunga = 5
  End Enum

  Private _cCabangEntry As String = aCfg(eCfg.msKodeCabang).ToString
  Private _cFaktur As String = ""
  Private _dTgl As Date = Date.Today
  Private _cRekeningJurnal As String = ""
  Private _cKeterangan As String = ""
  Private _nDebet As Double = 0
  Private _nKredit As Double = 0
  Private _nTotalDebet As Double = 0
  Private _nTotalKredit As Double = 0
  Private _cDateTime As String = Now.ToString
  Private _cUser As String = "system"
  Private _nID As Double = 0
  Private _cStatusJurnal As String = "0"
  Private _cDatabase As String = ""
  Private _cIPNumber As String = ""
  Private _cPort As String = ""
  Private _lUpdateHistory As Boolean = True

  Public Property UpdateHistory As Boolean
    Get
      Return _lUpdateHistory
    End Get
    Set(value As Boolean)
      _lUpdateHistory = value
    End Set
  End Property

  Public Property Port As String
    Get
      Return _cPort
    End Get
    Set(value As String)
      _cPort = value
    End Set
  End Property

  Public Property IPNumber As String
    Get
      Return _cIPNumber
    End Get
    Set(value As String)
      _cIPNumber = value
    End Set
  End Property

  Public Property Database As String
    Get
      Return _cDatabase
    End Get
    Set(value As String)
      _cDatabase = value
    End Set
  End Property

  Public Property StatusJurnal As String
    Get
      Return _cStatusJurnal
    End Get
    Set(value As String)
      _cStatusJurnal = value
    End Set
  End Property
  Public Property ID As Double
    Get
      Return _nID
    End Get
    Set(value As Double)
      _nID = value
    End Set
  End Property

  Public Property Username As String
    Get
      Return _cUser
    End Get
    Set(value As String)
      _cUser = value
    End Set
  End Property

  Public Property Waktu As String
    Get
      Return _cDateTime
    End Get
    Set(value As String)
      _cDateTime = value
    End Set
  End Property

  Public Property TotalKredit As Double
    Get
      Return _nTotalKredit
    End Get
    Set(value As Double)
      _nTotalKredit = value
    End Set
  End Property

  Public Property TotalDebet As Double
    Get
      Return _nTotalDebet
    End Get
    Set(value As Double)
      _nTotalDebet = value
    End Set
  End Property

  Public Property Kredit As Double
    Get
      Return _nKredit
    End Get
    Set(value As Double)
      _nKredit = value
    End Set
  End Property

  Public Property Debet As Double
    Get
      Return _nDebet
    End Get
    Set(value As Double)
      _nDebet = value
    End Set
  End Property

  Public Property Keterangan As String
    Get
      Return _cKeterangan
    End Get
    Set(value As String)
      _cKeterangan = value
    End Set
  End Property

  Public Property RekeningJurnal As String
    Get
      Return _cRekeningJurnal
    End Get
    Set(value As String)
      _cRekeningJurnal = value
    End Set
  End Property

  Public Property Tanggal As Date
    Get
      Return _dTgl
    End Get
    Set(value As Date)
      _dTgl = value
    End Set
  End Property

  Public Property Faktur As String
    Get
      Return _cFaktur
    End Get
    Set(value As String)
      _cFaktur = value
    End Set
  End Property

  Public Property Cabang As String
    Get
      Return _cCabangEntry
    End Get
    Set(value As String)
      _cCabangEntry = value
    End Set
  End Property

  Sub UpdBukuBesar()
    Dim myConnection As New DataBaseConnection()
    Dim classHistory As New ClassHistory
    If _dTgl >= DateSerial(2013, 8, 1) Then
      If _cRekeningJurnal <> "" Then
        If _nDebet <> 0 Or _nKredit <> 0 Then
          myConnection.Database = _cDatabase
          myConnection.IP = _cIPNumber
          myConnection.Port = _cPort
          myConnection.InitConnection()
          Dim SQLstring As String = "insert into bukubesar (urut, faktur, cabangentry, tgl, rekening, keterangan, debet, kredit, username, datetime) "
          SQLstring = SQLstring & "values (@urut, @faktur, @cabangentry, @tgl, @rekening, @keterangan, @debet, @kredit, @username, @datetime)"
          Dim cmd As MySqlCommand = New MySqlCommand(SQLstring, myConnection.OpenConnection)
          With cmd.Parameters
            .AddWithValue("@urut", _nID)
            .AddWithValue("@faktur", _cFaktur)
            .AddWithValue("@cabangentry", _cCabangEntry)
            .AddWithValue("@tgl", formatValue(_dTgl, formatType.yyyy_MM_dd))
            .AddWithValue("@rekening", _cRekeningJurnal)
            .AddWithValue("@keterangan", _cKeterangan)
            .AddWithValue("@debet", _nDebet)
            .AddWithValue("@kredit", _nKredit)
            .AddWithValue("@username", _cUser)
            .AddWithValue("@datetime", formatValue(_cDateTime, formatType.yyyy_MM_dd_HH_mm_ss))
          End With
          cmd.ExecuteNonQuery()
          classHistory.SQL = SQLstring
          classHistory.ObjCmd = cmd
          cmd.Dispose()
          myConnection.CloseConnection()
          myConnection.Dispose()
          If _lUpdateHistory Then
            classHistory.Database = _cDatabase
            classHistory.IPNumber = _cIPNumber
            classHistory.Port = _cPort
            classHistory.Username = _cUser
            classHistory.UpdateHistory()
          End If
        End If
      End If
    End If
  End Sub

  Sub DelBukuBesar()
    Dim myConnection As New DataBaseConnection()
    If _cFaktur <> "" Then
      myConnection.Database = _cDatabase
      myConnection.IP = _cIPNumber
      myConnection.Port = _cPort
      myConnection.SQL = String.Format("delete from bukubesar where faktur = '{0}' ", _cFaktur)
      myConnection.Delete()
    End If
  End Sub

  Sub UpdRekJurnal()
    Dim myConnection As New DataBaseConnection()
    Dim dbData As New DataTable
    ' Hapus Buku Besar
    DelBukuBesar()

    myConnection.Database = _cDatabase
    myConnection.IP = _cIPNumber
    myConnection.Port = _cPort
    ' Ambil Data Jurnal
    Dim cSQL As String = "select j.cabangentry, j.tgl, j.rekening, j.keterangan, j.debet, j.kredit, t.username, j.id from jurnal j "
    cSQL = cSQL & "left join totjurnal t on j.faktur = t.faktur "
    cSQL = String.Format("{0}where j.faktur = '{1}' ", cSQL, _cFaktur)
    myConnection.SQL = cSQL
    dbData = myConnection.Browse
    If Not IsNothing(dbData) Then
      For n As Integer = 0 To dbData.Rows.Count - 1
        With dbData.Rows(n)
          _cCabangEntry = .Item("CabangEntry").ToString
          _dTgl = CDate(.Item("Tgl").ToString)
          _cRekeningJurnal = .Item("Rekening").ToString
          _cKeterangan = .Item("Keterangan").ToString
          _nDebet = CDbl(.Item("Debet"))
          _nKredit = CDbl(.Item("Kredit"))
          _cUser = GetNull(.Item("UserName")).ToString
          _nID = CDbl(.Item("ID"))
          UpdBukuBesar()
        End With
      Next
    End If
    dbData.Dispose()
  End Sub

  Sub UpdTotJurnalLainLain()
    Dim myConnection As New DataBaseConnection()
    Dim classHistory As New ClassHistory
    Dim cSQL As String = ""

    myConnection.Database = _cDatabase
    myConnection.IP = _cIPNumber
    myConnection.Port = _cPort
    'hapus data lama dalam totjurnal
    cSQL = String.Format("delete from totjurnal where faktur = '{0}' ", _cFaktur)
    myConnection.SQL = cSQL
    myConnection.Delete()

    'Hapus Data Lama dalam JURNAL
    cSQL = String.Format("delete from jurnal where faktur = '{0}' ", _cFaktur)
    myConnection.SQL = cSQL
    myConnection.Delete()

    myConnection.InitConnection()
    cSQL = "insert into totjurnal (faktur, tgl, username, datetime, totaldebet, totalkredit, cabangentry, keterangan, status) "
    cSQL = cSQL & "values (@faktur, @tgl, @username, @datetime, @totaldebet, @totalkredit, @cabangentry, @keterangan, @status) "
    Dim cmd As MySqlCommand = New MySqlCommand(cSQL, myConnection.OpenConnection)
    With cmd.Parameters
      .AddWithValue("@faktur ", _cFaktur)
      .AddWithValue("@tgl", _dTgl)
      .AddWithValue("@username", _cUser)
      .AddWithValue("@datetime", _cDateTime)
      .AddWithValue("@totaldebet", _nTotalDebet)
      .AddWithValue("@totalkredit", _nTotalKredit)
      .AddWithValue("@cabangentry", _cCabangEntry)
      .AddWithValue("@keterangan", _cKeterangan)
      .AddWithValue("@status", _cStatusJurnal)
    End With
    cmd.ExecuteNonQuery()
    ClassHistory.SQL = cSQL
    ClassHistory.ObjCmd = cmd
    cmd.Dispose()
    myConnection.CloseConnection()
    myConnection.Dispose()
    If _lUpdateHistory Then
      ClassHistory.Database = _cDatabase
      ClassHistory.IPNumber = _cIPNumber
      ClassHistory.Port = _cPort
      ClassHistory.Username = _cUser
      ClassHistory.UpdateHistory()
    End If
  End Sub

  Sub UpdJurnal()
    Dim myConnection As New DataBaseConnection()
    Dim classHistory As New ClassHistory
    If _nDebet <> 0 Or _nKredit <> 0 Then
      'Simpan ke JURNAL (Detail)
      myConnection.Database = _cDatabase
      myConnection.IP = _cIPNumber
      myConnection.Port = _cPort
      myConnection.InitConnection()
      Dim cSQL As String = "insert into jurnal (Faktur, tgl, Rekening, Keterangan, Debet, Kredit, CabangEntry) "
      cSQL = cSQL & "values(@faktur, @tgl, @rekening, @keterangan, @debet, @kredit, @cabangentry) "
      Dim cmd As MySqlCommand = New MySqlCommand(cSQL, myConnection.OpenConnection)
      With cmd
        .Parameters.AddWithValue("@faktur", _cFaktur)
        .Parameters.AddWithValue("@tgl", _dTgl)
        .Parameters.AddWithValue("@rekening", _cRekeningJurnal)
        .Parameters.AddWithValue("@keterangan", _cKeterangan)
        .Parameters.AddWithValue("@debet", _nDebet)
        .Parameters.AddWithValue("@kredit", _nKredit)
        .Parameters.AddWithValue("@cabangentry", _cCabangEntry)
        .ExecuteNonQuery()
        classHistory.SQL = cSQL
        classHistory.ObjCmd = cmd
        .Dispose()
      End With
      myConnection.CloseConnection()
      myConnection.Dispose()
      If _lUpdateHistory Then
        classHistory.Database = _cDatabase
        classHistory.IPNumber = _cIPNumber
        classHistory.Port = _cPort
        classHistory.Username = _cUser
        classHistory.UpdateHistory()
      End If
    End If
  End Sub

  Sub PostingRekapBukuBesar()
    Dim myConnection As New DataBaseConnection()
    Dim classHistory As New ClassHistory
    Dim a As Double = 0
    Dim d As Date = BOM(DateAdd(DateInterval.Month, -3, _dTgl))
    Dim dbData As New DataTable

    myConnection.Database = _cDatabase
    myConnection.IP = _cIPNumber
    myConnection.Port = _cPort
    Dim cSQL As String = "select CabangEntry, Tgl, Rekening, Sum(Debet) as Debet, Sum(Kredit) as Kredit from bukubesar "
    cSQL = String.Format("{0}where tgl >= '{1}' group by  cabangentry, rekening, tgl ", cSQL, formatValue(d, formatType.yyyy_MM_dd))
    myConnection.SQL = cSQL
    dbData = myConnection.Browse
    If Not IsNothing(dbData) Then
      For n As Integer = 0 To dbData.Rows.Count - 1
        With dbData.Rows(n)
          a = a + 1

          cSQL = String.Format("delete from bukubesar_rekap where cabangentry = '{0}' ", .Item("CabangEntry"))
          cSQL = String.Format("{0}and tgl = '{1}' ", cSQL, formatValue(EOM(CDate(.Item("Tgl").ToString)), formatType.yyyy_MM_dd))
          cSQL = String.Format("{0}and rekening = '{1}' ", cSQL, .Item("Rekening"))
          myConnection.SQL = cSQL
          myConnection.Delete()

          myConnection.InitConnection()
          cSQL = "insert into bukubesar_rekap (cabangentry, tgl, rekening, debet, kredit) "
          cSQL = cSQL & "values (@cabangentry, @tgl, @rekening, @debet, @kredit) "
          Dim cmd As MySqlCommand = New MySqlCommand(cSQL, myConnection.OpenConnection)
          cmd.Parameters.AddWithValue("@cabangentry", .Item("CabangEntry").ToString)
          cmd.Parameters.AddWithValue("@tgl", EOM(CDate(.Item("Tgl").ToString)))
          cmd.Parameters.AddWithValue("@rekening", .Item("Rekening").ToString)
          cmd.Parameters.AddWithValue("@debet", "&Debet+" & Replace(CStr(.Item("Debet").ToString), ",", "."))
          cmd.Parameters.AddWithValue("@kredit", "&Kredit+" & Replace(CStr(.Item("Kredit").ToString), ",", "."))
          cmd.ExecuteNonQuery()
          ClassHistory.SQL = cSQL
          ClassHistory.ObjCmd = cmd
          cmd.Dispose()
          myConnection.CloseConnection()
          myConnection.Dispose()
          If _lUpdateHistory Then
            ClassHistory.Database = _cDatabase
            ClassHistory.IPNumber = _cIPNumber
            ClassHistory.Port = _cPort
            ClassHistory.Username = _cUser
            ClassHistory.UpdateHistory()
          End If
        End With
      Next
    End If
  End Sub
End Class
