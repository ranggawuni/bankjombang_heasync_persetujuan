﻿Imports System.Net
Imports System.IO

Public Class ClassPersetujuanPengajuan
  Public Enum JenisFiat
    acNoTransaksi = 0
    acSetorTabungan = 1
    acTarikTabungan = 2
    acTutupTabungan = 3
    acSetorDeposito = 10
    acTarikDeposito = 11
    acBungaDeposito = 12
    acPencairanKredit = 21
    acAngsuranKredit = 22
    acPenarikanRK = 23
    acPenerimaanRK = 24
    acMutasiKasKeluar = 31
    acMutasiKasMasuk = 32
    acMutasiKasBesarKeluar = 33
    acMutasiKasBesarMasuk = 34
    acGantiRegisterTabungan = 41
    acGantiRegisterDeposito = 42
    acGantiRegisterKredit = 43
    acPersetujuanPengajuanKredit = 44
  End Enum

  Public Enum eStatusPengajuanKredit
    eBelumCair = 0
    eCair = 1
    eDitolak = 2
    eDisetujui = 3
    eBatalDisetujui = 4
  End Enum

  Private Function GetURL() As String
    Dim x As New ClassXmlFile() With {.JenisDatabase = ClassXmlFile.enJenisDatabase.remote, .Field = "ip"}
    Return x.GetValue
  End Function

  <System.Diagnostics.CodeAnalysis.SuppressMessage("CodeRush", "String.Format can be used")>
  Sub KirimDataPengajuan(ByVal worker As System.ComponentModel.BackgroundWorker, ByVal e As System.ComponentModel.DoWorkEventArgs)
    Dim mc As New DataBaseConnection
    Dim cSQL As String = ""

    If worker.CancellationPending Then
      e.Cancel = True
    Else
      Dim db As New DataTable
      Dim x As New ClassXmlFile()
      With x
        .JenisDatabase = ClassXmlFile.enJenisDatabase.local
        .Field = "databasename"
        mc.Database = .GetValue
        .Field = "ip"
        mc.IP = .GetValue
        .Field = "port"
        mc.Port = .GetValue()
      End With
      Dim cURLTemp As String = ""
      cURLTemp = GetURL()
      'cSQL = "select * from pengajuankredit where statuspengajuan = 0 and tgl = curdate() "
      cSQL = "select * from request_pengajuan where acc = 0 and jenis = '" & JenisFiat.acPersetujuanPengajuanKredit & "' "
      mc.SQL = cSQL
      db = mc.Browse()
      If Not IsNothing(db) Then
        Dim nMax As Integer = db.Rows.Count - 1
        Dim nResult As Integer = 0
        Dim cWaktu As DateTime = Now
        Dim cReportStatus As String = "Preparing..."
        worker.ReportProgress(nResult, cReportStatus)
        For n As Integer = 0 To nMax
          Dim cRekening As String = db.Rows(n).Item("Rekening").ToString
          Dim lFound As Boolean = findDataPengajuan(cRekening)
          If Not lFound Then
            nResult = CInt(Math.Round((n + 1) / (nMax + 1) * 100))
            If worker.CancellationPending Then
              worker.ReportProgress(nResult, "Cancelling...")
              Exit For
            End If
            cReportStatus = HitungDurasi(cWaktu) & "~"
            worker.ReportProgress(nResult, cReportStatus)
            Dim cURL As String = "http://" & cURLTemp & "/pengajuankredit/server/api/save?"
            Dim dTglTemp As String = formatValue(CDate(db.Rows(n).Item("Tgl").ToString), formatType.yyyy_MM_dd)
            Dim cKeterangan As String = db.Rows(n).Item("Transaksi").ToString
            Dim cCabangEntry As String = db.Rows(n).Item("CabangEntry").ToString
            Dim nNominal As Double = CDbl(db.Rows(n).Item("Nominal"))
            Dim cUserRequest As String = db.Rows(n).Item("userrequest").ToString
            Dim nUserLevel = CInt(db.Rows(0)("level").ToString)
            Dim cData As String = "cabangentry=" & cCabangEntry & "&transaksi=" & cKeterangan & "&tgl=" & dTglTemp &
                                  "&level=" & nUserLevel & "&rekening=" & cRekening & "&nominal=" & nNominal &
                                  "&userrequest=" & cUserRequest

            Dim request As HttpWebRequest = CType(WebRequest.Create(cURL & cData), HttpWebRequest)
            Dim lError As Boolean = False
            Dim response As WebResponse = Nothing
            Try
              response = DirectCast(request.GetResponse(), HttpWebResponse)
            Catch ex As WebException ' Net.WebException
              Console.WriteLine(ex.Message)
              lError = True
            End Try

            If lError Then
              Console.WriteLine("No Response")
            Else
              Dim cKeteranganReponse As String = CType(response, HttpWebResponse).StatusCode & " - " & CType(response, HttpWebResponse).StatusDescription
              Console.WriteLine(cKeteranganReponse)
            End If
            If Not IsNothing(response) Then
              response.Close()
            End If
          End If
        Next
      End If
    End If
  End Sub

  <System.Diagnostics.CodeAnalysis.SuppressMessage("CodeRush", "String.Format can be used")>
  Sub AmbilDataPengajuan(ByVal worker As System.ComponentModel.BackgroundWorker, ByVal e As System.ComponentModel.DoWorkEventArgs)
    Dim mc As New DataBaseConnection
    Dim cSQL As String = ""
    If worker.CancellationPending Then
      e.Cancel = True
    Else
      Dim db As New DataTable
      Dim x As New ClassXmlFile() With {.JenisDatabase = ClassXmlFile.enJenisDatabase.local, .Field = "exit"}
      If x.GetValue = "1" Then
        End
        Exit Sub
      End If

      x.Field = "databasename"
      mc.Database = x.GetValue
      x.Field = "ip"
      mc.IP = x.GetValue
      x.Field = "port"
      mc.Port = x.GetValue()

      Dim cURLTemp As String = ""
      cURLTemp = GetURL()

      Dim cTanggal As String
      If Debugger.IsAttached Then
        cTanggal = "2015-07-01"
      Else
        cTanggal = "2015-08-01"
      End If
      cSQL = "select * from pengajuankredit where statuspengajuan = 0 and tgl >= '" & cTanggal & "' "
      'cSQL = cSQL & " and kode = '01.000001'"
      mc.SQL = cSQL
      db = mc.Browse()
      If Not IsNothing(db) Then
        Dim nMax As Integer = db.Rows.Count - 1
        Dim nResult As Integer = 0
        Dim cWaktu As DateTime = Now
        Dim cReportStatus As String = "Preparing..."
        worker.ReportProgress(nResult, cReportStatus)
        For n As Integer = 0 To nMax
          nResult = CInt(Math.Round((n + 1) / (nMax + 1) * 100))
          If worker.CancellationPending Then
            worker.ReportProgress(nResult, "Cancelling...")
            Exit For
          End If
          cReportStatus = HitungDurasi(cWaktu) & "~"
          worker.ReportProgress(nResult, cReportStatus)

          Dim cURL As String = "http://" & cURLTemp & "/pengajuankredit/server/api/GetAcc?"
          Dim cData As String = "rekening=" & db.Rows(n)("Rekening").ToString
          Dim request As HttpWebRequest = CType(WebRequest.Create(cURL & cData), HttpWebRequest)
          Dim lError As Boolean = False
          Dim response As WebResponse = Nothing
          Try
            response = DirectCast(request.GetResponse(), HttpWebResponse)
          Catch ex As WebException ' Net.WebException
            Console.WriteLine(ex.Message)
            lError = True
          End Try

          If lError Then
            Console.WriteLine("No Response")
          Else
            Dim cKeteranganReponse As String = CType(response, HttpWebResponse).StatusCode & " - " & CType(response, HttpWebResponse).StatusDescription
            Console.WriteLine(cKeteranganReponse)
            Dim reader As New StreamReader(response.GetResponseStream)
            Dim cResult As String = reader.ReadToEnd
            Console.WriteLine(cResult)
            If cResult <>"err" then
              Dim va As String() = cResult.Split(CChar("~"))
              If UBound(va) > 0 Then
                If va(1).ToString >= "1" Then
                  Dim cStatus As String = va(1).ToString
                  Dim cRekening As String = va(0).ToString
                  'untuk refrensi status di database bpr_jombang, lihat enum eStatusPengajuanKredit diatas
                  Dim cStatusPengajuan As String = cStatus
                  If cStatus = "1" Then
                    cStatusPengajuan = "3"
                  End If
                  cSQL = "update pengajuankredit set statuspengajuan = @statuspengajuan where rekening = @rekening "
                  Dim cmd As MySqlCommand = New MySqlCommand(cSQL, mc.OpenConnection)
                  With cmd.Parameters
                    .AddWithValue("@statuspengajuan", cStatusPengajuan.ToString)
                    .AddWithValue("@rekening", cRekening)
                  End With
                  cmd.ExecuteNonQuery()
                  cmd.Dispose()
                  cSQL = "update request_pengajuan set acc = @acc where rekening = @rekening "
                  cmd = New MySqlCommand(cSQL, mc.OpenConnection)
                  With cmd.Parameters
                    .AddWithValue("@acc", cStatus)
                    .AddWithValue("@rekening", cRekening)
                  End With
                  cmd.ExecuteNonQuery()
                  cmd.Dispose()
                  mc.CloseConnection()
                  mc.Dispose()
                End If
              End If
            End If
            End If
            If Not IsNothing(response) Then
              response.Close()
            End If
        Next
      End If
    End If
  End Sub

  <System.Diagnostics.CodeAnalysis.SuppressMessage("CodeRush", "String.Format can be used")>
  Function findDataPengajuan(ByVal cNomorPengajuan As String) As Boolean
    Dim lFound As Boolean

    Dim lError As Boolean = False
    Dim response As WebResponse = Nothing
    Dim cURLTemp As String = ""
    Dim cURL As String = ""
    Dim cData As String = ""
    Dim request As HttpWebRequest = Nothing
    Try
      cURLTemp = GetURL()
      cURL = "http://" & cURLTemp & "/pengajuankredit/server/api/findRekening?"
      cData = "rekening=" & cNomorPengajuan
      request = CType(WebRequest.Create(cURL & cData), HttpWebRequest)
      response = DirectCast(request.GetResponse(), HttpWebResponse)
    Catch ex As WebException ' Net.WebException
      Console.WriteLine(ex.Message)
      lError = True
    End Try

    If lError Then
      Console.WriteLine("No Response")
      'kalau ada error maka dianggap data ditemukan jadi data pengajuan tidak perlu di update 
      lFound = True
    Else
      Dim cKeteranganReponse As String = CType(response, HttpWebResponse).StatusCode & " - " & CType(response, HttpWebResponse).StatusDescription
      Console.WriteLine(cKeteranganReponse)
      Dim reader As New StreamReader(response.GetResponseStream)
      Dim cResult As String = reader.ReadToEnd
      Console.WriteLine(cResult)
      If cResult = "1" Then
        lFound = True
      Else
        lFound = False
      End If
    End If
    findDataPengajuan = lFound
    If Not IsNothing(response) Then
      response.Close()
    End If
  End Function

  <System.Diagnostics.CodeAnalysis.SuppressMessage("CodeRush", "String.Format can be used")>
  Function checkConnection() As Boolean
    Dim lFound As Boolean

    Dim lError As Boolean = False
    Dim response As WebResponse = Nothing
    Dim cURLTemp As String = ""
    Dim cURL As String = ""
    Dim cData As String = ""
    Dim request As HttpWebRequest = Nothing
    Try
      cURLTemp = GetURL()
      cURL = "http://" & cURLTemp & "/pengajuankredit/server/api/findRekening?"
      cData = "rekening=00"
      request = CType(WebRequest.Create(cURL & cData), HttpWebRequest)
      response = DirectCast(request.GetResponse(), HttpWebResponse)
    Catch ex As WebException ' Net.WebException
      Console.WriteLine(ex.Message)
      lError = True
    End Try

    If lError Then
      Console.WriteLine("No Response")
      'kalau ada error maka dianggap data ditemukan jadi data pengajuan tidak perlu di update 
      lFound = True
    Else
      Dim cKeteranganReponse As String = CType(response, HttpWebResponse).StatusCode & " - " & CType(response, HttpWebResponse).StatusDescription
      Console.WriteLine(cKeteranganReponse)
      Dim reader As New StreamReader(response.GetResponseStream)
      Dim cResult As String = reader.ReadToEnd
      Console.WriteLine(cResult)
      If cKeteranganReponse = "200 - OK" Then
        lError = False
      Else
        lError = True
      End If
    End If
    If Not IsNothing(response) Then
      response.Close()
    End If
    checkConnection = Not lError
  End Function

End Class
