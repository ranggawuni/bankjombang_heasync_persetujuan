﻿Imports System
Imports System.Reflection
Imports System.Security
Imports System.Security.AccessControl
Imports Microsoft.Win32

Module App
  Public NamaExe As String = "BPR-Jombang.exe"
  Public FileVersi As String = App.Path & "\ver.txt"
  Public LinkVersi As String = "http://hea.web.id/updjombang/ver.txt"
  Public LinkUpdate As String = "http://hea.web.id/updjombang/BPR-Jombang.exe"
  'Public LinkVersi As String = "http://localhost/updjombang/ver.txt"
  'Public LinkUpdate As String = "http://localhost/updjombang/BPR-Jombang.exe"
  Public VersiOnline As String = "0.0.0"
  Public FileExe As String = App.Path & "\" & NamaExe
  Public ProcessName As String = "BPR-Jombang"

  Public Function Path() As String
    Return System.IO.Path.GetDirectoryName( _
       System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase).Replace("file:\", "")
  End Function

  Public Function getFileVersion() As String
    Try
      Dim myFileVersionInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(App.Path & "\" & NamaExe)
      Return myFileVersionInfo.FileVersion
    Catch ex As Exception
      Return "-.-.-"
    End Try

  End Function

  Public Function ProsesIsRunning() As Boolean
    Dim p() As Process
    p = Process.GetProcessesByName(ProcessName)
    If p.Length > 0 Then
      'MsgBox("Proses running")
      Return True ' Process is running
    Else
      Return False ' Process is not running
    End If

  End Function

  Public Sub KillProcess()
    Dim ProsesSudahTidakAda As Boolean = False

    Do Until ProsesSudahTidakAda = True
      Dim pProcess() As Process = System.Diagnostics.Process.GetProcessesByName(ProcessName)
      For Each p As Process In pProcess
        Try
          p.Kill()
        Catch ex As Exception
          Exit Sub
        End Try

      Next
      ProsesSudahTidakAda = Not ProsesIsRunning()
    Loop
  End Sub

  Public Sub RegisterStartUp()

    'My.Computer.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Run", True).SetValue(Application.ProductName, Application.ExecutablePath)
    Dim rs As New RegistrySecurity()
    Dim user As String = Environment.UserDomainName & "\" & Environment.UserName
    rs.AddAccessRule(New RegistryAccessRule(user, _
            RegistryRights.ChangePermissions Or RegistryRights.ReadKey Or RegistryRights.Delete Or RegistryRights.CreateSubKey Or RegistryRights.CreateLink Or RegistryRights.WriteKey Or RegistryRights.SetValue, _
           InheritanceFlags.None, _
           PropagationFlags.None, _
           AccessControlType.Allow))
    Try
      My.Computer.Registry.LocalMachine.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Run", True).SetValue(Application.ProductName, Application.ExecutablePath)
    Catch ex As Exception
      MsgBox("Maaf, program harus dijalankan sebagai administrator. Silahkan klik kanan program->kemudian pilih ""Run as Administrator """, MsgBoxStyle.Critical, "Heasoft Application Updater")
      End
    End Try

    'Dim subKeyName As String = "SOFTWARE\Microsoft\Windows\CurrentVersion\Run\"
    '' Open the location for reading
    'Dim rk As RegistryKey ' = Nothing
    'rk = Registry.CurrentUser.OpenSubKey(subKeyName, True)
    'rk.SetAccessControl(rs)
    'rk.SetValue(Application.ProductName, Application.ExecutablePath)
    'rk.Close()
    
  End Sub

  Public Function isRegistered() As Boolean
    Try

      Dim subKeyName As String = "SOFTWARE\Microsoft\Windows\CurrentVersion\Run\"

      ' Open the location for reading
      Dim key As RegistryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(subKeyName)
      ' Get the value of the field InstallLocation
      Dim readValue As String = key.GetValue(Application.ProductName).ToString
      key.Close()
      If readValue = Nothing Then
        Return False
      Else
        Return True
      End If
    Catch ex As Exception
      Return False
    End Try

  End Function

  Public Sub AddStartUp()
    If Not isRegistered() Then
      RegisterStartUp()
      'TulisRegistry()
    End If
  End Sub

  Public Sub TulisRegistry()
    Try

      Dim subKeyName As String = "SOFTWARE\Microsoft\Windows\CurrentVersion\Run\"

      ' Open the location for reading
      Dim key As RegistryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(subKeyName, True)
      ' Get the value of the field InstallLocation
      key.SetValue(Application.ProductName, Application.ExecutablePath)
      key.Close()
    Catch ex As Exception
      MsgBox(ex.Message)
    End Try
  End Sub

  Function PrevInstance() As Boolean
    If UBound(Diagnostics.Process.GetProcessesByName _
     (Diagnostics.Process.GetCurrentProcess.ProcessName)) _
     > 0 Then
      Return True
    Else
      Return False
    End If
  End Function

End Module

