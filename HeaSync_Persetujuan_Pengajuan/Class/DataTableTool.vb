﻿Public Class DataTableTool
    Public Shared dtUrut As DataTable
    Public Shared objDataTable As DataTable
    Private Shared _Sql As String = ""

    Enum eGridColumnType
        eString = 0
        eInteger = 1
        eDouble = 2
        eDatetime = 3
        eSingle = 4
        eBoolean = 5
    End Enum

    Public Shared Property SQL() As String
        Get
            Return _Sql
        End Get
        Set(value As String)
            _Sql = value
        End Set
    End Property

    Public Shared Function UrutData(ByVal cOrder As String) As DataTable
        Dim dvUrut As New DataView(dtUrut) With {.Sort = cOrder}
        UrutData = dvUrut.ToTable
    End Function

    Shared Sub AddColumn(ByVal name As String, ByVal nType As eGridColumnType, Optional ByVal ro As Boolean = False,
                     Optional ByVal lAutoNumber As Boolean = False)
        Dim typeTemp As Type = Nothing
        Select Case nType
            Case eGridColumnType.eString
                typeTemp = System.Type.GetType("System.String")
            Case eGridColumnType.eInteger
                typeTemp = System.Type.GetType("System.Int32")
            Case eGridColumnType.eDouble
                typeTemp = System.Type.GetType("System.Double")
            Case eGridColumnType.eDatetime
                typeTemp = System.Type.GetType("System.DateTime")
            Case eGridColumnType.eSingle
                typeTemp = System.Type.GetType("System.Single")
            Case eGridColumnType.eBoolean
                typeTemp = System.Type.GetType("System.Boolean")
        End Select
        Dim col As DataColumn = New DataColumn(name, typeTemp) With {.Caption = name, .ReadOnly = ro}
        objDataTable.Columns.Add(col)
        If lAutoNumber Then
            col.AutoIncrement = lAutoNumber
            col.AutoIncrementSeed = 1
            col.AutoIncrementStep = 1
        End If
    End Sub

    Shared Sub AddPrimaryKey(ByVal columnName As String)
        Dim columns(1) As DataColumn
        columns(0) = objDataTable.Columns(columnName)
        objDataTable.PrimaryKey = columns
    End Sub

    Shared Sub RemovePrimaryKey(ByVal data As DataTable)
        data.PrimaryKey = Nothing
    End Sub
End Class
