﻿Imports System.IO
Imports System.Xml

Public Class ClassXmlFile
  Enum enJenisDatabase
    none = 0
    local = 1
    remote = 2
  End Enum

  Private _cField As String = ""
  Private _cValue As String = ""
  Private _JenisDatabase As enJenisDatabase = enJenisDatabase.none

  Public Property JenisDatabase As enJenisDatabase
    Get
      Return _JenisDatabase
    End Get
    Set(value As enJenisDatabase)
      _JenisDatabase = value
    End Set
  End Property

  Public Property Value As String
    Get
      Return _cValue
    End Get
    Set(value As String)
      _cValue = value
    End Set
  End Property

  Public Property Field As String
    Get
      Return _cField
    End Get
    Set(value As String)
      _cField = value
    End Set
  End Property

  Function GetValue() As String
    GetValue = ""
    Try
      Dim cPath As String = App.Path ' app.Path
      Dim cFile As String = cPath & "\sync_config.xml"
      Dim cJenisDatabase As String = ""
      If _JenisDatabase = enJenisDatabase.local Then
        cJenisDatabase = "local_database"
      ElseIf _JenisDatabase = enJenisDatabase.remote Then
        cJenisDatabase = "remote_database"
      End If
      Dim ds As New DataSet
      ds.ReadXml(cFile)
      _cValue = ds.Tables(cJenisDatabase).Rows(0)(_cField).ToString
      Return _cValue
    Catch errorVariable As Exception
      'Error trapping
      Console.Write(errorVariable.ToString())
    End Try
  End Function

  Function EditValue() As Boolean
    Try
      Dim cPath As String = app.Path
      Dim cFile As String = cPath & "\sync_config.xml"
      Dim cJenisDatabase As String = ""
      If _JenisDatabase = enJenisDatabase.local Then
        cJenisDatabase = "local_database"
      ElseIf _JenisDatabase = enJenisDatabase.remote Then
        cJenisDatabase = "remote_database"
      End If
      Dim ds As New DataSet
      ds.ReadXml(cFile)
      ds.Tables(cJenisDatabase).Rows(0)(_cField) = _cValue
      ds.AcceptChanges()
      ds.WriteXml(cFile)

      Return True
    Catch errorVariable As Exception
      'Error trapping
      Console.Write(errorVariable.ToString())
      Return False
    End Try
  End Function
End Class
